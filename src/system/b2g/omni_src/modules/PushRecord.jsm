"use strict";const Cc=Components.classes;const Ci=Components.interfaces;const Cu=Components.utils;const Cr=Components.results;Cu.import("resource://gre/modules/AppConstants.jsm");Cu.import("resource://gre/modules/Preferences.jsm");Cu.import("resource://gre/modules/Services.jsm");Cu.import("resource://gre/modules/XPCOMUtils.jsm");XPCOMUtils.defineLazyModuleGetter(this,"Messaging","resource://gre/modules/Messaging.jsm");XPCOMUtils.defineLazyModuleGetter(this,"PlacesUtils","resource://gre/modules/PlacesUtils.jsm");XPCOMUtils.defineLazyModuleGetter(this,"Task","resource://gre/modules/Task.jsm");XPCOMUtils.defineLazyModuleGetter(this,"PrivateBrowsingUtils","resource://gre/modules/PrivateBrowsingUtils.jsm");this.EXPORTED_SYMBOLS=["PushRecord"];const prefs=new Preferences("dom.push.");const maxRetryCounts=3;this.PushRecord=function(props){this.pushEndpoint=props.pushEndpoint;this.scope=props.scope;this.originAttributes=props.originAttributes;this.pushCount=props.pushCount||0;this.lastPush=props.lastPush||0;this.p256dhPublicKey=props.p256dhPublicKey;this.p256dhPrivateKey=props.p256dhPrivateKey;this.authenticationSecret=props.authenticationSecret;this.systemRecord=!!props.systemRecord;this.appServerKey=props.appServerKey;this.setQuota(props.quota);this.ctime=(typeof props.ctime==="number")?props.ctime:0;this.retryCounts=props.retryCounts||0;}
PushRecord.prototype={setQuota(suggestedQuota){if(this.quotaApplies()&&!isNaN(suggestedQuota)&&suggestedQuota>=0){this.quota=suggestedQuota;}else{this.resetQuota();}},resetQuota(){this.quota=this.quotaApplies()?prefs.get("maxQuotaPerSubscription"):Infinity;},updateQuota(lastVisit){if(this.isExpired()||!this.quotaApplies()||this.isInstalledApp()){return;}
if(lastVisit<0){this.quota=0;return;}
if(lastVisit>this.lastPush){
let daysElapsed=(Date.now()-lastVisit)/24/60/60/1000;this.quota=Math.min(Math.round(8*Math.pow(daysElapsed,-0.8)),prefs.get("maxQuotaPerSubscription"));Services.telemetry.getHistogramById("PUSH_API_QUOTA_RESET_TO").add(this.quota);}},receivedPush(lastVisit){this.updateQuota(lastVisit);this.pushCount++;this.lastPush=Date.now();},reduceQuota(){if(!this.quotaApplies()||this.isInstalledApp()){return;}
this.quota=Math.max(this.quota-1,0);if(this.isExpired()&&this.ctime>0){let duration=Date.now()-this.ctime;Services.telemetry.getHistogramById("PUSH_API_QUOTA_EXPIRATION_TIME").add(duration/1000);}},getLastVisit:Task.async(function*(){if(!this.quotaApplies()||this.isInstalledApp()||this.isTabOpen()){
return Date.now();}
if(AppConstants.MOZ_ANDROID_HISTORY){let result=yield Messaging.sendRequestForResult({type:"History:GetPrePathLastVisitedTimeMilliseconds",prePath:this.uri.prePath,});return result==0?-Infinity:result;}



const QUOTA_REFRESH_TRANSITIONS_SQL=[Ci.nsINavHistoryService.TRANSITION_LINK,Ci.nsINavHistoryService.TRANSITION_TYPED,Ci.nsINavHistoryService.TRANSITION_BOOKMARK,Ci.nsINavHistoryService.TRANSITION_REDIRECT_PERMANENT,Ci.nsINavHistoryService.TRANSITION_REDIRECT_TEMPORARY].join(",");let db=yield PlacesUtils.promiseDBConnection();
let rows=yield db.executeCached(`SELECT MAX(visit_date) AS lastVisit
       FROM moz_places p
       JOIN moz_historyvisits ON p.id = place_id
       WHERE rev_host = get_unreversed_host(:host || '.') || '.'
         AND url BETWEEN :prePath AND :prePath || X'FFFF'
         AND visit_type IN (${QUOTA_REFRESH_TRANSITIONS_SQL})
      `,{host:this.uri.host,prePath:this.uri.prePath,});if(!rows.length){return-Infinity;}
let lastVisit=rows[0].getResultByName("lastVisit");return lastVisit/1000;}),isInstalledApp(){return this.principal.originAttributes.appId!=Ci.nsIScriptSecurityManager.NO_APP_ID;},isTabOpen(){let windows=Services.wm.getEnumerator("navigator:browser");while(windows.hasMoreElements()){let window=windows.getNext();if(window.closed||PrivateBrowsingUtils.isWindowPrivate(window)){continue;}
let tabs;if(window.gBrowser){tabs=window.gBrowser.tabContainer.children;}else if(window.BrowserApp){tabs=window.BrowserApp.tabs;}else{continue;}
for(let tab of tabs){let tabURI=(tab.linkedBrowser||tab.browser).currentURI;if(tabURI.prePath==this.uri.prePath){return true;}}}
return false;},hasPermission(){if(this.systemRecord||prefs.get("testing.ignorePermission")){return true;}
let permission=Services.perms.testExactPermissionFromPrincipal(this.principal,"desktop-notification");return permission==Ci.nsIPermissionManager.ALLOW_ACTION;},quotaChanged(){if(!this.hasPermission()){return Promise.resolve(false);}
return this.getLastVisit().then(lastVisit=>lastVisit>this.lastPush);},quotaApplies(){return!this.systemRecord;},isExpired(){return this.quota===0;},matchesOriginAttributes(pattern){if(this.systemRecord){return false;}
return ChromeUtils.originAttributesMatchPattern(this.principal.originAttributes,pattern);},hasAuthenticationSecret(){return!!this.authenticationSecret&&this.authenticationSecret.byteLength==16;},matchesAppServerKey(key){if(!this.appServerKey){return!key;}
if(!key){return false;}
return this.appServerKey.length===key.length&&this.appServerKey.every((value,index)=>value===key[index]);},toSubscription(){return{endpoint:this.pushEndpoint,lastPush:this.lastPush,pushCount:this.pushCount,p256dhKey:this.p256dhPublicKey,authenticationSecret:this.authenticationSecret,appServerKey:this.appServerKey,quota:this.quotaApplies()?this.quota:-1,};},reachMaxRetryCounts(){if(this.retryCounts>=maxRetryCounts){return true;}
return false;},};
var principals=new WeakMap();Object.defineProperties(PushRecord.prototype,{principal:{get(){if(this.systemRecord){return Services.scriptSecurityManager.getSystemPrincipal();}
let principal=principals.get(this);if(!principal){let url=this.scope;if(this.originAttributes){url+=this.originAttributes;}
principal=Services.scriptSecurityManager.createCodebasePrincipalFromOrigin(url);principals.set(this,principal);}
return principal;},configurable:true,},uri:{get(){return this.principal.URI;},configurable:true,},});