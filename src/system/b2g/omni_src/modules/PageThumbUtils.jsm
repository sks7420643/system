this.EXPORTED_SYMBOLS=["PageThumbUtils"];const{classes:Cc,interfaces:Ci,utils:Cu}=Components;Cu.import("resource://gre/modules/XPCOMUtils.jsm",this);Cu.import("resource://gre/modules/Services.jsm");Cu.import("resource://gre/modules/Promise.jsm",this);Cu.import("resource://gre/modules/AppConstants.jsm");this.PageThumbUtils={THUMBNAIL_BG_COLOR:"#fff",HTML_NAMESPACE:"http://www.w3.org/1999/xhtml",createCanvas:function(aWindow,aWidth=0,aHeight=0){let doc=(aWindow||Services.appShell.hiddenDOMWindow).document;let canvas=doc.createElementNS(this.HTML_NAMESPACE,"canvas");canvas.mozOpaque=true;canvas.mozImageSmoothingEnabled=true;let[thumbnailWidth,thumbnailHeight]=this.getThumbnailSize(aWindow);canvas.width=aWidth?aWidth:thumbnailWidth;canvas.height=aHeight?aHeight:thumbnailHeight;return canvas;},getThumbnailSize:function(aWindow=null){if(!this._thumbnailWidth||!this._thumbnailHeight){let screenManager=Cc["@mozilla.org/gfx/screenmanager;1"].getService(Ci.nsIScreenManager);let left={},top={},screenWidth={},screenHeight={};screenManager.primaryScreen.GetRectDisplayPix(left,top,screenWidth,screenHeight);let systemScale=screenManager.systemDefaultScale;let windowScale=aWindow?aWindow.devicePixelRatio:systemScale;let scale=Math.max(systemScale,windowScale);if(AppConstants.platform=="macosx"&&!aWindow){scale=2;}
let prefWidth=Services.prefs.getIntPref("toolkit.pageThumbs.minWidth");let prefHeight=Services.prefs.getIntPref("toolkit.pageThumbs.minHeight");let divisor=Services.prefs.getIntPref("toolkit.pageThumbs.screenSizeDivisor");prefWidth*=scale;prefHeight*=scale;this._thumbnailWidth=Math.max(Math.round(screenWidth.value/divisor),prefWidth);this._thumbnailHeight=Math.max(Math.round(screenHeight.value/divisor),prefHeight);}
return[this._thumbnailWidth,this._thumbnailHeight];},getContentSize:function(aWindow){let utils=aWindow.QueryInterface(Ci.nsIInterfaceRequestor).getInterface(Ci.nsIDOMWindowUtils);let sbWidth={},sbHeight={};try{utils.getScrollbarSize(false,sbWidth,sbHeight);}catch(e){Cu.reportError("Unable to get scrollbar size in determineCropSize.");sbWidth.value=sbHeight.value=0;}
let width=aWindow.innerWidth-sbWidth.value;let height=aWindow.innerHeight-sbHeight.value;return[width,height];},createSnapshotThumbnail:function(aWindow,aDestCanvas,aArgs){if(Cu.isCrossProcessWrapper(aWindow)){throw new Error('Do not pass cpows here.');}
let fullScale=aArgs?aArgs.fullScale:false;let[contentWidth,contentHeight]=this.getContentSize(aWindow);let[thumbnailWidth,thumbnailHeight]=aDestCanvas?[aDestCanvas.width,aDestCanvas.height]:this.getThumbnailSize(aWindow);

if(fullScale){thumbnailWidth=contentWidth;thumbnailHeight=contentHeight;if(aDestCanvas){aDestCanvas.width=contentWidth;aDestCanvas.height=contentHeight;}}
let intermediateWidth=thumbnailWidth*2;let intermediateHeight=thumbnailHeight*2;let skipDownscale=false;

if((intermediateWidth>=contentWidth||intermediateHeight>=contentHeight)||fullScale){intermediateWidth=thumbnailWidth;intermediateHeight=thumbnailHeight;skipDownscale=true;} 
let snapshotCanvas=this.createCanvas(aWindow,intermediateWidth,intermediateHeight);


let scale=Math.min(Math.max(intermediateWidth/contentWidth,intermediateHeight/contentHeight),1);let snapshotCtx=snapshotCanvas.getContext("2d");snapshotCtx.save();snapshotCtx.scale(scale,scale);snapshotCtx.drawWindow(aWindow,0,0,contentWidth,contentHeight,PageThumbUtils.THUMBNAIL_BG_COLOR,snapshotCtx.DRAWWINDOW_DO_NOT_FLUSH);snapshotCtx.restore();

let finalCanvas=aDestCanvas||this.createCanvas(aWindow,thumbnailWidth,thumbnailHeight);let finalCtx=finalCanvas.getContext("2d");finalCtx.save();if(!skipDownscale){finalCtx.scale(0.5,0.5);}
finalCtx.drawImage(snapshotCanvas,0,0);finalCtx.restore();return finalCanvas;},determineCropSize:function(aWindow,aCanvas){if(Cu.isCrossProcessWrapper(aWindow)){throw new Error('Do not pass cpows here.');}
let utils=aWindow.QueryInterface(Ci.nsIInterfaceRequestor).getInterface(Ci.nsIDOMWindowUtils);let sbWidth={},sbHeight={};try{utils.getScrollbarSize(false,sbWidth,sbHeight);}catch(e){Cu.reportError("Unable to get scrollbar size in determineCropSize.");sbWidth.value=sbHeight.value=0;}
let width=aWindow.innerWidth-sbWidth.value;let height=aWindow.innerHeight-sbHeight.value;let{width:thumbnailWidth,height:thumbnailHeight}=aCanvas;let scale=Math.min(Math.max(thumbnailWidth/width,thumbnailHeight/height),1);let scaledWidth=width*scale;let scaledHeight=height*scale;if(scaledHeight>thumbnailHeight)
height-=Math.floor(Math.abs(scaledHeight-thumbnailHeight)*scale);if(scaledWidth>thumbnailWidth)
width-=Math.floor(Math.abs(scaledWidth-thumbnailWidth)*scale);return[width,height,scale];},shouldStoreContentThumbnail:function(aDocument,aDocShell){
if(aDocument instanceof Ci.nsIDOMXMLDocument){return false;}
let webNav=aDocShell.QueryInterface(Ci.nsIWebNavigation);if(webNav.currentURI.schemeIs("about")){return false;}
if(aDocShell.busyFlags!=Ci.nsIDocShell.BUSY_FLAGS_NONE){return false;}
let channel=aDocShell.currentDocumentChannel;if(!channel){return false;}
let uri=channel.originalURI;if(uri.schemeIs("about")){return false;}
let httpChannel;try{httpChannel=channel.QueryInterface(Ci.nsIHttpChannel);}catch(e){}
if(httpChannel){try{if(Math.floor(httpChannel.responseStatus/100)!=2){return false;}}catch(e){
return false;}
if(httpChannel.isNoStoreResponse()){return false;}
if(uri.schemeIs("https")&&!Services.prefs.getBoolPref("browser.cache.disk_cache_ssl")){return false;}} 
return true;}};