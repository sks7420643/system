"use strict";const{Cc,Ci}=require("chrome");const{BreakpointActor,setBreakpointAtEntryPoints}=require("devtools/server/actors/breakpoint");const{OriginalLocation,GeneratedLocation}=require("devtools/server/actors/common");const{createValueGrip}=require("devtools/server/actors/object");const{ActorClass,Arg,RetVal,method}=require("devtools/server/protocol");const DevToolsUtils=require("devtools/shared/DevToolsUtils");const{assert,fetch}=DevToolsUtils;const{joinURI}=require("devtools/shared/path");const promise=require("promise");const{defer,resolve,reject,all}=promise;loader.lazyRequireGetter(this,"SourceMapConsumer","source-map",true);loader.lazyRequireGetter(this,"SourceMapGenerator","source-map",true);loader.lazyRequireGetter(this,"mapURIToAddonID","devtools/server/actors/utils/map-uri-to-addon-id");function isEvalSource(source){let introType=source.introductionType;
return(introType==='eval'||introType==='Function'||introType==='eventHandler'||introType==='setTimeout'||introType==='setInterval');}
exports.isEvalSource=isEvalSource;function getSourceURL(source,window){if(isEvalSource(source)){
if(source.displayURL&&source.introductionScript&&!isEvalSource(source.introductionScript.source)){if(source.introductionScript.source.url==='debugger eval code'){if(window){

return joinURI(window.location.href,source.displayURL);}}
else{return joinURI(source.introductionScript.source.url,source.displayURL);}}
return source.displayURL;}
else if(source.url==='debugger eval code'){ return null;}
return source.url;}
exports.getSourceURL=getSourceURL;function resolveURIToLocalPath(aURI){let resolved;switch(aURI.scheme){case"jar":case"file":return aURI;case"chrome":resolved=Cc["@mozilla.org/chrome/chrome-registry;1"].getService(Ci.nsIChromeRegistry).convertChromeURL(aURI);return resolveURIToLocalPath(resolved);case"resource":resolved=Cc["@mozilla.org/network/protocol;1?name=resource"].getService(Ci.nsIResProtocolHandler).resolveURI(aURI);aURI=Services.io.newURI(resolved,null,null);return resolveURIToLocalPath(aURI);default:return null;}}
let SourceActor=ActorClass({typeName:"source",initialize:function({source,thread,originalUrl,generatedSource,isInlineSource,contentType}){this._threadActor=thread;this._originalUrl=originalUrl;this._source=source;this._generatedSource=generatedSource;this._contentType=contentType;this._isInlineSource=isInlineSource;this.onSource=this.onSource.bind(this);this._invertSourceMap=this._invertSourceMap.bind(this);this._encodeAndSetSourceMapURL=this._encodeAndSetSourceMapURL.bind(this);this._getSourceText=this._getSourceText.bind(this);this._mapSourceToAddon();if(this.threadActor.sources.isPrettyPrinted(this.url)){this._init=this.prettyPrint(this.threadActor.sources.prettyPrintIndent(this.url)).then(null,error=>{DevToolsUtils.reportException("SourceActor",error);});}else{this._init=null;}},get isSourceMapped(){return!!(!this.isInlineSource&&(this._originalURL||this._generatedSource||this.threadActor.sources.isPrettyPrinted(this.url)));},get isInlineSource(){return this._isInlineSource;},get threadActor(){return this._threadActor;},get sources(){return this._threadActor.sources;},get dbg(){return this.threadActor.dbg;},get scripts(){return this.threadActor.scripts;},get source(){return this._source;},get generatedSource(){return this._generatedSource;},get breakpointActorMap(){return this.threadActor.breakpointActorMap;},get url(){if(this.source){return getSourceURL(this.source,this.threadActor._parent.window);}
return this._originalUrl;},get addonID(){return this._addonID;},get addonPath(){return this._addonPath;},get prettyPrintWorker(){return this.threadActor.prettyPrintWorker;},form:function(){let source=this.source||this.generatedSource;

 let introductionUrl=null;if(source&&source.introductionScript){introductionUrl=source.introductionScript.source.url;}
return{actor:this.actorID,generatedUrl:this.generatedSource?this.generatedSource.url:null,url:this.url?this.url.split(" -> ").pop():null,addonID:this._addonID,addonPath:this._addonPath,isBlackBoxed:this.threadActor.sources.isBlackBoxed(this.url),isPrettyPrinted:this.threadActor.sources.isPrettyPrinted(this.url),isSourceMapped:this.isSourceMapped,introductionUrl:introductionUrl?introductionUrl.split(" -> ").pop():null,introductionType:source?source.introductionType:null};},disconnect:function(){if(this.registeredPool&&this.registeredPool.sourceActors){delete this.registeredPool.sourceActors[this.actorID];}},_mapSourceToAddon:function(){try{var nsuri=Services.io.newURI(this.url.split(" -> ").pop(),null,null);}
catch(e){ return;}
let localURI=resolveURIToLocalPath(nsuri);let id={};if(localURI&&mapURIToAddonID(localURI,id)){this._addonID=id.value;if(localURI instanceof Ci.nsIJARURI){ this._addonPath=localURI.JAREntry;}
else if(localURI instanceof Ci.nsIFileURL){
 let target=localURI.file;let path=target.leafName;
 let root=target.parent;let file=root.parent;while(file&&mapURIToAddonID(Services.io.newFileURI(file),{})){path=root.leafName+"/"+path;root=file;file=file.parent;}
if(!file){const error=new Error("Could not find the root of the add-on for "+this.url);DevToolsUtils.reportException("SourceActor.prototype._mapSourceToAddon",error)
return;}
this._addonPath=path;}}},_reportLoadSourceError:function(error,map=null){try{DevToolsUtils.reportException("SourceActor",error);JSON.stringify(this.form(),null,4).split(/\n/g).forEach(line=>console.error("\t",line));if(!map){return;}
console.error("\t","source map's sourceRoot =",map.sourceRoot);console.error("\t","source map's sources =");map.sources.forEach(s=>{let hasSourceContent=map.sourceContentFor(s,true);console.error("\t\t",s,"\t",hasSourceContent?"has source content":"no source content");});console.error("\t","source map's sourcesContent =");map.sourcesContent.forEach(c=>{if(c.length>80){c=c.slice(0,77)+"...";}
c=c.replace(/\n/g,"\\n");console.error("\t\t",c);});}catch(e){}},_getSourceText:function(){let toResolvedContent=t=>({content:t,contentType:this._contentType});let genSource=this.generatedSource||this.source;return this.threadActor.sources.fetchSourceMap(genSource).then(map=>{if(map){try{let sourceContent=map.sourceContentFor(this.url);if(sourceContent){return toResolvedContent(sourceContent);}}catch(error){this._reportLoadSourceError(error,map);throw error;}}





if(this.source&&this.source.text!=="[no source]"&&this._contentType&&(this._contentType.indexOf("javascript")!==-1||this._contentType==="text/wasm")){return toResolvedContent(this.source.text);}
else{




let sourceFetched=fetch(this.url,{loadFromCache:this.isInlineSource}); return sourceFetched.then(result=>{this._contentType=result.contentType;return result;},error=>{this._reportLoadSourceError(error,map);throw error;});}});},getExecutableLines:method(function(){function sortLines(lines){ lines=[...lines];lines.sort((a,b)=>{return a-b;});return lines;}
if(this.generatedSource){return this.threadActor.sources.getSourceMap(this.generatedSource).then(sm=>{let lines=new Set(); let offsets=this.getExecutableOffsets(this.generatedSource,false);for(let offset of offsets){let{line,source:sourceUrl}=sm.originalPositionFor({line:offset.lineNumber,column:offset.columnNumber});if(sourceUrl===this.url){lines.add(line);}}
return sortLines(lines);});}
let lines=this.getExecutableOffsets(this.source,true);return sortLines(lines);},{response:{lines:RetVal("json")}}),getExecutableOffsets:function(source,onlyLine){let offsets=new Set();for(let s of this.threadActor.scripts.getScriptsBySource(source)){for(let offset of s.getAllColumnOffsets()){offsets.add(onlyLine?offset.lineNumber:offset);}}
return offsets;},onSource:method(function(){return resolve(this._init).then(this._getSourceText).then(({content,contentType})=>{return{source:createValueGrip(content,this.threadActor.threadLifetimePool,this.threadActor.objectGrip),contentType:contentType};}).then(null,aError=>{reportError(aError,"Got an exception during SA_onSource: ");throw new Error("Could not load the source for "+this.url+".\n"+
DevToolsUtils.safeErrorString(aError));});},{request:{type:"source"},response:RetVal("json")}),prettyPrint:method(function(indent){this.threadActor.sources.prettyPrint(this.url,indent);return this._getSourceText().then(this._sendToPrettyPrintWorker(indent)).then(this._invertSourceMap).then(this._encodeAndSetSourceMapURL).then(()=>{

this._init=null;}).then(this.onSource).then(null,error=>{this.disablePrettyPrint();throw new Error(DevToolsUtils.safeErrorString(error));});},{request:{indent:Arg(0,"number")},response:RetVal("json")}),_sendToPrettyPrintWorker:function(aIndent){return({content})=>{return this.prettyPrintWorker.performTask("pretty-print",{url:this.url,indent:aIndent,source:content})};},_invertSourceMap:function({code,mappings}){const generator=new SourceMapGenerator({file:this.url});return DevToolsUtils.yieldingEach(mappings._array,m=>{let mapping={generated:{line:m.originalLine,column:m.originalColumn}};if(m.source){mapping.source=m.source;mapping.original={line:m.generatedLine,column:m.generatedColumn};mapping.name=m.name;}
generator.addMapping(mapping);}).then(()=>{generator.setSourceContent(this.url,code);let consumer=SourceMapConsumer.fromSourceMap(generator);return{code:code,map:consumer};});},_encodeAndSetSourceMapURL:function({map:sm}){let source=this.generatedSource||this.source;let sources=this.threadActor.sources;return sources.getSourceMap(source).then(prevMap=>{if(prevMap){ this._oldSourceMapping={url:source.sourceMapURL,map:prevMap};prevMap=SourceMapGenerator.fromSourceMap(prevMap);prevMap.applySourceMap(sm,this.url);sm=SourceMapConsumer.fromSourceMap(prevMap);}
let sources=this.threadActor.sources;sources.clearSourceMapCache(source.sourceMapURL);sources.setSourceMapHard(source,null,sm);});},disablePrettyPrint:method(function(){let source=this.generatedSource||this.source;let sources=this.threadActor.sources;let sm=sources.getSourceMap(source);sources.clearSourceMapCache(source.sourceMapURL,{hard:true});if(this._oldSourceMapping){sources.setSourceMapHard(source,this._oldSourceMapping.url,this._oldSourceMapping.map);this._oldSourceMapping=null;}
this.threadActor.sources.disablePrettyPrint(this.url);return this.onSource();},{response:RetVal("json")}),blackbox:method(function(){this.threadActor.sources.blackBox(this.url);if(this.threadActor.state=="paused"&&this.threadActor.youngestFrame&&this.threadActor.youngestFrame.script.url==this.url){return true;}
return false;},{response:{pausedInSource:RetVal("boolean")}}),unblackbox:method(function(){this.threadActor.sources.unblackBox(this.url);}),setBreakpoint:method(function(line,column,condition){if(this.threadActor.state!=="paused"){throw{error:"wrongState",message:"Cannot set breakpoint while debuggee is running."};}
let location=new OriginalLocation(this,line,column);return this._getOrCreateBreakpointActor(location,condition).then((actor)=>{let response={actor:actor.actorID,isPending:actor.isPending};let actualLocation=actor.originalLocation;if(!actualLocation.equals(location)){response.actualLocation=actualLocation.toJSON();}
return response;});},{request:{location:{line:Arg(0,"number"),column:Arg(1,"nullable:number")},condition:Arg(2,"nullable:string")},response:RetVal("json")}),_getOrCreateBreakpointActor:function(originalLocation,condition){let actor=this.breakpointActorMap.getActor(originalLocation);if(!actor){actor=new BreakpointActor(this.threadActor,originalLocation);this.threadActor.threadLifetimePool.addActor(actor);this.breakpointActorMap.setActor(originalLocation,actor);}
actor.condition=condition;return this._setBreakpoint(actor);},_setBreakpoint:function(actor){const{originalLocation}=actor;const{originalLine,originalSourceActor}=originalLocation;if(!this.isSourceMapped){if(!this._setBreakpointAtGeneratedLocation(actor,GeneratedLocation.fromOriginalLocation(originalLocation))){const scripts=this.scripts.getScriptsBySourceActorAndLine(this,originalLine);










if(originalLocation.originalColumn||scripts.length===0){return promise.resolve(actor);}

const largestScript=scripts.reduce((largestScript,script)=>{if(script.lineCount>largestScript.lineCount){return script;}
return largestScript;});const maxLine=largestScript.startLine+largestScript.lineCount-1;let actualLine=originalLine;for(;actualLine<=maxLine;actualLine++){const loc=new GeneratedLocation(this,actualLine);if(this._setBreakpointAtGeneratedLocation(actor,loc)){break;}}


assert(actualLine<=maxLine,"Could not find any entry points to set a breakpoint on, "+"even though I was told a script existed on the line I started "+"the search with.");
const actualLocation=new OriginalLocation(originalSourceActor,actualLine);const existingActor=this.breakpointActorMap.getActor(actualLocation);this.breakpointActorMap.deleteActor(originalLocation);if(existingActor){actor.delete();actor=existingActor;}else{actor.originalLocation=actualLocation;this.breakpointActorMap.setActor(actualLocation,actor);}}
return promise.resolve(actor);}else{return this.sources.getAllGeneratedLocations(originalLocation).then((generatedLocations)=>{this._setBreakpointAtAllGeneratedLocations(actor,generatedLocations);return actor;});}},_setBreakpointAtAllGeneratedLocations:function(actor,generatedLocations){let success=false;for(let generatedLocation of generatedLocations){if(this._setBreakpointAtGeneratedLocation(actor,generatedLocation)){success=true;}}
return success;},_setBreakpointAtGeneratedLocation:function(actor,generatedLocation){let{generatedSourceActor,generatedLine,generatedColumn,generatedLastColumn}=generatedLocation;let scripts=this.scripts.getScriptsBySourceActorAndLine(generatedSourceActor,generatedLine);scripts=scripts.filter((script)=>!actor.hasScript(script));let entryPoints=[];if(generatedColumn===undefined){
for(let script of scripts){let offsets=script.getLineOffsets(generatedLine);if(offsets.length>0){entryPoints.push({script,offsets});}}}else{
for(let script of scripts){let columnToOffsetMap=script.getAllColumnOffsets().filter(({lineNumber})=>{return lineNumber===generatedLine;});for(let{columnNumber:column,offset}of columnToOffsetMap){if(column>=generatedColumn&&column<=generatedLastColumn){entryPoints.push({script,offsets:[offset]});}}}}
if(entryPoints.length===0){return false;}
setBreakpointAtEntryPoints(actor,entryPoints);return true;}});exports.SourceActor=SourceActor;