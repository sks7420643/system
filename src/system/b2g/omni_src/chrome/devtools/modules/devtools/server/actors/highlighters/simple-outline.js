"use strict";const{installHelperSheet,addPseudoClassLock,removePseudoClassLock}=require("./utils/markup");const HIGHLIGHTED_PSEUDO_CLASS=":-moz-devtools-highlighted";const SIMPLE_OUTLINE_SHEET=`.__fx-devtools-hide-shortcut__ {
                                visibility: hidden !important
                              }
                              ${HIGHLIGHTED_PSEUDO_CLASS} {
                                outline: 2px dashed #F06!important;
                                outline-offset: -2px!important
                              }`;function SimpleOutlineHighlighter(highlighterEnv){this.chromeDoc=highlighterEnv.document;}
SimpleOutlineHighlighter.prototype={destroy:function(){this.hide();this.chromeDoc=null;},show:function(node){if(!this.currentNode||node!==this.currentNode){this.hide();this.currentNode=node;installHelperSheet(node.ownerDocument.defaultView,SIMPLE_OUTLINE_SHEET);addPseudoClassLock(node,HIGHLIGHTED_PSEUDO_CLASS);}
return true;},hide:function(){if(this.currentNode){removePseudoClassLock(this.currentNode,HIGHLIGHTED_PSEUDO_CLASS);this.currentNode=null;}}};exports.SimpleOutlineHighlighter=SimpleOutlineHighlighter;