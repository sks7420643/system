"use strict";const{Cu}=require("chrome");const Services=require("Services");const EXPAND_TAB="devtools.editor.expandtab";const TAB_SIZE="devtools.editor.tabsize";const DETECT_INDENT="devtools.editor.detectindentation";const DETECT_INDENT_MAX_LINES=500;function getIndentationFromPrefs(){let shouldDetect=Services.prefs.getBoolPref(DETECT_INDENT);if(shouldDetect){return false;}
let indentWithTabs=!Services.prefs.getBoolPref(EXPAND_TAB);let indentUnit=Services.prefs.getIntPref(TAB_SIZE);return{indentUnit,indentWithTabs};}
function getIndentationFromIteration(iterFunc){let indentWithTabs=!Services.prefs.getBoolPref(EXPAND_TAB);let indentUnit=Services.prefs.getIntPref(TAB_SIZE);let shouldDetect=Services.prefs.getBoolPref(DETECT_INDENT);if(shouldDetect){let indent=detectIndentation(iterFunc);if(indent!=null){indentWithTabs=indent.tabs;indentUnit=indent.spaces?indent.spaces:indentUnit;}}
return{indentUnit,indentWithTabs};}
function getIndentationFromString(string){let iteratorFn=function(start,end,callback){let split=string.split(/\r\n|\r|\n|\f/);split.slice(start,end).forEach(callback);};return getIndentationFromIteration(iteratorFn);}
function detectIndentation(textIteratorFn){ let spaces={}; let last=0; let tabs=0;let total=0;textIteratorFn(0,DETECT_INDENT_MAX_LINES,(text)=>{if(text.startsWith("\t")){tabs++;total++;return;}
let width=0;while(text[width]===" "){width++;} 
if(width==text.length){last=0;return;}
if(width>1){total++;} 
let indent=Math.abs(width-last);if(indent>1&&indent<=8){spaces[indent]=(spaces[indent]||0)+1;}
last=width;}); if(total==0){return null;} 
if(tabs>=total/2){return{tabs:true};} 
let freqIndent=null,max=1;for(let width in spaces){width=parseInt(width,10);let tally=spaces[width];if(tally>max){max=tally;freqIndent=width;}}
if(!freqIndent){return null;}
return{tabs:false,spaces:freqIndent};}
exports.EXPAND_TAB=EXPAND_TAB;exports.TAB_SIZE=TAB_SIZE;exports.DETECT_INDENT=DETECT_INDENT;exports.getIndentationFromPrefs=getIndentationFromPrefs;exports.getIndentationFromIteration=getIndentationFromIteration;exports.getIndentationFromString=getIndentationFromString;