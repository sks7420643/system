"use strict";const{Cc,Ci}=require("chrome");const SPECIALVALUES=new Set(["initial","inherit","unset"]);function CssAngle(angleValue){this.newAngle(angleValue);}
module.exports.angleUtils={CssAngle:CssAngle,classifyAngle:classifyAngle};CssAngle.ANGLEUNIT={"deg":"deg","rad":"rad","grad":"grad","turn":"turn"};CssAngle.prototype={_angleUnit:null,_angleUnitUppercase:false,authored:null,lowerCased:null,get angleUnit(){if(this._angleUnit===null){this._angleUnit=classifyAngle(this.authored);}
return this._angleUnit;},set angleUnit(unit){this._angleUnit=unit;},get valid(){return/^-?\d+\.?\d*(deg|rad|grad|turn)$/gi.test(this.authored);},get specialValue(){return SPECIALVALUES.has(this.lowerCased)?this.authored:null;},get deg(){let invalidOrSpecialValue=this._getInvalidOrSpecialValue();if(invalidOrSpecialValue!==false){return invalidOrSpecialValue;}
let angleUnit=classifyAngle(this.authored);if(angleUnit===CssAngle.ANGLEUNIT.deg){return this.authored;}
let degValue;if(angleUnit===CssAngle.ANGLEUNIT.rad){degValue=this.authoredAngleValue/(Math.PI/180);}
if(angleUnit===CssAngle.ANGLEUNIT.grad){degValue=this.authoredAngleValue*0.9;}
if(angleUnit===CssAngle.ANGLEUNIT.turn){degValue=this.authoredAngleValue*360;}
let unitStr=CssAngle.ANGLEUNIT.deg;if(this._angleUnitUppercase===true){unitStr=unitStr.toUpperCase();}
return`${Math.round(degValue * 100) / 100}${unitStr}`;},get rad(){let invalidOrSpecialValue=this._getInvalidOrSpecialValue();if(invalidOrSpecialValue!==false){return invalidOrSpecialValue;}
let unit=classifyAngle(this.authored);if(unit===CssAngle.ANGLEUNIT.rad){return this.authored;}
let radValue;if(unit===CssAngle.ANGLEUNIT.deg){radValue=this.authoredAngleValue*(Math.PI/180);}
if(unit===CssAngle.ANGLEUNIT.grad){radValue=this.authoredAngleValue*0.9*(Math.PI/180);}
if(unit===CssAngle.ANGLEUNIT.turn){radValue=this.authoredAngleValue*360*(Math.PI/180);}
let unitStr=CssAngle.ANGLEUNIT.rad;if(this._angleUnitUppercase===true){unitStr=unitStr.toUpperCase();}
return`${Math.round(radValue * 10000) / 10000}${unitStr}`;},get grad(){let invalidOrSpecialValue=this._getInvalidOrSpecialValue();if(invalidOrSpecialValue!==false){return invalidOrSpecialValue;}
let unit=classifyAngle(this.authored);if(unit===CssAngle.ANGLEUNIT.grad){ return this.authored;}
let gradValue;if(unit===CssAngle.ANGLEUNIT.deg){ gradValue=this.authoredAngleValue/0.9;}
if(unit===CssAngle.ANGLEUNIT.rad){ gradValue=this.authoredAngleValue/0.9/(Math.PI/180);}
if(unit===CssAngle.ANGLEUNIT.turn){ gradValue=this.authoredAngleValue*400;}
let unitStr=CssAngle.ANGLEUNIT.grad;if(this._angleUnitUppercase===true){unitStr=unitStr.toUpperCase();}
return`${Math.round(gradValue * 100) / 100}${unitStr}`;},get turn(){let invalidOrSpecialValue=this._getInvalidOrSpecialValue();if(invalidOrSpecialValue!==false){return invalidOrSpecialValue;}
let unit=classifyAngle(this.authored);if(unit===CssAngle.ANGLEUNIT.turn){ return this.authored;}
let turnValue;if(unit===CssAngle.ANGLEUNIT.deg){ turnValue=this.authoredAngleValue/360;}
if(unit===CssAngle.ANGLEUNIT.rad){ turnValue=(this.authoredAngleValue/(Math.PI/180))/360;}
if(unit===CssAngle.ANGLEUNIT.grad){ turnValue=this.authoredAngleValue/400;}
let unitStr=CssAngle.ANGLEUNIT.turn;if(this._angleUnitUppercase===true){unitStr=unitStr.toUpperCase();}
return`${Math.round(turnValue * 100) / 100}${unitStr}`;},_getInvalidOrSpecialValue:function(){if(this.specialValue){return this.specialValue;}
if(!this.valid){return"";}
return false;},newAngle:function(angle){

this.lowerCased=angle.toLowerCase();this._angleUnitUppercase=(angle===angle.toUpperCase());this.authored=angle;let reg=new RegExp(`(${Object.keys(CssAngle.ANGLEUNIT).join("|")})$`,"i");let unitStartIdx=angle.search(reg);this.authoredAngleValue=angle.substring(0,unitStartIdx);this.authoredAngleUnit=angle.substring(unitStartIdx,angle.length);return this;},nextAngleUnit:function(){
let formats=Object.keys(CssAngle.ANGLEUNIT);let putOnEnd=formats.splice(0,formats.indexOf(this.angleUnit));formats=formats.concat(putOnEnd);let currentDisplayedValue=this[formats[0]];for(let format of formats){if(this[format].toLowerCase()!==currentDisplayedValue.toLowerCase()){this.angleUnit=CssAngle.ANGLEUNIT[format];break;}}
return this.toString();},toString:function(){let angle;switch(this.angleUnit){case CssAngle.ANGLEUNIT.deg:angle=this.deg;break;case CssAngle.ANGLEUNIT.rad:angle=this.rad;break;case CssAngle.ANGLEUNIT.grad:angle=this.grad;break;case CssAngle.ANGLEUNIT.turn:angle=this.turn;break;default:angle=this.deg;}
if(this._angleUnitUppercase&&this.angleUnit!=CssAngle.ANGLEUNIT.authored){angle=angle.toUpperCase();}
return angle;},valueOf:function(){return this.deg;},};function classifyAngle(value){value=value.toLowerCase();if(value.endsWith("deg")){return CssAngle.ANGLEUNIT.deg;}
if(value.endsWith("grad")){return CssAngle.ANGLEUNIT.grad;}
if(value.endsWith("rad")){return CssAngle.ANGLEUNIT.rad;}
if(value.endsWith("turn")){return CssAngle.ANGLEUNIT.turn;}
return CssAngle.ANGLEUNIT.deg;}