/* © 2017 KAI OS TECHNOLOGIES (HONG KONG) LIMITED All rights reserved.
 * This file or any portion thereof may not be reproduced or used in any manner
 * whatsoever without the express written permission of KAI OS TECHNOLOGIES
 * (HONG KONG) LIMITED. KaiOS is the trademark of KAI OS TECHNOLOGIES (HONG KONG)
 * LIMITED or its affiliate company and may be registered in some jurisdictions.
 * All other trademarks are the property of their respective owners.
 */
// ************************************************************************
// * File Name: flashlight.js
// * Description: mmitest -> test item: Flashlight.
// * Note:
// ************************************************************************

/* global TestItem */
'use strict';

function $(id) {
  return document.getElementById(id);
}

var Flashlight = new TestItem();
Flashlight.onInit = function() {
  this.startTest();
};

Flashlight.startTest = function() {
  this.passButton.disabled = 'disabled';
  this.failButton.disabled = 'disabled';

  navigator.engmodeExtension.startUniversalCommand('echo 1 > /sys/class/leds/flashlight/brightness', true);
/*  if (navigator.mozPower.flashLight) {
    navigator.mozPower.flashLight();
  }*/
  window.setTimeout(this.timeOutCallback.bind(this), 500);
};

Flashlight.timeOutCallback = function() {
  this.passButton.disabled = '';
  this.failButton.disabled = '';
};

Flashlight.onHandleEvent = function(evt) {
  evt.preventDefault();
  return false;
};

Flashlight.onDeinit = function() {
  navigator.engmodeExtension.startUniversalCommand('echo 0 > /sys/class/leds/flashlight/brightness', true);
/*  if (navigator.mozPower.flashLight) {
    navigator.mozPower.flashLight();
  }*/
};

window.addEventListener('load', Flashlight.init.bind(Flashlight));
window.addEventListener('beforeunload', Flashlight.uninit.bind(Flashlight));
window.addEventListener('keydown', Flashlight.handleKeydown.bind(Flashlight));
