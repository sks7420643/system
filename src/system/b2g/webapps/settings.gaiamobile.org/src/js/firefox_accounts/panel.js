/* global NavigationMap, SettingsHelper, SettingsSoftkey, Toaster,
          Normalizer */
/* exported FxaPanel */



var FxaPanel = (function fxa_panel() {
  var _ = navigator.mozL10n.get,
    fxaContainer,
    phoneUnverifiedPanel,
    loggedOutPanel,
    loggedInPanel,
    unverifiedPanel,
    cancelBtn,
    loginBtn,
    logoutBtn,
    loggedInEmail,
    unverifiedEmail,
    resendEmail,
    fxaHelper,
    createAccountBtn,
    resetPasswordBtn,
    resendMailBtn,
    birthdayContent,
    genderContent,
    headTitle,
    birthday;
  var setPasswordUrl;
  var SignIned = false;
  var NeedVerify = false;
  var Signing = false;
  function init(fxAccountsIACHelper) {
    // allow mock to be passed in for unit testing
    fxaHelper = fxAccountsIACHelper;
    fxaContainer = document.getElementById('fxa');
    phoneUnverifiedPanel = document.getElementById('phone-unverified');
    loggedOutPanel = document.getElementById('fxa-logged-out');
    loggedInPanel = document.getElementById('fxa-logged-in');
    unverifiedPanel = document.getElementById('fxa-unverified');
    resendEmail = document.getElementById('fxa-resend-email');
    cancelBtn = document.getElementById('fxa-cancel-confirmation');
    loginBtn = document.getElementById('fxa-login');
    createAccountBtn = document.getElementById('fxa-createaAccount');
    logoutBtn = document.getElementById('fxa-logout');
    loggedInEmail = document.getElementById('fxa-logged-in-text');
    unverifiedEmail = document.getElementById('fxa-unverified-text');

    createAccountBtn = document.getElementById('fxa-create-account-btn');
    loginBtn = document.getElementById('fxa-login');
    resetPasswordBtn = document.getElementById('fxa-reset-password');
    resendMailBtn = document.getElementById('fxa-resend-mail');
    headTitle = document.getElementById('fxa-accounts-header');

    birthdayContent = document.getElementById('fxa-personal-info-birthday');
    genderContent = document.getElementById('fxa-personal-info-gender');

    var fxaSettingsHelper =
      SettingsHelper('identity.fxaccounts.reset-password.url');

    fxaSettingsHelper.get(function on_fxa_get_settings(url) {
      setPasswordUrl = url;
    });
    // listen for changes
    onVisibilityChange();
    // start by checking current status
    refreshStatus();
    document.addEventListener('visibilitychange', onVisibilityChange);
    window.addEventListener('panelready', function (e) {
      if (e.detail.current == '#fxa') {
        if (Signing) {
          initSignInSoftKey();
        } else {
          initSoftKey();
        }
      }
      if (e.detail.previous == '#fxa') {
        SettingsSoftkey.hide();
      }
    });

  }

  function onVisibilityChange(hide) {
    var panel = Settings._currentPanel;
    // mozId can be logged in fxa findmydevice developer > first time launch
    if (['#fxa', '#findmydevice', '#developer'].indexOf(panel) > -1) {
      if (document.hidden || hide === true) {
        fxaHelper.removeEventListener('onlogin', refreshStatus);
        fxaHelper.removeEventListener('onverified', refreshStatus);
        fxaHelper.removeEventListener('onlogout', refreshStatus);
      } else if (!document.hidden || hide === false) {
        fxaHelper.addEventListener('onlogin', refreshStatus);
        fxaHelper.addEventListener('onverified', refreshStatus);
        fxaHelper.addEventListener('onlogout', refreshStatus);
        refreshStatus();
      } else if (document.hidden && hide === false) {
        refreshStatus();
      }
    }
  }

  function refreshStatus() {
    fxaHelper.getAccounts(onFxAccountStateChange, onFxAccountError);
  }

  // if e == null, user is logged out.
  // if e.verified, user is logged in & verified.
  // if !e.verified, user is logged in & unverified.
  function onFxAccountStateChange(e) {
    var email, gender = '';
    if (e) {
      email = Normalizer.escapeHTML(e.email || e.accountId) || '';
      birthday = e.birthday || '';
      gender = e.gender || '';
    }

    Signing = false;
    if (!e) {
      NeedVerify = false;
      SignIned = false;
      hideLoggedInPanel();
      hideUnverifiedPanel();
      showLoggedOutPanel();
      initSoftKey();
      headTitle.setAttribute('data-l10n-id', 'fxa-accounts-header');
    } else if (e.verified) {
      NeedVerify = false;
      Signing = true;
      hideLoggedOutPanel();
      hideUnverifiedPanel();
      showLoggedInPanel(email, gender);
      initSignInSoftKey();
      headTitle.setAttribute('data-l10n-id', 'fxa-accounts-header-signed');
    } else {
      headTitle.setAttribute('data-l10n-id', 'fxa-accounts-header-signed');
      NeedVerify = true;
      hideLoggedOutPanel();
      hideLoggedInPanel();
      showUnverifiedPanel(email);
      hidePhoneVerificationPanel();
      initSoftKey();
    }

    var evt = new CustomEvent('panelready', {
      detail: {
        current: '#fxa'
      }
    });
    if (Settings._currentPanel === '#fxa') {
      window.dispatchEvent(evt);
    }
  }

  function onFxAccountError(err) {
    console.error('FxaPanel: Error getting Firefox Account: ' + err.error);
  }

  function onResetPasswordClick(e) {
    e.stopPropagation();
    e.preventDefault();
    onVisibilityChange(true);
    window.open(setPasswordUrl, '', 'dialog');
  }

  function hidePhoneVerificationPanel() {
    phoneUnverifiedPanel.hidden = true;
  }

  function showPhoneVerificationPanel() {
    phoneUnverifiedPanel.hidden = false;
  }

  function hideLoggedOutPanel() {
    loginBtn.onclick = null;
    loggedOutPanel.hidden = true;
    createAccountBtn.onclick = null;
  }

  function showLoggedOutPanel() {
    loginBtn.onclick = onLoginClick;
    loggedOutPanel.hidden = false;
    createAccountBtn.onclick = onCreateAccountClick;
  }

  function hideLoggedInPanel() {
    loggedInPanel.hidden = true;
    loggedInEmail.textContent = '';
    window.removeEventListener('localized', localizedDate);
  }

  function localizedDate () {
    var formatedDate = new Date(birthday);
    var dateLanguage = navigator.language;
    var localizedbirthday;
    if (!isNaN(formatedDate.getTime())) {
      var option = {
        month: 'long',
        day: 'numeric',
        year: 'numeric',
        timeZone: 'UTC'
      };
      localizedbirthday = formatedDate.toLocaleString(dateLanguage, option);
    }
    birthdayContent.textContent = localizedbirthday;
  }

  function showLoggedInPanel(email, gender) {
    navigator.mozL10n.setAttributes(loggedInEmail, 'fxa-logged-in-text', {
      email: email
    });

    localizedDate();
    window.addEventListener('localized', localizedDate);

    navigator.mozL10n.setAttributes(
      genderContent,
      'fxa-gender-' + gender.toLowerCase());
    loggedInPanel.hidden = false;
  }

  function hideUnverifiedPanel() {
    unverifiedPanel.hidden = true;
    unverifiedEmail.textContent = '';
    resendMailBtn.onclick = null;
  }

  function showUnverifiedPanel(email) {
    unverifiedPanel.hidden = false;
    navigator.mozL10n.setAttributes(
      unverifiedEmail,
      'fxa-verification-email-sent-msg',
      { email: email }
    );
    // dynamically construct the resend link
    var dontSeeText = _('fxa-dont-see-email');
    resendMailBtn.onclick = _onResendClick;
  }

  function onLogoutClick(e) {
    e.stopPropagation();
    e.preventDefault();
    fxaHelper.getAccounts(function onGetAccounts(accts) {
      var email = accts && (accts.email || accts.accountId);
      if (!email) {
        return onFxAccountStateChange(accts);
      }
      fxaHelper.signOut(email,refreshStatus, onFxAccountError);
    }, onFxAccountError);
  }

  function _onResendClick(e) {
    e.stopPropagation();
    e.preventDefault();
    if (resendMailBtn.classList.contains('disabled')) {
      return;
    }
    if (!navigator.onLine) {
      var toast = {
        messageL10nId: 'fxa-no-internet-connection',
        latency: 2000,
        useTransition: true
      };
      Toaster.showToast(toast);
      return;
    }
    fxaHelper.getAccounts(function onGetAccounts(accts) {
      var email = accts && (accts.email || accts.accountId);
      if (!email) {
        return onFxAccountStateChange(accts);
      }
      fxaHelper.resendVerificationEmail(email, function (result) {
        _onResend(result, email);
      }, onFxAccountError);
    }, onFxAccountError);
  }

  function _onResend(result, email) {
    var toast = {
      messageL10nId: 'fxa-resend-alert',
      messageL10nArgs: { email: email },
      latency: 2000,
      useTransition: true
    };
    Toaster.showToast(toast);
    // disable link for 60 seconds, then re-enable
    resendMailBtn.classList.add('disabled');
    resendMailBtn.parentNode.parentNode.classList.add('disabled');
    setTimeout(function enableResendMailBtn() {
      resendMailBtn.classList.remove('disabled');
      resendMailBtn.parentNode.parentNode.classList.remove('disabled');
    }, 60000);
    fxaHelper.openFlow(function (accts) {
      setOrRefresh(accts.success);
    }, function () { });
  }

  function setOrRefresh(success) {
    if (success === true) {
      SettingsHelper('antitheft.enabled').set(true);
    }
    if (success !== undefined) {
      SignIned = true;
      refreshStatus();
    }
  }

  function onLoginClick(e) {
    e.stopPropagation();
    e.preventDefault();
    onVisibilityChange(true);
    NavigationMap.delayFocusSet = true;
    fxaHelper.openFlow(function (accts) {
      setOrRefresh(accts.success);
    }, function () { });
  }

  function onCreateAccountClick(e) {
    e.stopPropagation();
    e.preventDefault();
    onVisibilityChange(true);
    NavigationMap.delayFocusSet = true;
    fxaHelper.createAccount(function () {
      refreshStatus();
    }, function () { });
  }

  function onPhoneVerificationClick() {
    onVisibilityChange(true);
    NavigationMap.delayFocusSet = true;
    fxaHelper.phoneVerification(() => {
      refreshStatus();
      NavigationMap.navigateBack();
    }, () => {
      NavigationMap.navigateBack();
    });
  }

  function initSoftKey() {
    var softkeyParams = {
      menuClassName: 'menu-button',
      header: { l10nId: 'fxa-options' },
      items: [
        {
          name: 'Cancel',
          l10nId: 'cancel',
          priority: 1,
          method: function () {
            if (NeedVerify) {
              NavigationMap.delayFocusSet = true;
              fxaHelper.openFlow(function () {
                SignIned = true;
                refreshStatus();
              }, function () { });
              return;
            } else {
              NavigationMap.navigateBack();
            }
          }
        },
        {
          name: 'Select',
          l10nId: 'select',
          priority: 2,
          method: function () {
          }
        }
      ]
    };
    SettingsSoftkey.init(softkeyParams);
    SettingsSoftkey.show();
  }

  function initSignInSoftKey() {
    var SignInsoftkeyParams = {
      menuClassName: 'menu-button',
      header: { l10nId: 'fxa-options' },
      items: [
        {
          name: 'Sign out',
          l10nId: 'sign-out',
          priority: 5,
          method: function () {
            fxaHelper.getAccounts(function onGetAccounts(accts) {
              var email = accts && (accts.email || accts.accountId);
              if (!email) {
                return onFxAccountStateChange(accts);
              }
              fxaHelper.signOut(email, function(result) {
                if (result.success) {
                  SettingsHelper('antitheft.enabled').set(false);
                }
                refreshStatus();
                var toast = {
                  messageL10nId: result.success ?
                    'fxa-sign-out-success' : 'fxa-sign-out-unsuccess',
                  latency: 2000,
                  useTransition: true
                };
                Toaster.showToast(toast);
              }, onFxAccountError);
            }, onFxAccountError);
          }
        },
        {
          name: 'Change password',
          l10nId: 'change-password',
          priority: 5,
          method: function () {
            fxaHelper.getAccounts(function onGetAccounts(accts) {
              var email = accts && (accts.email || accts.accountId);
              if (!email) {
                return onFxAccountStateChange(accts);
              }
              fxaHelper.changePassword(email, function(result) {
                if (result.success) {
                  refreshStatus();
                  var toast = {
                    messageL10nId: 'fxa-password-changed-successfully',
                    latency: 2000,
                    useTransition: true
                  };
                  Toaster.showToast(toast);
                }
              }, onFxAccountError);
            }, onFxAccountError);
          }
        }
      ]
    };
    SettingsSoftkey.init(SignInsoftkeyParams);
    SettingsSoftkey.show();
  }
  return {
    init: init,
    onVisibilityChange: onVisibilityChange,
    // exposed for testing
    _onResendClick: _onResendClick,
    _onResend: _onResend
  };

})();
