
define('panels/wifi_status/panel',['require','modules/dialog_panel','shared/wifi_helper'],function(require) {
  

  var DialogPanel = require('modules/dialog_panel');
  var WifiHelper = require('shared/wifi_helper');
  var wifiManager = WifiHelper.getWifiManager();

  return function ctor_statusWifi() {
    var elements = {};

    return DialogPanel({
      onInit: function(panel) {
        elements = {};
        elements.panel = panel;
        elements.ip = panel.querySelector('[data-ip]');
        elements.speed = panel.querySelector('[data-speed]');
        elements.ssid = panel.querySelector('[data-ssid]');
        elements.signal = panel.querySelector('[data-signal]');
        elements.security = panel.querySelector('[data-security]');
        elements.forgetNetworkDialog = panel.querySelector('form');
        this.keydownHandler = this._keydownHandler.bind(this);
        this.handleVisibiltychange = this._handleVisibiltychange.bind(this);
      },
      onBeforeShow: function(panel, options) {
        this._updateNetworkInfo();
        elements.ssid.textContent = options.network.ssid;
        elements.signal.setAttribute('data-l10n-id',
          'signalLevel' + options.sl);

        var wifiSecurity = elements.panel.querySelector('.wifi-security');
        wifiSecurity.hidden = true;

        wifiManager.onconnectioninfoupdate = this._updateNetworkInfo;
        this._initpanelready();
        this._initSoftKey();
        this._dispatchDialogShowEvent();
        document.addEventListener('visibilitychange', this.handleVisibiltychange);
        window.addEventListener('keydown', this.keydownHandler);
      },
      onBeforeHide: function() {
        wifiManager.onconnectioninfoupdate = null;
        SettingsSoftkey.hide();
        var evt = new CustomEvent("dialogpanelhide", {
          detail: {
            dialogpanel: "#" + elements.panel.id
          }
        });
        window.dispatchEvent(evt);
        window.removeEventListener('keydown', this.keydownHandler);
        document.removeEventListener('visibilitychange', this.handleVisibiltychange);
      },
      _keydownHandler: function(e) {
        switch (e.key) {
          case 'BrowserBack':
          case 'Backspace':
          case 'KanjiMode':
            elements.panel.querySelector('button[type="reset"]').click();
            break;
        }
      },
      _initpanelready: function() {
        var evt = new CustomEvent("panelready", {
          detail: {
            current: "#" + elements.panel.id,
            previous: "#wifi-available-networks"
          }
        });
        window.dispatchEvent(evt);
      },
      _initSoftKey: function() {
        var self = this;
        var softkeyParams = {
          menuClassName: 'menu-button',
          header: {
            l10nId: 'message'
          },
          items: [{
            name: 'Forget',
            l10nId: 'forget',
            priority: 3,
            method: function() {
              self._showConfirmDialog();
            }
          }]
        };
        //add for task5277229 by shangpeng.su 2017.09.14
        var wifiOffloadEnable = false;
        var profile = {};
        navigator.customization.getValue(
          'wifi_offload_enable').then((result) => {
          wifiOffloadEnable = JSON.stringify(result) === 'true' ? true : false;
        }).then(() => {
          navigator.customization.getValue(
            'wifi_offload_profile').then((res) => {
            var val = JSON.stringify(res);
            if (val !== 'undefined') {
              profile = res;
            }
            for (var i=0; i<profile.length; i++) {
              if (wifiOffloadEnable && elements.ssid.textContent === profile[i].ssid) {
                SettingsSoftkey.hide();
              } else {
                SettingsSoftkey.init(softkeyParams);
                SettingsSoftkey.show();
              }
            }
            if (!profile) {
              SettingsSoftkey.init(softkeyParams);
              SettingsSoftkey.show();
            }
          });
        });
        //add end
      },
      _showConfirmDialog: function() {
        var self = this;
        var dialogConfig = {
          title: {
            id: 'forgetNetwork-confirmation',
            args: {}
          },
          body: {
            id: 'forgetNetwork-dialog',
            args: {}
          },
          cancel: {
            l10nId: 'cancel',
            priority: 1,
            callback: function() {
              dialog.destroy();
              window.addEventListener('keydown', self.keydownHandler);
            },
          },
          confirm: {
            l10nId: 'forget',
            priority: 3,
            callback: function() {
              dialog.destroy();
              elements.panel.querySelector('button[type="submit"]').click();
            },
          },
          backcallback: function() {
            window.addEventListener('keydown', self.keydownHandler);
          }
        };
        var dialog = new ConfirmDialogHelper(dialogConfig);
        dialog.show(document.getElementById('app-confirmation-dialog'));
        window.removeEventListener('keydown', self.keydownHandler);
      },
      _updateNetworkInfo: function() {
        var info = wifiManager.connectionInformation || {};
        elements.ip.textContent = info.ipAddress || '';
        navigator.mozL10n.setAttributes(elements.speed,
          'linkSpeedMbs', {
            linkSpeed: info.linkSpeed
          });
      },
      _handleVisibiltychange: function() {
        if (!document.hidden) {
          this._initSoftKey(elements.panel);
          this._dispatchDialogShowEvent();
        }
      },

      _dispatchDialogShowEvent: function() {
        var evt = new CustomEvent("dialogpanelshow", {
          detail: {
            dialogpanel: "#" + elements.panel.id
          }
        });
        window.dispatchEvent(evt);
      }
    });
  };
});
