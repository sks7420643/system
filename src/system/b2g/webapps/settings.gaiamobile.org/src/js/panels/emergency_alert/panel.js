/* global SettingsSoftkey */
/**
 * Used to show Emergency alert panel
 */
define(['require','modules/settings_panel'],function(require) {
  

  var SettingsPanel = require('modules/settings_panel');

  return function ctor_emergency_alert_panel() {
    var elements = {};
    //JWJ: Fix ARG-1616: add 2 elements which needs to be listen the 'checked' event in _initAlertSettingListener
    var settingElements = ["cmas.extreme.enabled", "cmas.severe.enabled",
      "cmas.amber.enabled", "cmas.monthlytest.enabled", "cmas.volume.enable", "cmas.vibrate.enable"];
    var listElements = document.querySelectorAll('#emergency-alert li');
    var softkeyParams = {
      menuClassName: 'menu-button',
      header: {
        l10nId: 'message'
      },
      items: [{
        name: 'Deselect',
        l10nId: 'deselect',
        priority: 2,
        method: function() {}
      }]
    };
    var params = {
      menuClassName: 'menu-button',
      header: {
        l10nId: 'message'
      },
      items: [{
        name: 'Select',
        l10nId: 'select',
        priority: 2
      }]
    };

    function _updateSoftkey() {
      var focusedElement = document.querySelector('#emergency-alert .focus');
      if (focusedElement && focusedElement.classList &&
          focusedElement.classList.contains('none-select')) {
        SettingsSoftkey.hide();
        return;
      }
      if (focusedElement && focusedElement.querySelector('input') &&
          focusedElement.querySelector('input').checked) {
        SettingsSoftkey.init(softkeyParams);
      } else {
        SettingsSoftkey.init(params);
      }
      SettingsSoftkey.show();
    }

    function _initFocusEventListener() {
      var i = listElements.length - 1;
      for (i; i >= 0; i--) {
        listElements[i].addEventListener('focus', _updateSoftkey);
      }
    }

    function _removeFocusEventListener() {
      var i = listElements.length - 1;
      for (i; i >= 0; i--) {
        listElements[i].removeEventListener('focus', _updateSoftkey);
      }
    }

    function _initAlertSettingListener() {
      var i = settingElements.length - 1;
      for (i; i >= 0; i--) {
        SettingsListener.observe(settingElements[i], true, _updateSoftkey);
      }
    }

    function _removeAlertSettingListener() {
      var i = settingElements.length - 1;
      for (i; i >= 0; i--) {
        SettingsListener.unobserve(settingElements[i] , _updateSoftkey());
      }
    }

    function _keyDownHandler(evt) {
      switch (evt.key) {
        case 'Enter':
          if ('alert-inbox' === evt.target.id) {
            try {
              new MozActivity({
                name: 'alert_inbox'
              });
            } catch (e) {
              console.error('Failed to create an alert_inbox activity: ' + e);
            }
          } else if ('ringtone-preview' === evt.target.id) {
            try {
              new MozActivity({
                name: 'ringtone_preview'
              });
            } catch (e) {
              console.error('Failed to create an alert_inbox activity: ' + e);
            }
          }
          break;
        default:
          break;
      }
    }

      // Task5758730-chengyanzhang@t2mobile.com-for add exercise alert message-begin
    function _disableSomeMenu() {// bug4000-chengyanzhang@t2mobile.com-modify
      navigator.customization.getValue("def.operator.name").then((operator) => {
        console.log('Emergency-alert customization operator: ' + operator);
        if (operator === undefined) {
          return;
        }

        if (operator !== 'IL' && operator !== 'UAE') {
          var exerciseAlertMenu = document.getElementById('exercise-alert-menu');
          // exerciseAlertMenu.setAttribute('hidden', true);
          exerciseAlertMenu.classList.add('hidden');
          exerciseAlertMenu.classList.remove('navigable');
        }
        
        // bug4018-wen.jun.jiang@fih-foxconn.com-begin
        if (operator == 'UAE') {
          var AlertNotificationMenu = document.getElementById('alert-notification');
          AlertNotificationMenu.classList.add('hidden');
          AlertNotificationMenu.classList.remove('navigable');
        }        
        // bug4018-wen.jun.jiang@fih-foxconn.com-end

        // bug4000-chengyanzhang@t2mobile.com-begin
        // hidden some default menu and display cmas menu for tw
        if (operator === 'TW') {
          var emergencyAlertTWMenu = document.getElementById('emergency-alert-tw-menu');
          var emergencyMessageTWMenu = document.getElementById('emergency-message-tw-menu');
          emergencyAlertTWMenu.removeAttribute('hidden');
          emergencyMessageTWMenu.removeAttribute('hidden');

          var cmasExtremeMenu = document.getElementById('cmas-extreme-menu');
          var cmasSevereMenu = document.getElementById('cmas-severe-menu');
          var cmasAmberMenu = document.getElementById('cmas-amber-menu');
          var ringtonePreviewMenu = document.getElementById('ringtone-preview-menu');
          cmasExtremeMenu.classList.add('hidden');
          cmasSevereMenu.classList.add('hidden');
          cmasAmberMenu.classList.add('hidden');
          ringtonePreviewMenu.classList.add('hidden');
        }
        // bug4000-chengyanzhang@t2mobile.com-end

        // Added by yingsen.zhang@t2mobile.com 20180319 begin
        // Change Romania CMAS settings
        if (operator === 'RO') {
          var emergencyAlertTWMenu = document.getElementById('emergency-alert-tw-menu');
          var emergencyMessageTWMenu = document.getElementById('emergency-message-tw-menu');
          emergencyAlertTWMenu.classList.add('hidden');
          emergencyMessageTWMenu.classList.add('hidden');

          var exerciseAlertMenu = document.getElementById('exercise-alert-menu');
          var requiredMonthlyTestMenu = document.getElementById('required-monthly-test-menu');
          exerciseAlertMenu.classList.add('hidden');
          requiredMonthlyTestMenu.classList.add('hidden');
        }
        // Added by yingsen.zhang@t2mobile.com 20180319 end
        
        // JWJ For Korea --Begin
        if (operator === 'KR') {
          var emergencyAlertTWMenu = document.getElementById('emergency-alert-tw-menu');
          var emergencyMessageTWMenu = document.getElementById('emergency-message-tw-menu');
          emergencyAlertTWMenu.classList.add('hidden');
          emergencyMessageTWMenu.classList.add('hidden');

          var exerciseAlertMenu = document.getElementById('exercise-alert-menu');
          var requiredMonthlyTestMenu = document.getElementById('required-monthly-test-menu');
          exerciseAlertMenu.classList.add('hidden');
          requiredMonthlyTestMenu.classList.add('hidden');
        }
        // JWJ For Korea --End

       });
    }
    // Task5758730-chengyanzhang@t2mobile.com-for add exercise alert message-end

    function _updateAlertBodyDisplay(panel) {
      var alertBody = panel.querySelector('#receive-alert-body');
      var request = navigator.mozSettings.createLock().get('cmas.settings.show');
      request.onsuccess = () => {
        var val = request.result['cmas.settings.show'];
        if (val === 'undefined') {
          val = true;
        }
        if (alertBody.hidden === val) {
          alertBody.hidden = !val;
          window.dispatchEvent(new CustomEvent('refresh'));
        }
      };
      request.onerror = () => {
        console.error('ERROR: Can not get the receive alert setting.');
      };
    }

    return SettingsPanel({
      onInit: function(panel) {
        elements = [
          document.getElementById('alert-inbox'),
          document.getElementById('ringtone-preview')
        ];
        _updateAlertBodyDisplay(panel);
      },

      onBeforeShow: function() {
        _updateSoftkey();
        _disableSomeMenu();// Task5758730-chengyanzhang@t2mobile.com-for add exercise alert message-add
        elements.forEach((ele) => {
          ele.addEventListener('keydown', _keyDownHandler);
        })
        _initFocusEventListener();
        _initAlertSettingListener();
      },

      onBeforeHide: function() {
        _removeFocusEventListener();
        _removeAlertSettingListener();
        SettingsSoftkey.hide();
        elements.forEach((ele) => {
          ele.removeEventListener('keydown', _keyDownHandler);
        });
      }
    });
  };
});
