
define('panels/input_languages_selection/panel',['require','modules/settings_panel'],function(require) {
  
  var SettingsPanel = require('modules/settings_panel');

  return function ctor_selectInputLanguagePanel() {
    var _panel;
    var _keypadHelper;

    function _initUI(layouts) {
      var container = document.getElementById('keypad-container');
      var li, label, input, span, bdiName, l10nId;
      layouts.forEach((value, key) => {
        li = document.createElement('li');
        li.setAttribute('role', 'menuitem');
        label = document.createElement('label');
        label.className = 'pack-checkbox';
        input = document.createElement('input');
        input.name = key;
        input.type = 'checkbox';
        input.checked = value;
        span = document.createElement('span');
        bdiName = document.createElement('bdi');
        bdiName.textContent = _keypadHelper.getDisplayLanguageName(key);

        span.appendChild(bdiName);
        label.appendChild(input);
        label.appendChild(span);
        li.appendChild(label);
        container.appendChild(li);
      });
    }

    function _initSoftkey() {
      var params = {
        menuClassName: 'menu-button',
        header: {
          l10nId: 'message'
        },
        items: [{
          name: 'Select',
          l10nId: 'select',
          priority: 2,
          method: function() {}
        }]
      };

      SettingsSoftkey.init(params);
      SettingsSoftkey.show();
    }

    function _checkInputLanguage() {
      var allCheckboxs =
        _panel.querySelectorAll('input[type=checkbox]');
      var i = allCheckboxs.length - 1,
        count = 0,
        enabled;

      // To count user selected checkbox
      for (i; i >= 0; i--) {
        enabled = allCheckboxs[i].checked;
        if (!enabled) {
          count++;
        }
      }

      // If user uncheck all of these languages,
      // English will still be checked automatically.
      if (count === allCheckboxs.length) {
        _keypadHelper.setLayoutEnabled('EN_US', true);
      }
    }

    return SettingsPanel({
      onInit: function kalp_onInit(panel, options) {
        _panel = panel;
        _keypadHelper = options.KeypadHelper;
        _initUI(options.Layouts);
      },

      onBeforeShow: function kalp_onBeforeShow(panel, options) {
        _initSoftkey();
      },

      onBeforeHide: function kalp_onBeforeHide() {
        _checkInputLanguage();
        SettingsSoftkey.hide();
      }
    });
  };
});
