/**
 * Created by xa on 15-11-20.
 */
'use strict';

var inited = false;
var dmDefApnSettings;
var dmApnSettings;
var apnSettings;

function OmaConfiguration(){
  debug('testbox-omaConfiguration');
}
OmaConfiguration.prototype._init = function() {
  //do nothing
  //if(navigator.OmaService) {
  //  //navigator.OmaService.servicestart();
  //  debug("testbox-omaConfiguration: navigator.OmaService is not NULL");
  //} else {
  //  debug("testbox-omaConfiguration: navigator.OmaService is NULL");
  //}

  var request = window.navigator.mozSettings.createLock().get('ril.data.dmdefaultApnSettings');
  request.onsuccess = function() {
    dmDefApnSettings = request.result['ril.data.dmdefaultApnSettings'];
    debug("testbox-omaConfiguration dmDefApnSettings get = " + dmDefApnSettings.toString());
  }
}

OmaConfiguration.prototype.anpRestore = function(types) {

  var sTypes = types.toUpperCase();
  var request = window.navigator.mozSettings.createLock().get('ril.data.dmApnSettings');
  request.onsuccess = function() {
    dmApnSettings = request.result['ril.data.dmApnSettings'];
    debug("testbox-omaConfiguration dmApnSettings get = " + dmApnSettings.toString());
    var request1 = window.navigator.mozSettings.createLock().get('ril.data.apnSettings');
    request1.onsuccess = function() {
      apnSettings = request1.result['ril.data.apnSettings'];
      if ((null != apnSettings) && ("" != apnSettings)) {
        debug("testbox-omaConfiguration apnSettings get = " + apnSettings.toString());
        settingsApnRestoreType(sTypes);
      } else {
        debug('testbox-omaConfiguration apnSettings is undefined');
        alert('Please insert a valid uim-card');
      }
    };
  };

  function settingsApnRestoreType(mType) {
    switch(mType) {

      case 'ALL':
        var dmApnSettings4 = dmApnSettings[0][4];
        var apnSettings4 = apnSettings[0][4];
        dmApnSettings[0] = dmDefApnSettings[0];
        apnSettings[0] = dmDefApnSettings[0];
        dmApnSettings[0][4] = dmApnSettings4;
        apnSettings[0][4] = apnSettings4;
        break;

      case 'VZWIMS':
        dmApnSettings[0][0] = dmDefApnSettings[0][0];
        apnSettings[0][0] = dmDefApnSettings[0][0];
        var strDefApnSettings = JSON.stringify(dmDefApnSettings[0][0]);
        var request = navigator.engmodeExtension.setDataProfileByType(strDefApnSettings, 1);
        request.onsuccess = function() {
          var request1 = navigator.engmodeExtension.setDataProfileByType(strDefApnSettings, 2);
          request1.onsuccess = function() {
            debug('VZWIMS Restore Success!!!');
            alert('VZWIMS Restore Success!!!');
          };
        };
        break;

      case 'VZWADMIN':
        dmApnSettings[0][1] = dmDefApnSettings[0][1];
        apnSettings[0][1] = dmDefApnSettings[0][1];
        var strDefApnSettings = JSON.stringify(dmDefApnSettings[0][1]);
        var request = navigator.engmodeExtension.setDataProfileByType(strDefApnSettings, 1);
        request.onsuccess = function() {
          var request1 = navigator.engmodeExtension.setDataProfileByType(strDefApnSettings, 2);
          request1.onsuccess = function() {
            debug('VZWADMIN Restore Success!!!');
            alert('VZWADMIN Restore Success!!!');
          };
        };
        break;

      case 'VZWINTERNET':
        dmApnSettings[0][2] = dmDefApnSettings[0][2];
        apnSettings[0][2] = dmDefApnSettings[0][2];
        var strDefApnSettings = JSON.stringify(dmDefApnSettings[0][2]);
        var request = navigator.engmodeExtension.setDataProfileByType(strDefApnSettings, 1);
        request.onsuccess = function() {
          var request1 = navigator.engmodeExtension.setDataProfileByType(strDefApnSettings, 2);
          request1.onsuccess = function() {
            debug('VZWINTERNET Restore Success!!!');
            alert('VZWINTERNET Restore Success!!!');
          };
        };
        break;

      case 'VZWAPP':
        dmApnSettings[0][3] = dmDefApnSettings[0][3];
        apnSettings[0][3] = dmDefApnSettings[0][3];
        var strDefApnSettings = JSON.stringify(dmDefApnSettings[0][3]);
        var request = navigator.engmodeExtension.setDataProfileByType(strDefApnSettings, 1);
        request.onsuccess = function() {
          var request1 = navigator.engmodeExtension.setDataProfileByType(strDefApnSettings, 2);
          request1.onsuccess = function() {
            debug('VZWAPP Restore Success!!!');
            alert('VZWAPP Restore Success!!!');
          };
        };
        break;

      default :
        debug("testbox-omaConfiguration anpRestore wrong types");
        break;

    }
    navigator.engmodeExtension.eraseAPN(2);
    navigator.engmodeExtension.eraseAPN(3);
    var request = window.navigator.mozSettings.createLock().set({'ril.data.dmApnSettings' : dmApnSettings});
    request.onsuccess = function() {
      debug("testbox-omaConfiguration dmApnSettings set");
      var request1 = window.navigator.mozSettings.createLock().set({'ril.data.apnSettings': apnSettings});
      request1.onsuccess = function () {
        debug("testbox-omaConfiguration apnSettings set");
      }
    }
  }
}

OmaConfiguration.prototype.apnDisabled = function(types) {
  var sTypes = types.toUpperCase();
  var request = window.navigator.mozSettings.createLock().get('ril.data.dmApnSettings');
  request.onsuccess = function() {
    dmApnSettings = request.result['ril.data.dmApnSettings'][0];
  };
  switch(sTypes) {
    case 'ALL':

      break;
    case 'VZWIMS':
      dmApnSettings[0][0].enabled  = "false";
      break;
    case 'VZWADMIN':
      dmApnSettings[0][1].enabled  = "false";
      break;
    case 'VZWINTERNET':
      dmApnSettings[0][2].enabled  = "false";
      break;
    case 'VZWAPP':
      dmApnSettings[0][3].enabled  = "false";
      break;
    default :
      debug("testbox-omaConfiguration apnDisabled wrong types");
      break;

  }
}

OmaConfiguration.prototype.handleEvent = function(evt) {
  dump('testbox-omaConfiguration-------evt.key: ' + evt.key);
  if(evt.key) {
    switch(evt.key) {
      case 'Enter':
      case 'BrowserForward':
        switch(evt.target.id){
          case 'omadm-mIvzwall-disable':
            OmaConfiguration.prototype.apnDisabled('ALL');
            break;
          case 'omadm-mIvzwall-restore':
            OmaConfiguration.prototype.anpRestore('ALL');
            break;
          case 'omadm-mIvzwims-disable':
            OmaConfiguration.prototype.apnDisabled('VZWIMS');
            break;
          case 'omadm-mIvzwims-restore':
            OmaConfiguration.prototype.anpRestore('VZWIMS');
            break;
          case 'omadm-mIvzwadmin-disable':
            OmaConfiguration.prototype.apnDisabled('VZWADMIN');
            break;
          case 'omadm-mIvzwadmin-restore':
            OmaConfiguration.prototype.anpRestore('VZWADMIN');
            break;

          case 'omadm-mIvzwinternet-disable':
            OmaConfiguration.prototype.apnDisabled('VZWINTERNET');
            break;
          case 'omadm-mIvzwinternet-restore':
            OmaConfiguration.prototype.anpRestore('VZWINTERNET');
            break;
          case 'omadm-mIvzwapp-disable':
            OmaConfiguration.prototype.apnDisabled('VZWAPP');
            break;
          case 'omadm-mIvzwapp-restore':
            OmaConfiguration.prototype.anpRestore('VZWAPP');
            break;

          default :
                break;
        }
      default :
        break;
    }
  }
}

this.addEventListener('keydown', OmaConfiguration.prototype.handleEvent.bind(OmaConfiguration));

$('menuItem-DmConfiguration').addEventListener('click', function() {
  OmaConfiguration.prototype._init.bind(OmaConfiguration)();
})
