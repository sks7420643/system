/**
 * Created by xa on 3/31/14.
 */
'use strict';

$('menuItem-setting').addEventListener('click', function() {
	emSettings.init();
});
var _mobileConnections;
var _mobileConnection;
var _settings;
var _self;

var NETWORK_GSM_MAP = {
	'wcdma/gsm': 'operator-networkType-preferWCDMA',
	'gsm': 'operator-networkType-GSM',
	'wcdma': 'operator-networkType-WCDMA',
	'wcdma/gsm-auto': 'operator-networkType-auto-gsmcdma'
};

var NETWORK_CDMA_MAP = {
	'cdma/evdo': 'operator-networkType-auto-evdocdma',
	'cdma': 'operator-networkType-CDMA',
	'evdo': 'operator-networkType-EVDO'
};

var NETWORK_DUALSTACK_MAP = {
	'wcdma/gsm': 'operator-networkType-preferWCDMA',
	'gsm': 'operator-networkType-GSM',
	'wcdma': 'operator-networkType-WCDMA',
	'wcdma/gsm-auto': 'operator-networkType-auto-gsmcdma',
	'cdma/evdo':'operator-networkType-auto-evdocdma',
	'cdma':'operator-networkType-CDMA',
	'evdo': 'operator-networkType-EVDO',
	'wcdma/gsm/cdma/evdo': 'operator-networkType-auto-globe'
};

var emSettings = {

  init: function init() {
	  _self = this;
	  _self.addListeners();

	  _mobileConnections = window.navigator.mozMobileConnections;
	  _settings = window.navigator.mozSettings;

	  _mobileConnection = _mobileConnections[
		  DsdsSettings.getIccCardIndexForCellAndDataSettings()
		  ];
	  _self.UpdateCallwaitingStates();
	  _self.UndateAutoswerStates();
 //   _self.UndateAutoGPRSStates();
    _self.UndateCMASStates();
    _self.UndateDebugingStates();
    _self.UndateSdcardStates();
 //   _self.UndateRelayOprt();
  },

	addListeners: function addListeners() {
		window.addEventListener('change', _self.handleEvent);

		$('network-select').addEventListener('change', _self.selectNetwork.bind(_self), false);
		$('menuItem-networkSelect').onclick = function() {
			getSupportedNetworkInfo(_mobileConnection, function(result) {
				if (result.networkTypes) {
					_self.updateNetworkTypeSelector(result.networkTypes,
						result.gsm,
						result.cdma);
				}
			});
		};
	},

	UpdateCallwaitingStates: function UpdateCallwaitingStates() {
		var getCWEnabled = _mobileConnection.getCallWaitingOption();
		getCWEnabled.onsuccess = function cs_getCWEnabledSuccess() {
			var enabled = getCWEnabled.result;
			//input.checked = enabled;
			if (enabled) {
				$('callWaiting-input').checked = true;
			}
			else {
				$('callWaiting-input').checked = false;
			}
			if (callback) {
				callback(null);
			}
		};
	},
	UndateAutoswerStates: function UndateAutoswerStates() {
		var engmodeEx = navigator.engmodeExtension;
		if (engmodeEx) {
			var value = engmodeEx.readRoValue('persist.sys.tel.autoanswer.ms');
			if ('0' == value || '' == value) {
				$('autoAnswer-input').checked = false;
			}
			else {
				$('autoAnswer-input').checked = true;
			}
		}
	},

/*  UndateAutoGPRSStates: function UndateAutoGPRSStates() {
    var engmodeEx = navigator.engmodeExtension;
    if (engmodeEx) {
      var value = engmodeEx.readRoValue('persist.ro.ril.data_auto_attach');
      if ('false' == value || '' == value) {
        $('autoGPRS-input').checked = false;
      }
      else {
        $('autoGPRS-input').checked = true;
      }
    }
  }, */

  UndateCMASStates: function UndateCMASStates() {
    var request = window.navigator.mozSettings.createLock().get('cmas.test.enabled');
    request.onsuccess = function() {
      if (true == request.result['cmas.test.enabled']) {
        $('cmas-input').checked = true;
        debug('Cmas tatus = true');
      }else {
        $('cmas-input').checked = false;
        debug('Cmas tatus = false');
      }
    };
    request.onerror = function() {
      $('cmas-input').checked = false;
    };
  },

  UndateDebugingStates: function UndateDebugingStates() {
    var request = window.navigator.mozSettings.createLock().get('debugger.remote-mode');
    request.onsuccess = function() {
      if ('adb-devtools' == request.result['debugger.remote-mode']) {
        $('debuging-input').checked = true;
        debug('debuging tatus = true');
      }else {
        $('debuging-input').checked = false;
        debug('debuging tatus = false');
      }
    };
    request.onerror = function() {
      $('debuging-input').checked = false;
    };
  },
  UndateSdcardStates: function UndateSdcardStates() {
    var request = window.navigator.mozSettings.createLock().get('ums.enabled');
    request.onsuccess = function () {
      if (true == request.result['ums.enabled']) {
        $('sdcard-input').checked = true;
        debug('sdcard tatus = true');
      } else {
        $('sdcard-input').checked = false;
        debug('sdcard tatus = false');
      }
    };
    request.onerror = function () {
      $('sdcard-input').checked = false;
    };
  },

/*  UndateRelayOprt: function UndateRelayOprt() {
    var engmodeEx = navigator.engmodeExtension;
    if (engmodeEx) {
      var value = engmodeEx.readRoValue('persist.radio.relay_oprt_change');
      if ('0' == value || '' == value) {
        $('relay-oprt-change-input').checked = false;
      }
      else {
        $('relay-oprt-change-input').checked = true;
      }
    }
  }, */
	updateNetworkTypeSelector: function updateNetworkTypeSelector(networkTypes, gsm, cdma) {
    dump('testbox-em_Settings : gsm = ' + gsm + '  cdma = ' + cdma + '\n');
    var request = _mobileConnection.getPreferredNetworkType();
    request.onsuccess = function onSuccessHandler() {
      var networkType = request.result;
      dump('testbox-em_Settings :  networkType ' + networkType + '\n');
      if (networkType) {
        var selector = $('network-select');
        // Clean up all option before updating again.
        while (selector.hasChildNodes()) {
          selector.removeChild(selector.lastChild);
        }
        networkTypes.forEach(function(type) {
          dump('testbox-em_Settings : networkTypes ' + type);
          var option = document.createElement('option');
          option.value = type;
          option.selected = (networkType === type);
          // show user friendly network mode names
          if (gsm && cdma) {
            if (type in NETWORK_DUALSTACK_MAP) {
              //localize(option, NETWORK_DUALSTACK_MAP[type]);
              option.textContent = type;
            }
          } else if (gsm) {
            if (type in NETWORK_GSM_MAP) {
              //localize(option, NETWORK_GSM_MAP[type]);
              option.textContent = type;
            }
          } else if (cdma) {
            if (type in NETWORK_CDMA_MAP) {
              //localize(option, NETWORK_CDMA_MAP[type]);
              option.textContent = type;
            }
          } else { //failback only
            debug('testbox-em_Settings : ------type ' + type + '\n');
	          option.textContent = type;
          }
          selector.appendChild(option);
        });
      } else {
       console.warn('carrier: could not retrieve network type');
      }
    };
    request.onerror = function onErrorHandler() {
      console.warn('carrier: could not retrieve network type');
    };
  },

	selectNetwork: function selectNetwork(evt) {

    var type = evt.value;
    var networkIndex = $('network-select').selectedIndex;
    var networkType = $('network-select').options[networkIndex].value;

    var request = _mobileConnection.setPreferredNetworkType(networkType);
      request.onsuccess = function() {

        getSupportedNetworkInfo(_mobileConnection, function(result) {

          for (var s in result) {
            dump('testbox-em_Settings : s = ' + s);
          }
        });
      };
    request.onerror = function onErrorHandler() {
      debug('preferredNetworkTypeAlertErrorMessage');
    };
  },

	handleEvent: function handleEvent(evt) {
    var input = evt.target;
    var type = input.type;
    var id = input.id;
    var key = input.name;
    var value;

    switch (id) {
      case 'callForwarding-input':
        value = input.checked;
        break;

      case 'autoAnswer-input':
        value = input.checked;
        var autoCommand;
         if (value) {
           autoCommand = '3000';
         } else {
           autoCommand = '0';
         }
        if (navigator.engmodeExtension) {
          var engmodeEx = navigator.engmodeExtension;
	}
          var initRequest = engmodeEx.setPropertyLE('calling_autoanswer', autoCommand);
	        initRequest.onsuccess = function() {
            _self.UndateAutoswerStates();
        }
        break;

      case 'autoGPRS-input':
        value = input.checked;
        var autoCommand;
        if (value) {
          autoCommand = 'true';
        } else {
          autoCommand = 'false';
        }
        if (navigator.engmodeExtension) {
          var engmodeEx = navigator.engmodeExtension;
          var initRequest = engmodeEx.setPropertyLE('calling_autoGPRS', autoCommand);
          initRequest.onsuccess = function() {
            if (value){
              alert('Auto GPRS Opened!');
              if(engmodeEx){
                window.setTimeout(function(){
                  var parmArray = new Array();
                  parmArray.push('engmodereboot');
                  var initRequest = engmodeEx.execCmdLE(parmArray, 1);
                },3000);
              }
            } else {
              alert('Auto GPRS Closed!');
            }
          };
        }
        break;

      case 'callWaiting-input':
        value = input.checked;
        var req = _mobileConnection.setCallWaitingOption(value);
        req.onsuccess = function(){
          _self.UpdateCallwaitingStates();
        };
        req.onerror = function(){
          _self.UpdateCallwaitingStates();
        };
        break;

      case 'cmas-input':
        debug('cmas-input');
        value = input.checked;
        key = 'cmas.test.enabled';
        break;

      case 'debuging-input':
        debug('debuging-input');
        if(input.checked == true){
          value = 'adb-devtools';
        }else{
          value = 'adb-only';
        }

        key = 'debugger.remote-mode';
        break;

      case 'sdcard-input':
        debug('sdcard-input');
        value = input.checked;
        key = 'ums.enabled';
        break;

      case 'relay-oprt-change-input':
        debug('relay_oprt_change');
        value = input.checked;
        var autoCommand;
        if (value) {
          autoCommand = '1';
        } else {
          autoCommand = '0';
        }
        if (navigator.engmodeExtension) {
          var engmodeEx = navigator.engmodeExtension;
          var initRequest = engmodeEx.setPropertyLE('relay_oprt_change', autoCommand);
          initRequest.onsuccess = function() {
            if (value){
              alert('relay oprt change to Opened!');
            } else {
              alert('relay oprt change to Closed!');
            }
          };
        }
        break;

      default:
        break;
    }

    if (!key || !_settings || evt.type != 'change')
      return;
    debug('testbox-em_Settings : key ' + key);
    var cset = {};
    cset[key] = value;
		_settings.createLock().set(cset);
  }
};



