define([ "require", "tmpl!./tng/account_item.html", "api", "cards", "./base", "template!./settings_main.html" ], function(e) {
    var t, n = e("tmpl!./tng/account_item.html"), o = e("api"), r = e("cards");
    return [ e("./base")(e("template!./settings_main.html")), {
        createdCallback: function() {
            this.acctsSlice = o.viewAccounts(!1), this.acctsSlice.onsplice = this.onAccountsSplice.bind(this), 
            this._secretButtonClickCount = 0, this._secretButtonTimer = null;
        },
        onArgs: function(e) {
            this.args = e, t = this;
        },
        handleKeyDown: function(e) {
            switch (e.key) {
              case "Backspace":
                e.preventDefault(), t.onClose();
                break;

              case "*":
                t.onClickSecretButton();
            }
        },
        extraClasses: [ "anim-fade", "anim-overlay" ],
        menuOptions: [ {
            name: "Add Account",
            l10nId: "settings-account-add",
            priority: 1,
            method: function() {
                r.pushCard("setup_account_info", "animate", {
                    allowBack: !0
                }, "right");
            }
        }, {
            name: "Select",
            l10nId: "select",
            priority: 2
        } ],
        onCardVisible: function(e) {
            this.addEventListener("keydown", t.handleKeyDown), this.acctsSlice.items.forEach(function(e) {
                t.updateAccountLabel(e);
            }), this.accountsContainer.setAttribute("role", "heading"), this.setNavigationMap(e);
        },
        setNavigationMap: function(e) {
            const t = this.localName, n = ".tng-account-item", o = t + " " + n;
            "forward" !== e && e ? "back" === e && (NavigationMap.navSetup(t, n), NavigationMap.setCurrentControl(o), 
            NavigationMap.setFocus("restore"), NavigationMap.setSoftKeyBar(this.menuOptions)) : (NavigationMap.navSetup(t, n), 
            NavigationMap.setCurrentControl(o), NavigationMap.setFocus("first"), NavigationMap.setSoftKeyBar(this.menuOptions));
        },
        onClose: function() {
            r.removeCardAndSuccessors(this, "delay-animate", 1);
        },
        onAccountsSplice: function(e, t, o) {
            var r, i = this.accountsContainer;
            if (t) for (var s = e + t - 1; s >= e; s--) r = this.acctsSlice.items[s], i.removeChild(r.element);
            var a = e >= i.childElementCount ? null : i.children[e], c = this;
            o.forEach(function(e) {
                var t = e.element = n.cloneNode(!0);
                t.account = e, c.updateAccountDom(e, !0), i.insertBefore(t, a);
            }), this.setNavigationMap();
        },
        updateAccountDom: function(e, t) {
            var n = e.element;
            if (t) {
                var o = n.querySelector(".tng-account-item-label"), r = n.querySelector(".tng-account-item-accountLabel");
                o.textContent = e.name, r.innerHTML = e.label, n.addEventListener("click", this.onClickEnterAccount.bind(this, e), !1);
            }
        },
        updateAccountLabel: function(e) {
            var t = e.element.querySelector(".tng-account-item-label"), n = e.element.querySelector(".tng-account-item-accountLabel");
            t.textContent = e.name, n.innerHTML = e.label;
        },
        onClickAddAccount: function() {
            r.pushCard("setup_account_info", "animate", {
                allowBack: !0
            }, "right");
        },
        onClickEnterAccount: function(e) {
            r.pushCard("settings_account", "animate", {
                account: e
            }, "right");
        },
        onClickSecretButton: function() {
            null === this._secretButtonTimer && (this._secretButtonTimer = window.setTimeout(function() {
                this._secretButtonTimer = null, this._secretButtonClickCount = 0;
            }.bind(this), 2e3)), ++this._secretButtonClickCount >= 5 && (window.clearTimeout(this._secretButtonTimer), 
            this._secretButtonTimer = null, this._secretButtonClickCount = 0, r.pushCard("settings_debug", "animate", {}, "right"));
        },
        die: function() {
            this.acctsSlice.die(), this.removeEventListener("keydown", t.handleKeyDown);
        }
    } ];
});