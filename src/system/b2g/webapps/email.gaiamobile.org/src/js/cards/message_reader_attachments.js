define([ "require", "exports", "module", "tmpl!./msg/view_attachment_item.html", "cards", "l10n!", "mime_to_class", "file_display", "./base", "template!./message_reader_attachments.html" ], function(e) {
    var t, n, o = e("tmpl!./msg/view_attachment_item.html"), r = e("cards"), i = e("l10n!"), s = e("mime_to_class"), a = e("file_display"), c = -1, d = "application/octet-stream", u = !1, l = !1, h = 5242880;
    return [ e("./base")(e("template!./message_reader_attachments.html")), {
        onArgs: function(o) {
            this.attachments = o.composer, this.bView = !1, this.bShare = !1, n = this, this.attachmentsHeader = i.get("msg-reader-display-attachments"), 
            e([ "shared/js/mime_mapper" ], function(e) {
                t || (t = e);
            });
        },
        onBack: function() {
            r.removeCardAndSuccessors(this, "animate");
        },
        die: function() {
            u = !1, window.removeEventListener("keydown", this.handleAttachmentsKeydown);
        },
        getAttachmentBlob: function(e, t) {
            try {
                var n = e._file[0], o = e._file[1], r = navigator.getDeviceStorage(n), i = r.get(o);
                i.onerror = function() {
                    console.warn("Could not open attachment file: ", o, i.error.name), "NotFoundError" === i.error.name && Toaster.showToast({
                        messageL10nId: "message-attachment-not-found",
                        latency: 2e3
                    });
                }, i.onsuccess = function() {
                    var e = i.result;
                    t(e);
                };
            } catch (s) {
                console.warn("Exception getting attachment from device storage:", e._file, "\n", s, "\n", s.stack);
            }
        },
        onViewAttachment: function(e) {
            return console.log("trying to open", e._file, "known type:", e.mimetype), !e._file && e.isDownloadable ? (console.log("download this file firstly"), 
            n.downloadAttachment(e), n.bView = !0, void 0) : (e.isDownloaded && n.getAttachmentBlob(e, function(n) {
                try {
                    if (!n) throw new Error("Blob does not exist");
                    var o = e.mimetype;
                    o && o !== d && "audio/vnd.dlna.adts" !== o || (o = n.type), o && o !== d || (o = t.guessTypeFromFileProperties(e.filename, d)), 
                    o || (o = d), console.log("triggering open activity with MIME type:", o);
                    var r = new MozActivity({
                        name: "open",
                        data: {
                            type: o,
                            filename: e.filename,
                            blob: n,
                            url: e.filename,
                            forEditing: !0
                        }
                    });
                    r.onerror = function() {
                        if (console.warn('Problem with "open" activity', r.error.name), "NO_PROVIDER" === r.error.name) {
                            l = !0;
                            var e = {
                                title: {
                                    id: "message-attachment-did-not-open-label",
                                    args: {}
                                },
                                body: {
                                    id: "message-attachment-did-not-open-body",
                                    args: {}
                                },
                                desc: {
                                    id: "",
                                    args: {}
                                },
                                accept: {
                                    l10nId: "confirm-dialog-ok",
                                    priority: 2,
                                    callback: function() {
                                        l = !1;
                                    }
                                }
                            }, t = new ConfirmDialogHelper(e);
                            t.show(document.getElementById("confirm-dialog-container"));
                        }
                    }, r.onsuccess = function() {
                        console.log('"open" activity allegedly succeeded');
                    };
                } catch (i) {
                    console.warn('Problem creating "open" activity:', i, "\n", i.stack);
                }
            }), void 0);
        },
        doDownload: function(e) {
            console.log("go to downloadAttachment, attachment is " + e), e.isDownloading = !0, 
            this.setSoftKey(), e.download(function() {
                console.log("download attachment succuss callback");
                var t = document.querySelector('[data-name="' + e.filename + '"] .reader-view-attachment-downloading');
                e.isDownloading = !1, "card center" === n.className && (e._file || (console.log("attachment file is null"), 
                Toaster.showToast({
                    messageL10nId: "emptyDownloadFile",
                    latency: 2e3
                })), u && (t.classList.contains("collapsed") || t.classList.add("collapsed"), n._refresh(), 
                n.setSoftKey(), NavigationMap.setFocus("restore"), e === c && e._file && (n.bShare ? n.shareAttachmentHelper(e) : n.bView && (n.bView = !1, 
                n.onViewAttachment(e)))));
            }, null, !0), n.setSoftKey(), document.querySelector('[data-name="' + e.filename + '"] .reader-view-attachment-downloading').classList.remove("collapsed");
        },
        downloadAttachment: function(e) {
            var t = function(e) {
                Toaster.showToast({
                    messageL10nId: e,
                    latency: 2e3
                });
            };
            if (!window.navigator.onLine) return this.noConnectionDailog(), void 0;
            if (e.sizeEstimateInBytes > h) return t("message-attachment-too-large"), void 0;
            var o = navigator.getDeviceStorage("sdcard");
            o.available().onsuccess = function(r) {
                "available" !== r.target.result ? console.error("Can not get available memory card to download!") : o.freeSpace().onsuccess = function(r) {
                    r.target.result > e.sizeEstimateInBytes ? n.doDownload(e) : "sdcard" === o.storageName ? t("storage-full") : t("storage-sdcard-full");
                };
            };
        },
        shareAttachmentHelper: function(e) {
            console.log("go to shareAttachmentHelper"), n.bShare = !1, n.getAttachmentBlob(e, function(n) {
                try {
                    if (!n) throw new Error("Blob does not exist");
                    var o = e.mimetype;
                    o && o !== d || (o = n.type), o && o !== d || (o = t.guessTypeFromFileProperties(e.filename, d)), 
                    o || (o = d), o && (o = o.split("/")[0] + "/*"), console.log("triggering share activity with MIME type:", o);
                    var r = new MozActivity({
                        name: "share",
                        data: {
                            type: o,
                            number: 1,
                            blobs: [ n ],
                            filenames: [ e.filename ],
                            filepaths: [ e.filename ],
                            owner: "email"
                        }
                    });
                    r.onerror = function() {
                        console.warn('Problem with "share" activity', r.error.name);
                    }, r.onsuccess = function() {
                        console.log('"share" activity allegedly succeeded');
                    };
                } catch (i) {
                    console.warn('Problem creating "share" activity:', i, "\n", i.stack);
                }
            });
        },
        shareAttachment: function(e) {
            console.log("go to shareAttachment, attachment is " + e), e.isDownloaded && e._file ? n.shareAttachmentHelper(e) : e.isDownloaded || (console.log("not a downloaded attachment, downloading"), 
            n.bShare = !0, n.downloadAttachment(e), n.setSoftKey());
        },
        noConnectionDailog: function() {
            var e = {
                title: {
                    id: "confirmation-title",
                    args: {}
                },
                body: {
                    id: "noInternetConnection",
                    args: {}
                },
                desc: {
                    id: "",
                    args: {}
                },
                cancel: {
                    l10nId: "opt-cancel",
                    priority: 1,
                    callback: function() {}
                },
                confirm: {
                    l10nId: "opt-settings",
                    priority: 3,
                    callback: function() {
                        var e = new MozActivity({
                            name: "configure",
                            data: {
                                target: "device",
                                section: "connectivity-settings"
                            }
                        });
                        e.onerror = function() {
                            console.warn("Configure activity error:", e.error.name);
                        };
                    }
                }
            }, t = new ConfirmDialogHelper(e);
            t.show(document.getElementById("confirm-dialog-container"));
        },
        onCardVisible: function(e) {
            console.log(this.localName + ".onCardVisible, navDirection=" + e), window.addEventListener("keydown", this.handleAttachmentsKeydown), 
            u = !0, this._refresh(), this.setSoftKey(), "forward" === e ? NavigationMap.setFocus("first") : "back" === e && NavigationMap.setFocus("restore");
        },
        onHidden: function() {
            console.log(this.localName + " onHidden"), u = !1, window.removeEventListener("keydown", this.handleAttachmentsKeydown);
        },
        navSetup: function() {
            var e = this.localName, t = ".msg-reader-view-attachment-focusable", n = e + " " + t;
            NavigationMap.navSetup(e, t), NavigationMap.setCurrentControl(n);
        },
        handleAttachmentsKeydown: function(e) {
            switch (e.key) {
              case "Backspace":
                option.menuVisible !== !1 || l || (e.preventDefault(), n.onBack()), l && (e.preventDefault(), 
                l = !1);
            }
        },
        _refresh: function() {
            if (console.log("go to messgae_reader_attachments --->_refresh"), this.attachments && this.attachments.length) {
                this.readerViewAttachmentsContainer.innerHTML = "";
                for (var e = o, n = e.querySelector(".reader-view-attachment-filename"), r = e.querySelector(".reader-view-attachment-filesize"), i = 0; i < this.attachments.length; i++) {
                    var c = this.attachments[i];
                    n.textContent = c.filename, navigator.largeTextEnabled && (n.classList.add("p-pri"), 
                    r.classList.add("p-pri"));
                    var d = c.filename.split(".").pop(), u = s(c.mimetype || t.guessTypeFromExtension(d));
                    "application/zip" === c.mimetype && (u = "mime-zip"), a.fileSize(r, c.sizeEstimateInBytes);
                    var l = e.cloneNode(!0);
                    l.setAttribute("data-name", n.textContent), l.classList.add(u);
                    var h = l.querySelector(".reader-view-attachment-icon"), p = "";
                    switch (u) {
                      case "mime-audio":
                        p = "file-audio";
                        break;

                      case "mime-video":
                        p = "file-video";
                        break;

                      case "mime-image":
                        p = "file-photo";
                        break;

                      case "mime-zip":
                        p = "file-compress";
                        break;

                      default:
                        p = "file";
                    }
                    h.setAttribute("data-icon", p);
                    var f = l.querySelector(".reader-view-attachment-downloaded-picture"), m = l.querySelector(".reader-view-attachment-downloading");
                    c.isDownloaded ? (f.classList.remove("collapsed"), m.classList.add("collapsed")) : c.isDownloading ? (m.classList.remove("collapsed"), 
                    f.classList.add("collapsed")) : (f.classList.add("collapsed"), m.classList.add("collapsed")), 
                    this.readerViewAttachmentsContainer.appendChild(l);
                }
            }
            this.viewAttachmentsLabel.innerHTML = this.attachmentsHeader, this.navSetup();
        },
        setSoftKey: function() {
            var e = [];
            c.isDownloaded || c.isDownloading || !c.isDownloadable || e.push({
                name: "Download",
                l10nId: "attachment-download",
                priority: 1,
                method: function() {
                    n.downloadAttachment(c);
                }
            }), c.isDownloading || (e.push({
                name: "Select",
                l10nId: "select",
                priority: 2,
                method: function() {
                    n.onViewAttachment(c);
                }
            }), c.isDownloadable && e.push({
                name: "Share",
                l10nId: "attachment-share",
                priority: 3,
                method: function() {
                    n.shareAttachment(c);
                }
            })), NavigationMap.setSoftKeyBar(e);
        },
        onFocusChanged: function(e, t) {
            console.log(this.localName + ".onFocusChanged, queryChild=" + e + ", index=" + t), 
            this.attachments[t] !== c && (this.bShare = !1, this.bView = !1, c = this.attachments[t]), 
            this.setSoftKey();
        }
    } ];
});