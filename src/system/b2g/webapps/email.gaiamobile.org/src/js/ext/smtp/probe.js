define([ "logic", "./client", "exports" ], function(e, t, n) {
    function o(t, n) {
        return e(r, "checking-address-validity", {
            ns: "SmtpProber",
            _address: n
        }), new Promise(function(e, o) {
            t.useEnvelope({
                from: n,
                to: [ n ]
            }), t.onready = function() {
                e();
            }, t.onerror = function(e) {
                o(e);
            };
        });
    }
    var r = e.scope("SmtpProber");
    n.probeAccount = function(n, s) {
        e(r, "connecting", {
            _credentials: n,
            connInfo: s
        });
        var i;
        return t.createSmtpConnection(n, s, function() {
            e(r, "credentials-updated");
        }).then(function(e) {
            return i = e, o(i, s.emailAddress);
        }).then(function() {
            return e(r, "success"), i.close(), i;
        }).catch(function(n) {
            var o = t.analyzeSmtpError(i, n, !1);
            throw i && i.close(), e(r, "error", {
                error: o,
                connInfo: s
            }), o;
        });
    };
});