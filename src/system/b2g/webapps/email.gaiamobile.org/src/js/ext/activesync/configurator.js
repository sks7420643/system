define("activesync/folder", [ "logic", "../date", "../syncbase", "../allback", "../db/mail_rep", "activesync/codepages/AirSync", "activesync/codepages/AirSyncBase", "activesync/codepages/ItemEstimate", "activesync/codepages/Email", "activesync/codepages/ItemOperations", "safe-base64", "mimetypes", "module", "require", "exports" ], function(e, n, t, o, r, s, a, i, c, l, d, u, f, g, h) {
    function v() {
        _ = s.Enums.FilterType, S = {
            auto: null,
            "1d": _.OneDayBack,
            "3d": _.ThreeDaysBack,
            "1w": _.OneWeekBack,
            "2w": _.TwoWeeksBack,
            "1m": _.OneMonthBack,
            all: _.NoFilter
        }, b = {
            0: "all messages",
            1: "one day",
            2: "three days",
            3: "one week",
            4: "two weeks",
            5: "one month"
        };
    }
    function y(e, n, t) {
        return function() {
            var o = Array.slice(arguments), r = o[e], s = this;
            g([ "wbxml", "addressparser", "../mailchew" ], function(e, a, i) {
                I || (I = e, w = a.parse.bind(a), k = i, v()), s._account.withConnection(r, function() {
                    n.apply(s, o);
                }, t);
            });
        };
    }
    function p(n, t) {
        this._account = n, this._storage = t, e.defineScope(this, "ActiveSyncFolderConn", {
            folderId: t.folderId,
            accountId: n.id
        }), this.folderMeta = t.folderMeta, this.syncKey || (this.syncKey = "0");
    }
    function m(n, t) {
        this._account = n, this.folderStorage = t, e.defineScope(this, "ActiveSyncFolderSyncer", {
            accountId: n.id,
            folderId: t.folderId
        }), this.folderConn = new p(n, t);
    }
    var _, S, b, I, w, k, E = 5120, C = 50;
    p.prototype = {
        get syncKey() {
            return this.folderMeta.syncKey;
        },
        set syncKey(e) {
            return this.folderMeta.syncKey = e;
        },
        get serverId() {
            return this.folderMeta.serverId;
        },
        get filterType() {
            var e = this._account.accountDef.syncRange;
            if (S.hasOwnProperty(e)) {
                var n = S[e];
                return n ? n : this.folderMeta.filterType;
            }
            return console.warn("Got an invalid syncRange (" + e + ") using three days back instead"), 
            s.Enums.FilterType.ThreeDaysBack;
        },
        _getSyncKey: y(1, function(e, n) {
            var t = this, o = this._account, r = s.Tags, a = new I.Writer("1.3", 1, "UTF-8");
            a.stag(r.Sync).stag(r.Collections).stag(r.Collection), o.conn.currentVersion.lt("12.1") && a.tag(r.Class, "Email"), 
            a.tag(r.SyncKey, "0").tag(r.CollectionId, this.serverId).stag(r.Options).tag(r.FilterType, e).etag().etag().etag().etag(), 
            o.conn.postCommand(a, function(e, s) {
                if (e) return console.error(e), o._reportErrorIfNecessary(e), n("unknown"), void 0;
                t.syncKey = "0";
                var a = new I.EventParser();
                a.addEventListener([ r.Sync, r.Collections, r.Collection, r.SyncKey ], function(e) {
                    t.syncKey = e.children[0].textContent;
                }), a.onerror = function() {}, a.run(s), "0" === t.syncKey ? (console.error("Unable to get sync key for folder"), 
                n("unknown")) : n();
            });
        }),
        _getItemEstimate: y(1, function(e, n) {
            var t = i.Tags, o = s.Tags, r = this._account, a = new I.Writer("1.3", 1, "UTF-8");
            a.stag(t.GetItemEstimate).stag(t.Collections).stag(t.Collection), this._account.conn.currentVersion.gte("14.0") ? a.tag(o.SyncKey, this.syncKey).tag(t.CollectionId, this.serverId).stag(o.Options).tag(o.FilterType, e).etag() : a.tag(t.Class, "Email").tag(o.SyncKey, this.syncKey).tag(t.CollectionId, this.serverId).tag(o.FilterType, e), 
            a.etag(t.Collection).etag(t.Collections).etag(t.GetItemEstimate), r.conn.postCommand(a, function(e, o) {
                if (e) return console.error(e), r._reportErrorIfNecessary(e), n("unknown"), void 0;
                var s, a, c = new I.EventParser(), l = [ t.GetItemEstimate, t.Response ];
                c.addEventListener(l.concat(t.Status), function(e) {
                    s = e.children[0].textContent;
                }), c.addEventListener(l.concat(t.Collection, t.Estimate), function(e) {
                    a = parseInt(e.children[0].textContent, 10);
                });
                try {
                    c.run(o);
                } catch (d) {
                    return console.error("Error parsing GetItemEstimate response", d, "\n", d.stack), 
                    n("unknown"), void 0;
                }
                s !== i.Enums.Status.Success ? (console.error("Error getting item estimate:", s), 
                n("unknown")) : n(null, a);
            });
        }),
        _inferFilterType: y(0, function(n) {
            var t = this, o = s.Enums.FilterType, r = function(e, r) {
                t._getSyncKey(e, function(s) {
                    return s ? (n("unknown"), void 0) : (t._getItemEstimate(e, function(e, t) {
                        return e ? (n(null, o.ThreeDaysBack), void 0) : (r(t), void 0);
                    }), void 0);
                });
            };
            r(o.TwoWeeksBack, function(s) {
                var a, i = s / 14;
                if (0 > s) a = o.ThreeDaysBack; else if (i >= C) a = o.OneDayBack; else if (3 * i >= C) a = o.ThreeDaysBack; else if (7 * i >= C) a = o.OneWeekBack; else if (14 * i >= C) a = o.TwoWeeksBack; else {
                    if (!(30 * i >= C)) return r(o.NoFilter, function(r) {
                        var s;
                        r > C ? (s = o.OneMonthBack, t.syncKey = "0") : s = o.NoFilter, e(t, "inferFilterType", {
                            filterType: s
                        }), n(null, s);
                    }), void 0;
                    a = o.OneMonthBack;
                }
                a !== o.TwoWeeksBack && (t.syncKey = "0"), e(t, "inferFilterType", {
                    filterType: a
                }), n(null, a);
            });
        }),
        _enumerateFolderChanges: y(0, function(n, t) {
            var o = this, r = this._storage;
            if (!this.filterType) return this._inferFilterType(function(e, r) {
                return e ? (n("unknown"), void 0) : (console.log("We want a filter of", b[r]), o.folderMeta.filterType = r, 
                o._enumerateFolderChanges(n, t), void 0);
            }), void 0;
            if ("0" === this.syncKey) return this._getSyncKey(this.filterType, function(e) {
                return e ? (n("aborted"), void 0) : (o._enumerateFolderChanges(n, t), void 0);
            }), void 0;
            var i = s.Tags, c = s.Enums;
            a.Tags, a.Enums;
            var l;
            e(this, "exchange version: ", {
                version: this._account.conn.currentVersion
            }), 0 === this._account._syncsInProgress++ && this._account._lastSyncKey === this.syncKey && this._account._lastSyncFilterType === this.filterType && this._account._lastSyncResponseWasEmpty ? l = i.Sync : (l = new I.Writer("1.3", 1, "UTF-8"), 
            l.stag(i.Sync).stag(i.Collections).stag(i.Collection), this._account.conn.currentVersion.lte("12.1") && l.tag(i.Class, "Email"), 
            l.tag(i.SyncKey, this.syncKey).tag(i.CollectionId, this.serverId).tag(i.GetChanges).stag(i.Options).tag(i.FilterType, this.filterType), 
            this._account.conn.currentVersion.lt("12.0") ? l.tag(i.MIMESupport, c.MIMESupport.Never).tag(i.Truncation, c.MIMETruncation.TruncateAll) : l.tag(i.MIMESupport, c.MIMESupport.Always).tag(i.MIMETruncation, c.MIMETruncation.NoTruncate), 
            l.etag().etag().etag().etag()), this._account.conn.postCommand(l, function(e, s) {
                var a, l = [], d = [], u = [], f = !1;
                if (o._account._syncsInProgress--, e) return console.error("Error syncing folder:", e), 
                o._account._reportErrorIfNecessary(e), n("aborted"), void 0;
                if (o._account._lastSyncKey = o.syncKey, o._account._lastSyncFilterType = o.filterType, 
                !s) return console.log("Sync completed with empty response"), o._account._lastSyncResponseWasEmpty = !0, 
                n(null, l, d, u), void 0;
                o._account._lastSyncResponseWasEmpty = !1;
                var g = new I.EventParser(), h = [ i.Sync, i.Collections, i.Collection ];
                g.addEventListener(h.concat(i.SyncKey), function(e) {
                    o.syncKey = e.children[0].textContent;
                }), g.addEventListener(h.concat(i.Status), function(e) {
                    a = e.children[0].textContent;
                }), g.addEventListener(h.concat(i.MoreAvailable), function() {
                    f = !0;
                }), g.addEventListener(h.concat(i.Commands, [ [ i.Add, i.Change ] ]), function(e) {
                    var n, t;
                    for (var s in Iterator(e.children)) {
                        var a = s[1];
                        switch (a.tag) {
                          case i.ServerId:
                            n = a.children[0].textContent;
                            break;

                          case i.ApplicationData:
                            try {
                                t = o._parseMessage(a, e.tag === i.Add, r);
                            } catch (c) {
                                return console.error("Failed to parse a message:", c, "\n", c.stack), void 0;
                            }
                        }
                    }
                    t.header.srvid = n;
                    var u = e.tag === i.Add ? l : d;
                    u.push(t);
                }), g.addEventListener(h.concat(i.Commands, [ [ i.Delete, i.SoftDelete ] ]), function(e) {
                    var n;
                    for (var t in Iterator(e.children)) {
                        var o = t[1];
                        switch (o.tag) {
                          case i.ServerId:
                            n = o.children[0].textContent;
                        }
                    }
                    u.push(n);
                });
                try {
                    g.run(s);
                } catch (v) {
                    return console.error("Error parsing Sync response:", v, "\n", v.stack), n("unknown"), 
                    void 0;
                }
                a === c.Status.Success ? (console.log("Sync completed: added " + l.length + ", changed " + d.length + ", deleted " + u.length), 
                n(null, l, d, u, f), f && o._enumerateFolderChanges(n, t)) : a === c.Status.InvalidSyncKey ? (console.warn("ActiveSync had a bad sync key"), 
                n("badkey")) : a ? (console.error("Something went wrong during ActiveSync syncing and we got a status of " + a), 
                n("unknown")) : n("checkAccount");
            }, null, null, function(e, n) {
                n || (n = Math.max(1e6, e)), t(.1 + .7 * e / n);
            });
        }, "aborted"),
        _parseMessage: function(e, n, t) {
            var o, s, i, l = c.Tags, d = a.Tags, f = a.Enums;
            if (n) {
                var g = t._issueNewHeaderId();
                o = {
                    id: g,
                    srvid: null,
                    suid: t.folderId + "/" + g,
                    guid: "",
                    author: null,
                    to: null,
                    cc: null,
                    bcc: null,
                    replyTo: null,
                    date: null,
                    flags: [],
                    hasAttachments: !1,
                    subject: null,
                    snippet: null
                }, s = {
                    date: null,
                    size: 0,
                    attachments: [],
                    relatedParts: [],
                    references: null,
                    bodyReps: null
                }, i = function(e, n) {
                    n && o.flags.push(e);
                };
            } else o = {
                flags: [],
                mergeInto: function(e) {
                    for (var n in Iterator(this.flags)) {
                        var t = n[1];
                        if (t[1]) e.flags.push(t[0]); else {
                            var o = e.flags.indexOf(t[0]);
                            -1 !== o && e.flags.splice(o, 1);
                        }
                    }
                    var r = [ "mergeInto", "suid", "srvid", "guid", "id", "flags" ];
                    for (var n in Iterator(this)) {
                        var s = n[0], a = n[1];
                        -1 === r.indexOf(s) && (e[s] = a);
                    }
                }
            }, s = {
                mergeInto: function(e) {
                    for (var n in Iterator(this)) {
                        var t = n[0], o = n[1];
                        "mergeInto" !== t && (e[t] = o);
                    }
                }
            }, i = function(e, n) {
                o.flags.push([ e, n ]);
            };
            var h, v;
            for (var y in Iterator(e.children)) {
                var p = y[1], m = p.children.length ? p.children[0].textContent : null;
                switch (p.tag) {
                  case l.Subject:
                    o.subject = m;
                    break;

                  case l.From:
                    o.author = w(m)[0] || null;
                    break;

                  case l.To:
                    o.to = w(m);
                    break;

                  case l.Cc:
                    o.cc = w(m);
                    break;

                  case l.ReplyTo:
                    o.replyTo = w(m);
                    break;

                  case l.DateReceived:
                    s.date = o.date = new Date(m).valueOf();
                    break;

                  case l.Read:
                    i("\\Seen", "1" === m);
                    break;

                  case l.Flag:
                    for (var _ in Iterator(p.children)) {
                        var S = _[1];
                        S.tag === l.Status && i("\\Flagged", "0" !== S.children[0].textContent);
                    }
                    break;

                  case d.Body:
                    for (var _ in Iterator(p.children)) {
                        var S = _[1];
                        switch (S.tag) {
                          case d.Type:
                            var b = S.children[0].textContent;
                            b === f.Type.HTML ? h = "html" : (b !== f.Type.PlainText && console.warn("A message had a strange body type:", b), 
                            h = "plain");
                            break;

                          case d.EstimatedDataSize:
                            v = S.children[0].textContent;
                        }
                    }
                    break;

                  case l.MIMEData:
                    h = "html", v = m;
                    break;

                  case l.BodySize:
                    h = "plain", v = m;
                    break;

                  case d.Attachments:
                  case l.Attachments:
                    for (var _ in Iterator(p.children)) {
                        var I = _[1];
                        if (I.tag === d.Attachment || I.tag === l.Attachment) {
                            var k = {
                                name: null,
                                contentId: null,
                                type: null,
                                part: null,
                                encoding: null,
                                sizeEstimate: null,
                                file: null
                            }, E = !1;
                            for (var C in Iterator(I.children)) {
                                var F, M, T = C[1], D = T.children.length ? T.children[0].textContent : null;
                                switch (T.tag) {
                                  case d.DisplayName:
                                  case l.DisplayName:
                                    k.name = D, F = k.name.lastIndexOf("."), M = F > 0 ? k.name.substring(F + 1).toLowerCase() : "", 
                                    k.type = u.detectMimeType(M);
                                    break;

                                  case d.FileReference:
                                  case l.AttName:
                                  case l.Att0Id:
                                    k.part = D;
                                    break;

                                  case d.EstimatedDataSize:
                                  case l.AttSize:
                                    k.sizeEstimate = parseInt(D, 10);
                                    break;

                                  case d.ContentId:
                                    k.contentId = D;
                                    break;

                                  case d.IsInline:
                                    E = "1" === D;
                                }
                            }
                            E ? s.relatedParts.push(r.makeAttachmentPart(k)) : s.attachments.push(r.makeAttachmentPart(k));
                        }
                    }
                    o.hasAttachments = s.attachments.length > 0;
                }
            }
            return n ? (s.bodyReps = [ r.makeBodyPart({
                type: h,
                sizeEstimate: v,
                amountDownloaded: 0,
                isDownloaded: !1
            }) ], {
                header: r.makeHeaderInfo(o),
                body: r.makeBodyInfo(s)
            }) : {
                header: o,
                body: s
            };
        },
        downloadBodies: function(e, n, t) {
            if (this._account.conn.currentVersion.lt("12.0")) return this._syncBodies(e, t);
            for (var r = 0, s = o.latch(), a = 0; a < e.length; a++) {
                var i = e[a];
                i && null === i.snippet && (r++, this.downloadBodyReps(i, n, s.defer(i.suid)));
            }
            s.then(function(e) {
                t(o.extractErrFromCallbackArgs(e), r);
            });
        },
        downloadBodyReps: y(1, function(e, n, t) {
            var o = this, r = this._account;
            if (r.conn.currentVersion.lt("12.0")) return this._syncBodies([ e ], t);
            "function" == typeof n && (t = n, n = null), n = n || {};
            var i = l.Tags, c = l.Enums, d = s.Tags;
            s.Enums;
            var u = a.Tags, f = a.Enums.Type, g = function(s) {
                if (!s) return t("unknown");
                var a, l = s.bodyReps[0], g = "html" === l.type ? f.HTML : f.PlainText;
                n.maximumBytesToFetch < l.sizeEstimate && (g = f.PlainText, a = E);
                var h = new I.Writer("1.3", 1, "UTF-8");
                h.stag(i.ItemOperations).stag(i.Fetch).tag(i.Store, "Mailbox").tag(d.CollectionId, o.serverId).tag(d.ServerId, e.srvid).stag(i.Options).stag(i.Schema).tag(u.Body).etag().stag(u.BodyPreference).tag(u.Type, g), 
                a && h.tag(u.TruncationSize, a), h.etag().etag().etag().etag(), r.conn.postCommand(h, function(n, l) {
                    if (n) return console.error(n), r._reportErrorIfNecessary(n), t("unknown"), void 0;
                    var d, f, g = new I.EventParser();
                    g.addEventListener([ i.ItemOperations, i.Status ], function(e) {
                        d = e.children[0].textContent;
                    }), g.addEventListener([ i.ItemOperations, i.Response, i.Fetch, i.Properties, u.Body, u.Data ], function(e) {
                        f = e.children[0].textContent;
                    });
                    try {
                        g.run(l);
                    } catch (h) {
                        return t("unknown");
                    }
                    return d !== c.Status.Success ? t("unknown") : (o._updateBody(e, s, f, !!a, t), 
                    void 0);
                });
            };
            this._storage.getMessageBody(e.suid, e.date, g);
        }),
        _syncBodies: function(e, n) {
            var t = s.Tags, r = s.Enums, a = c.Tags, i = this, l = this._account, d = new I.Writer("1.3", 1, "UTF-8");
            d.stag(t.Sync).stag(t.Collections).stag(t.Collection).tag(t.Class, "Email").tag(t.SyncKey, this.syncKey).tag(t.CollectionId, this.serverId).stag(t.Options).tag(t.MIMESupport, r.MIMESupport.Never).etag().stag(t.Commands);
            for (var u = 0; u < e.length; u++) d.stag(t.Fetch).tag(t.ServerId, e[u].srvid).etag();
            d.etag().etag().etag().etag(), l.conn.postCommand(d, function(s, c) {
                if (s) return console.error(s), l._reportErrorIfNecessary(s), n("unknown"), void 0;
                var d = o.latch(), u = 0, f = new I.EventParser(), g = [ t.Sync, t.Collections, t.Collection ];
                f.addEventListener(g.concat(t.SyncKey), function(e) {
                    i.syncKey = e.children[0].textContent;
                }), f.addEventListener(g.concat(t.Status), function(e) {
                    var n = e.children[0].textContent;
                    n !== r.Status.Success && d.defer("status")("unknown");
                }), f.addEventListener(g.concat(t.Responses, t.Fetch, t.ApplicationData, a.Body), function(n) {
                    var t = e[u++], o = n.children[0].textContent, r = d.defer(t.suid);
                    i._storage.getMessageBody(t.suid, t.date, function(e) {
                        i._updateBody(t, e, o, !1, r);
                    });
                }), f.run(c), d.then(function(e) {
                    n(o.extractErrFromCallbackArgs(e));
                });
            });
        },
        _activeSyncHeaderIsSeen: function(e) {
            for (var n = 0; n < e.flags.length; n++) if ("\\Seen" === e.flags[n][0] && e.flags[n][1]) return !0;
            return !1;
        },
        _updateBody: function(e, n, t, r, s) {
            var a = n.bodyReps[0];
            t || (t = ""), t = t.replace(/\r/g, "");
            var i = r ? "plain" : a.type, c = k.processMessageContent(t, i, !r, !0);
            e.snippet = c.snippet, a.isDownloaded = !r, a.amountDownloaded = t.length, r || (a.content = c.content);
            var l = {
                changeDetails: {
                    bodyReps: [ 0 ]
                }
            }, d = o.latch();
            this._storage.updateMessageHeader(e.date, e.id, !1, e, n, d.defer("header")), this._storage.updateMessageBody(e, n, {}, l, d.defer("body")), 
            d.then(s.bind(null, null, n, !1));
        },
        sync: y(1, function(n, r, s) {
            var a = this, i = 0, c = 0, l = 0;
            e(this, "sync_begin");
            var d = this;
            this._enumerateFolderChanges(function(s, u, f, g, h) {
                var v = a._storage;
                if ("badkey" === s) return a.syncKey = "0", a._account._recreateFolder(v.folderId, function() {
                    a._storage = null, e(a, "sync_end", {
                        full: null,
                        changed: null,
                        deleted: null
                    });
                }), void 0;
                if ("checkAccount" === s) return a._account.syncFolderList(), void 0;
                if (s) return e(a, "sync_end", {
                    full: null,
                    changed: null,
                    deleted: null
                }), r(s), void 0;
                var y = o.latch();
                for (var p in Iterator(u)) {
                    var m = p[1];
                    v.hasMessageWithServerId(m.header.srvid) || (v.addMessageHeader(m.header, m.body, y.defer()), 
                    v.addMessageBody(m.header, m.body, y.defer()), i++);
                }
                for (var p in Iterator(f)) {
                    var m = p[1];
                    v.hasMessageWithServerId(m.header.srvid) && (v.updateMessageHeaderByServerId(m.header.srvid, !0, function(e, n) {
                        return !d._activeSyncHeaderIsSeen(n) && d._activeSyncHeaderIsSeen(e.header) ? v.folderMeta.unreadCount-- : d._activeSyncHeaderIsSeen(n) && !d._activeSyncHeaderIsSeen(e.header) && v.folderMeta.unreadCount++, 
                        e.header.mergeInto(n), !0;
                    }.bind(null, m), null, y.defer()), c++);
                }
                for (var p in Iterator(g)) {
                    var _ = p[1];
                    v.hasMessageWithServerId(_) && (v.deleteMessageByServerId(_, y.defer()), l++);
                }
                if (!h) {
                    var S = i + c + l;
                    y.then(function() {
                        e(a, "sync_end", {
                            full: i,
                            changed: c,
                            deleted: l
                        }), v.markSyncRange(t.OLDEST_SYNC_DATE, n, "XXX", n), r(null, null, S);
                    });
                }
            }, s);
        }),
        performMutation: y(1, function(e, n) {
            var t = this, o = s.Tags, r = this._account, a = new I.Writer("1.3", 1, "UTF-8");
            a.stag(o.Sync).stag(o.Collections).stag(o.Collection), r.conn.currentVersion.lt("12.1") && a.tag(o.Class, "Email"), 
            a.tag(o.SyncKey, this.syncKey).tag(o.CollectionId, this.serverId).tag(o.DeletesAsMoves, "trash" === this.folderMeta.type ? "0" : "1").tag(o.GetChanges, "0").stag(o.Commands);
            try {
                e(a);
            } catch (i) {
                return console.error("Exception in performMutation callee:", i, "\n", i.stack), 
                n("unknown"), void 0;
            }
            a.etag(o.Commands).etag(o.Collection).etag(o.Collections).etag(o.Sync), r.conn.postCommand(a, function(e, a) {
                if (e) return console.error("postCommand error:", e), r._reportErrorIfNecessary(e), 
                n("unknown"), void 0;
                var i, c, l = new I.EventParser(), d = [ o.Sync, o.Collections, o.Collection ];
                l.addEventListener(d.concat(o.SyncKey), function(e) {
                    i = e.children[0].textContent;
                }), l.addEventListener(d.concat(o.Status), function(e) {
                    c = e.children[0].textContent;
                });
                try {
                    l.run(a);
                } catch (u) {
                    return console.error("Error parsing Sync response:", u, "\n", u.stack), n("unknown"), 
                    void 0;
                }
                c === s.Enums.Status.Success ? (t.syncKey = i, n && n(null)) : (console.error("Something went wrong during ActiveSync syncing and we got a status of " + c), 
                n("status:" + c));
            });
        }),
        downloadMessageAttachments: y(2, function(e, n, t) {
            var o = this, r = l.Tags, s = l.Enums.Status, i = a.Tags, c = new I.Writer("1.3", 1, "UTF-8");
            c.stag(r.ItemOperations);
            for (var u in Iterator(n)) {
                var f = u[1];
                c.stag(r.Fetch).tag(r.Store, "Mailbox").tag(i.FileReference, f.part).etag();
            }
            c.etag(), this._account.conn.postCommand(c, function(e, a) {
                if (e) return console.error("postCommand error:", e), o._account._reportErrorIfNecessary(e), 
                t("unknown"), void 0;
                var c, l = {}, u = new I.EventParser();
                u.addEventListener([ r.ItemOperations, r.Status ], function(e) {
                    c = e.children[0].textContent;
                }), u.addEventListener([ r.ItemOperations, r.Response, r.Fetch ], function(e) {
                    var n = null, t = {};
                    for (var o in Iterator(e.children)) {
                        var s = o[1];
                        switch (s.tag) {
                          case r.Status:
                            t.status = s.children[0].textContent;
                            break;

                          case i.FileReference:
                            n = s.children[0].textContent;
                            break;

                          case r.Properties:
                            var a = null, c = null;
                            for (var u in Iterator(s.children)) {
                                var f = u[1], g = f.children[0].textContent;
                                switch (f.tag) {
                                  case i.ContentType:
                                    a = g;
                                    break;

                                  case r.Data:
                                    c = d.decode(g);
                                }
                            }
                            a && c && (t.data = new Blob([ c ], {
                                type: a
                            }));
                        }
                        n && (l[n] = t);
                    }
                }), u.run(a);
                var f = c !== s.Success ? "unknown" : null, g = [];
                for (var h in Iterator(n)) {
                    var v = h[1];
                    l.hasOwnProperty(v.part) && l[v.part].status === s.Success ? g.push(l[v.part].data) : (f = "unknown", 
                    g.push(null));
                }
                t(f, g);
            });
        })
    }, h.ActiveSyncFolderSyncer = m, m.prototype = {
        get syncable() {
            return null !== this.folderConn.serverId;
        },
        get canGrowSync() {
            return !1;
        },
        initialSync: function(e, t, o, r, s) {
            o("sync", !0), this.folderConn.sync(n.NOW(), this.onSyncCompleted.bind(this, r, !0), s);
        },
        refreshSync: function(e, t, o, r, s, a, i) {
            this.folderConn.sync(n.NOW(), this.onSyncCompleted.bind(this, a, !1), i);
        },
        growSync: function() {
            return !1;
        },
        onSyncCompleted: function(e, n, o, r, s) {
            var a = this.folderStorage;
            console.log("Sync Completed!", s, "messages synced"), o || a.markSyncedToDawnOfTime(), 
            this._account.__checkpointSyncCompleted(function() {
                o ? e(o) : n ? (a._curSyncSlice.ignoreHeaders = !1, a._curSyncSlice.waitingOnData = "db", 
                a.getMessagesInImapDateRange(0, null, t.INITIAL_FILL_SIZE, t.INITIAL_FILL_SIZE, a.onFetchDBHeaders.bind(a, a._curSyncSlice, !1, e, null))) : e(o);
            });
        },
        allConsumersDead: function() {},
        shutdown: function() {
            this.folderConn.shutdown();
        }
    };
}), define("activesync/jobs", [ "logic", "mix", "../jobmixins", "../drafts/jobs", "activesync/codepages/AirSync", "activesync/codepages/Email", "activesync/codepages/Move", "module", "require", "exports" ], function(e, n, t, o, r, s, a, i, c, l) {
    function d(e, n, t) {
        return function() {
            var o = Array.slice(arguments), r = o[e], s = this;
            c([ "wbxml" ], function(e) {
                f || (f = e), s.account.withConnection(r, function() {
                    n.apply(s, o);
                }, t);
            });
        };
    }
    function u(n, t) {
        this.account = n, this.resilientServerIds = !0, this._heldMutexReleasers = [], e.defineScope(this, "ActiveSyncJobDriver", {
            accountId: this.account.id
        }), this._state = t, t.hasOwnProperty("suidToServerId") || (t.suidToServerId = {}, 
        t.moveMap = {}), this._stateDelta = {
            serverIdMap: null,
            moveMap: null
        };
    }
    var f;
    l.ActiveSyncJobDriver = u, u.prototype = {
        postJobCleanup: t.postJobCleanup,
        allJobsDone: t.allJobsDone,
        _accessFolderForMutation: function(n, t, o, r, s) {
            var a = this.account.getFolderStorageForFolderId(n), i = this;
            a.runMutexed(s, function(n) {
                i._heldMutexReleasers.push(n);
                var r = a.folderSyncer;
                if (t) i.account.withConnection(o, function() {
                    try {
                        o(r.folderConn, a);
                    } catch (n) {
                        e(i, "callbackErr", {
                            ex: n
                        });
                    }
                }); else try {
                    o(r.folderConn, a);
                } catch (s) {
                    e(i, "callbackErr", {
                        ex: s
                    });
                }
            });
        },
        _partitionAndAccessFoldersSequentially: t._partitionAndAccessFoldersSequentially,
        local_do_modtags: t.local_do_modtags,
        do_modtags: d(1, function(e, n, t) {
            function o(e) {
                return a && -1 !== a.indexOf(e) ? !0 : i && -1 !== i.indexOf(e) ? !1 : void 0;
            }
            var a = t ? e.removeTags : e.addTags, i = t ? e.addTags : e.removeTags, c = o("\\Seen"), l = o("\\Flagged"), d = r.Tags, u = s.Tags, f = null;
            this._partitionAndAccessFoldersSequentially(e.messages, !0, function(e, n, t, o, r) {
                return t = t.filter(function(e) {
                    return !!e;
                }), t.length ? (e.performMutation(function(e) {
                    for (var n = 0; n < t.length; n++) e.stag(d.Change).tag(d.ServerId, t[n]).stag(d.ApplicationData), 
                    void 0 !== c && e.tag(u.Read, c ? "1" : "0"), void 0 !== l && e.stag(u.Flag).tag(u.Status, l ? "2" : "0").etag(), 
                    e.etag(d.ApplicationData).etag(d.Change);
                }, function(e) {
                    e && (f = e), r();
                }), void 0) : (r(), void 0);
            }, function() {
                n(f);
            }, function() {
                f = "aborted-retry";
            }, t, "modtags");
        }),
        check_modtags: function(e, n) {
            n(null, "idempotent");
        },
        local_undo_modtags: t.local_undo_modtags,
        undo_modtags: function(e, n) {
            this.do_modtags(e, n, !0);
        },
        local_do_move: t.local_do_move,
        do_move: d(1, function(e, n) {
            var t = null, o = this.account, r = this.account.getFolderStorageForFolderId(e.targetFolder), s = a.Tags;
            this._partitionAndAccessFoldersSequentially(e.messages, !0, function(e, n, a, i, c) {
                if (a = a.filter(function(e) {
                    return !!e;
                }), !a.length) return c(), void 0;
                if (a = a.filter(function(e) {
                    return !!e;
                }), !a.length) return c(), void 0;
                var l = new f.Writer("1.3", 1, "UTF-8");
                l.stag(s.MoveItems);
                for (var d = 0; d < a.length; d++) l.stag(s.Move).tag(s.SrcMsgId, a[d]).tag(s.SrcFldId, n.folderMeta.serverId).tag(s.DstFldId, r.folderMeta.serverId).etag(s.Move);
                l.etag(s.MoveItems), o.conn.postCommand(l, function(e) {
                    e && (t = e, console.error("failure moving messages:", e)), c();
                });
            }, function() {
                n(t, null, !0);
            }, function() {
                t = "aborted-retry";
            }, !1, "move");
        }),
        check_move: function() {},
        local_undo_move: t.local_undo_move,
        undo_move: function() {},
        local_do_delete: t.local_do_delete,
        do_delete: d(1, function(e, n) {
            var t = null, o = r.Tags;
            s.Tags, this._partitionAndAccessFoldersSequentially(e.messages, !0, function(e, n, r, s, a) {
                return r = r.filter(function(e) {
                    return !!e;
                }), r.length ? (e.performMutation(function(e) {
                    for (var n = 0; n < r.length; n++) e.stag(o.Delete).tag(o.ServerId, r[n]).etag(o.Delete);
                }, function(e) {
                    e && (t = e, console.error("failure deleting messages:", e)), a();
                }), void 0) : (a(), void 0);
            }, function() {
                n(t, null, !0);
            }, function() {
                t = "aborted-retry";
            }, !1, "delete");
        }),
        check_delete: function(e, n) {
            n(null, "idempotent");
        },
        local_undo_delete: t.local_undo_delete,
        undo_delete: function(e, n) {
            n("moot");
        },
        local_do_syncFolderList: function(e, n) {
            n(null);
        },
        do_syncFolderList: d(1, function(e, n) {
            var t, o = this.account, r = o.getFirstFolderWithType("inbox");
            r && null === r.serverId && (t = o.getFolderStorageForFolderId(r.id)), o.syncFolderList(function(e) {
                e || (o.meta.lastFolderSyncAt = Date.now()), n(e ? "aborted-retry" : null, null, !e), 
                t && t.hasActiveSlices && (e || (console.log("Refreshing fake inbox"), t.resetAndRefreshActiveSlices()));
            });
        }, "aborted-retry"),
        check_syncFolderList: function(e, n) {
            n(null, "coherent-notyet");
        },
        local_undo_syncFolderList: function(e, n) {
            n("moot");
        },
        undo_syncFolderList: function(e, n) {
            n("moot");
        },
        local_do_downloadBodies: t.local_do_downloadBodies,
        do_downloadBodies: t.do_downloadBodies,
        check_downloadBodies: t.check_downloadBodies,
        local_do_downloadBodyReps: t.local_do_downloadBodyReps,
        do_downloadBodyReps: t.do_downloadBodyReps,
        check_downloadBodyReps: t.check_downloadBodyReps,
        local_do_download: t.local_do_download,
        do_download: t.do_download,
        check_download: t.check_download,
        local_undo_download: t.local_undo_download,
        undo_download: t.undo_download,
        local_do_sendOutboxMessages: t.local_do_sendOutboxMessages,
        do_sendOutboxMessages: t.do_sendOutboxMessages,
        check_sendOutboxMessages: t.check_sendOutboxMessages,
        local_undo_sendOutboxMessages: t.local_undo_sendOutboxMessages,
        undo_sendOutboxMessages: t.undo_sendOutboxMessages,
        local_do_setOutboxSyncEnabled: t.local_do_setOutboxSyncEnabled,
        local_do_upgradeDB: t.local_do_upgradeDB,
        local_do_purgeExcessMessages: function(e, n) {
            n(null);
        },
        do_purgeExcessMessages: function(e, n) {
            n(null);
        },
        check_purgeExcessMessages: function() {
            return "idempotent";
        },
        local_undo_purgeExcessMessages: function(e, n) {
            n(null);
        },
        undo_purgeExcessMessages: function(e, n) {
            n(null);
        }
    }, n(u.prototype, o.draftsMixins);
}), define("activesync/account", [ "logic", "../a64", "../accountmixins", "../mailslice", "../searchfilter", "activesync/codepages/FolderHierarchy", "./folder", "./jobs", "../util", "../db/folder_info_rep", "module", "require", "exports" ], function(e, n, t, o, r, s, a, i, c, l, d, u, f) {
    function g(e, n, t) {
        return function() {
            var o = Array.slice(arguments), r = o[e], s = this;
            this.withConnection(r, function() {
                n.apply(s, o);
            }, t);
        };
    }
    function h(n, r, s, c, l) {
        this.universe = n, this.id = r.id, this.accountDef = r, this._db = c, e.defineScope(this, "Account", {
            accountId: this.id,
            accountType: "activesync"
        }), l ? (this.conn = l, this._attachLoggerToConnection(this.conn)) : this.conn = null, 
        this.enabled = !0, this.problems = [], this._alive = !0, this.identities = r.identities, 
        this.folders = [], this._folderStorages = {}, this._folderInfos = s, this._serverIdToFolderId = {}, 
        this._deadFolderIds = null, this._syncsInProgress = 0, this._lastSyncKey = null, 
        this._lastSyncResponseWasEmpty = !1, this.meta = s.$meta, this.mutations = s.$mutations;
        for (var d in s) if ("$" !== d[0]) {
            var u = s[d];
            this._folderStorages[d] = new o.FolderStorage(this, d, u, this._db, a.ActiveSyncFolderSyncer), 
            this._serverIdToFolderId[u.$meta.serverId] = d, this.folders.push(u.$meta);
        }
        this.folders.sort(function(e, n) {
            return e.path.localeCompare(n.path);
        }), this._jobDriver = new i.ActiveSyncJobDriver(this, this._folderInfos.$mutationState), 
        this.ensureEssentialOfflineFolders(), t.accountConstructorMixin.call(this, this, this);
    }
    var v, y, p, m = c.bsearchForInsert, _ = s.Enums.Type, S = f.DEFAULT_TIMEOUT_MS = 3e4;
    f.makeUniqueDeviceId = function() {
        return Math.random().toString(36).substr(2);
    }, f.Account = f.ActiveSyncAccount = h, h.prototype = {
        type: "activesync",
        supportsServerFolders: !0,
        toString: function() {
            return "[ActiveSyncAccount: " + this.id + "]";
        },
        withConnection: function(e, n, t) {
            if (!v) return u([ "wbxml", "activesync/protocol", "activesync/codepages" ], function(o, r, s) {
                v = o, y = r, p = s, this.withConnection(e, n, t);
            }.bind(this)), void 0;
            if (!this.conn) {
                var o = this.accountDef;
                this.conn = new y.Connection(o.connInfo.deviceId, o.connInfo.policyKey), this._attachLoggerToConnection(this.conn), 
                this.conn.open(o.connInfo.server, o.credentials.username, o.credentials.password), 
                this.conn.timeout = S;
            }
            this.conn.connected ? n() : this.conn.connect(function(o) {
                if (o) return this._reportErrorIfNecessary(o), this._isBadUserOrPassError(o) && !t && (t = "bad-user-or-pass"), 
                e(t || "unknown"), void 0;
                var r = this.conn.policyKey;
                this.accountDef.connInfo.policyKey !== r && (this.accountDef.connInfo.policyKey = r, 
                this.universe.saveAccountDef(this.accountDef)), n();
            }.bind(this));
        },
        _isBadUserOrPassError: function(e) {
            return e && e instanceof y.HttpError && 401 === e.status;
        },
        _reportErrorIfNecessary: function(e) {
            e && this._isBadUserOrPassError(e) && this.universe.__reportAccountProblem(this, "bad-user-or-pass", "incoming");
        },
        _attachLoggerToConnection: function(n) {
            e.defineScope(n, "ActiveSyncConnection", {
                connectionId: e.uniqueId()
            }), n.onmessage = e.isCensored ? this._onmessage_safe.bind(this, n) : this._onmessage_dangerous.bind(this, n);
        },
        _onmessage_safe: function(n, t, o, r, s, a, i, c) {
            "options" === t ? e(n, "options", {
                special: o,
                status: r.status,
                response: c
            }) : e(n, "command", {
                type: t,
                special: o,
                status: r.status
            });
        },
        _onmessage_dangerous: function(n, t, o, r, s, a, i, c) {
            if ("options" === t) e(n, "options", {
                special: o,
                status: r.status,
                response: c
            }); else {
                var l, d;
                if (i) try {
                    var u = new v.Reader(new Uint8Array(i), p);
                    l = u.dump();
                } catch (f) {
                    l = "parse problem";
                }
                if (c) try {
                    d = c.dump(), c.rewind();
                } catch (f) {
                    d = "parse problem";
                }
                e(n, "command", {
                    type: t,
                    special: o,
                    status: r.status,
                    params: s,
                    extraHeaders: a,
                    sentXML: l,
                    receivedXML: d
                });
            }
        },
        toBridgeWire: function() {
            return {
                id: this.accountDef.id,
                name: this.accountDef.name,
                label: this.accountDef.label,
                path: this.accountDef.name,
                type: this.accountDef.type,
                defaultPriority: this.accountDef.defaultPriority,
                enabled: this.enabled,
                problems: this.problems,
                syncRange: this.accountDef.syncRange,
                syncInterval: this.accountDef.syncInterval,
                notifyOnNew: this.accountDef.notifyOnNew,
                playSoundOnSend: this.accountDef.playSoundOnSend,
                identities: this.identities,
                credentials: {
                    username: this.accountDef.credentials.username,
                    password: this.accountDef.credentials.password
                },
                servers: [ {
                    type: this.accountDef.type,
                    connInfo: this.accountDef.connInfo
                } ]
            };
        },
        toBridgeFolder: function() {
            return {
                id: this.accountDef.id,
                name: this.accountDef.name,
                path: this.accountDef.name,
                type: "account"
            };
        },
        get numActiveConns() {
            return 0;
        },
        checkAccount: function(e) {
            null != this.conn && (this.conn.connected && this.conn.disconnect(), this.conn = null), 
            this.withConnection(function(n) {
                e(n);
            }, function() {
                e();
            });
        },
        __checkpointSyncCompleted: function(e, n) {
            this.saveAccountState(null, e, n || "checkpointSync");
        },
        shutdown: function(e) {
            e && e();
        },
        accountDeleted: function() {
            this._alive = !1, this.shutdown();
        },
        sliceFolderMessages: function(e, n) {
            var t = this._folderStorages[e], r = new o.MailSlice(n, t);
            t.sliceOpenMostRecent(r);
        },
        sortFolderMessages: function(e, n, t) {
            var r = this._folderStorages[e], s = new o.MailSlice(n, r, t);
            r.sliceOpenMostRecent(s);
        },
        searchFolderMessages: function(e, n, t, o) {
            var s = this._folderStorages[e], a = new r.SearchSlice(n, s, t, o);
            return s.sliceOpenSearch(a), a;
        },
        syncFolderList: g(0, function(e) {
            var n = this, t = p.FolderHierarchy.Tags, o = new v.Writer("1.3", 1, "UTF-8");
            o.stag(t.FolderSync).tag(t.SyncKey, this.meta.syncKey).etag(), this.conn.postCommand(o, function(o, r) {
                var s = !1;
                if (o) {
                    if (449 !== o.status) return n._reportErrorIfNecessary(o), e(o), void 0;
                    s = !0;
                }
                var a = new v.EventParser(), i = [], c = p.Common.Enums, l = p.FolderHierarchy.Enums;
                a.addEventListener([ t.FolderSync, t.Status ], function(t) {
                    var r = t.children[0].textContent;
                    if (r !== l.Status.Success) if (r === c.Status.PolicyRefresh || r === c.Status.InvalidPolicyKey || s) {
                        console.log("Policy need refresh!!!");
                        var a = n;
                        n.conn.getPolicyKey(!1, function(n) {
                            return n ? (e(n), void 0) : (a.conn.getPolicyKey(!0, function(n) {
                                return n ? (e(n), void 0) : (a.accountDef.connInfo.policyKey = a.conn.policyKey, 
                                a.universe.saveAccountDef(a.accountDef), void 0);
                            }), void 0);
                        });
                    } else {
                        if (r !== c.Status.RemoteWipeRequested) return o = "Sync folder list error, folder error status: " + r, 
                        n._reportErrorIfNecessary(o), e(o), void 0;
                        n.conn.deviceWipe(function(n) {
                            return n ? (e(n), void 0) : void 0;
                        });
                    }
                }), a.addEventListener([ t.FolderSync, t.SyncKey ], function(e) {
                    n.meta.syncKey = e.children[0].textContent;
                }), a.addEventListener([ t.FolderSync, t.Changes, [ t.Add, t.Delete ] ], function(e) {
                    var o = {};
                    for (var r in Iterator(e.children)) {
                        var s = r[1];
                        o[s.localTagName] = s.children[0].textContent;
                    }
                    e.tag === t.Add ? n._addedFolder(o.ServerId, o.ParentId, o.DisplayName, o.Type) || i.push(o) : n._deletedFolder(o.ServerId);
                });
                try {
                    a.run(r);
                } catch (d) {
                    return console.error("Error parsing FolderSync response:", d, "\n", d.stack), e("unknown"), 
                    void 0;
                }
                for (;i.length; ) {
                    var u = [];
                    for (var f in Iterator(i)) {
                        var g = f[1];
                        n._addedFolder(g.ServerId, g.ParentId, g.DisplayName, g.Type) || u.push(g);
                    }
                    if (u.length === i.length) throw new Error("got some orphaned folders");
                    i = u;
                }
                n.ensureEssentialOnlineFolders(), n.normalizeFolderHierarchy(), console.log("Synced folder list"), 
                e && e(null);
            });
        }),
        _folderTypes: {
            1: "normal",
            2: "inbox",
            3: "drafts",
            4: "trash",
            5: "sent",
            6: "normal",
            12: "normal"
        },
        _junkFolderNames: [ "bulk mail", "correo no deseado", "courrier indésirable", "istenmeyen", "istenmeyen e-posta", "junk", "levélszemét", "nevyžiadaná pošta", "nevyžádaná pošta", "no deseado", "posta indesiderata", "pourriel", "roskaposti", "skräppost", "spam", "spamowanie", "søppelpost", "thư rác", "спам", "דואר זבל", "الرسائل العشوائية", "هرزنامه", "สแปม", "垃圾郵件", "垃圾邮件", "垃圾電郵" ],
        _addedFolder: function(e, t, r, s, i, c) {
            if (!(i || s in this._folderTypes)) return !0;
            var d = r, u = null, f = 0;
            if ("0" !== t) {
                if (u = this._serverIdToFolderId[t], void 0 === u) return null;
                var g = this._folderInfos[u];
                d = g.$meta.path + "/" + d, f = g.$meta.depth + 1;
            }
            var h = this._folderTypes[s];
            if (2 > f) {
                var v = r.toLowerCase();
                -1 !== this._junkFolderNames.indexOf(v) && (h = "junk");
            }
            if (i && (h = i), s === _.DefaultInbox) {
                var y = this.getFirstFolderWithType("inbox");
                if (y) return delete this._serverIdToFolderId[y.serverId], this._serverIdToFolderId[e] = y.id, 
                y.serverId = e, y.name = r, y.path = d, y.depth = f, y;
            }
            var p = this.id + "/" + n.encodeInt(this.meta.nextFolderNum++), S = this._folderInfos[p] = {
                $meta: l.makeFolderMeta({
                    id: p,
                    serverId: e,
                    name: r,
                    type: h,
                    path: d,
                    parentId: u,
                    depth: f,
                    lastSyncedAt: 0,
                    syncKey: "0",
                    version: o.FOLDER_DB_VERSION
                }),
                $impl: {
                    nextId: 0,
                    nextHeaderBlock: 0,
                    nextBodyBlock: 0
                },
                accuracy: [],
                headerBlocks: [],
                bodyBlocks: [],
                serverIdHeaderBlockMapping: {}
            };
            console.log("Added folder " + r + " (" + p + ")"), this._folderStorages[p] = new o.FolderStorage(this, p, S, this._db, a.ActiveSyncFolderSyncer), 
            this._serverIdToFolderId[e] = p;
            var b = S.$meta, I = m(this.folders, b, function(e, n) {
                return e.path.localeCompare(n.path);
            });
            return this.folders.splice(I, 0, b), c || this.universe.__notifyAddedFolder(this, b), 
            b;
        },
        _deletedFolder: function(e, n) {
            var t = this._serverIdToFolderId[e], o = this._folderInfos[t], r = o.$meta;
            console.log("Deleted folder " + r.name + " (" + t + ")"), delete this._serverIdToFolderId[e], 
            delete this._folderInfos[t], delete this._folderStorages[t];
            var s = this.folders.indexOf(r);
            this.folders.splice(s, 1), null === this._deadFolderIds && (this._deadFolderIds = []), 
            this._deadFolderIds.push(t), n || this.universe.__notifyRemovedFolder(this, r);
        },
        _recreateFolder: function(n, t) {
            e(this, "recreateFolder", {
                folderId: n
            });
            var r = this._folderInfos[n];
            r.$impl = {
                nextId: 0,
                nextHeaderBlock: 0,
                nextBodyBlock: 0
            }, r.accuracy = [], r.headerBlocks = [], r.bodyBlocks = [], r.serverIdHeaderBlockMapping = {}, 
            null === this._deadFolderIds && (this._deadFolderIds = []), this._deadFolderIds.push(n);
            var s = this;
            this.saveAccountState(null, function() {
                var e = new o.FolderStorage(s, n, r, s._db, a.ActiveSyncFolderSyncer);
                for (var i in Iterator(s._folderStorages[n]._slices)) {
                    var c = i[1];
                    c._storage = e, c.reset(), e.sliceOpenMostRecent(c);
                }
                s._folderStorages[n]._slices = [], s._folderStorages[n] = e, t(e);
            }, "recreateFolder");
        },
        createFolder: g(3, function(e, n, t, o) {
            var r = this, s = e ? this._folderInfos[e] : "0", a = p.FolderHierarchy.Tags, i = p.FolderHierarchy.Enums.Status, c = p.FolderHierarchy.Enums.Type.Mail, l = new v.Writer("1.3", 1, "UTF-8");
            l.stag(a.FolderCreate).tag(a.SyncKey, this.meta.syncKey).tag(a.ParentId, s).tag(a.DisplayName, n).tag(a.Type, c).etag(), 
            this.conn.postCommand(l, function(e, t) {
                r._reportErrorIfNecessary(e);
                var l, d, u = new v.EventParser();
                u.addEventListener([ a.FolderCreate, a.Status ], function(e) {
                    l = e.children[0].textContent;
                }), u.addEventListener([ a.FolderCreate, a.SyncKey ], function(e) {
                    r.meta.syncKey = e.children[0].textContent;
                }), u.addEventListener([ a.FolderCreate, a.ServerId ], function(e) {
                    d = e.children[0].textContent;
                });
                try {
                    u.run(t);
                } catch (f) {
                    return console.error("Error parsing FolderCreate response:", f, "\n", f.stack), 
                    o("unknown"), void 0;
                }
                if (l === i.Success) {
                    var g = r._addedFolder(d, s, n, c);
                    o(null, g);
                } else l === i.FolderExists ? o("already-exists") : o("unknown");
            });
        }),
        deleteFolder: g(1, function(e, n) {
            var t = this, o = this._folderInfos[e].$meta, r = p.FolderHierarchy.Tags, s = p.FolderHierarchy.Enums.Status;
            p.FolderHierarchy.Enums.Type.Mail;
            var a = new v.Writer("1.3", 1, "UTF-8");
            a.stag(r.FolderDelete).tag(r.SyncKey, this.meta.syncKey).tag(r.ServerId, o.serverId).etag(), 
            this.conn.postCommand(a, function(e, a) {
                t._reportErrorIfNecessary(e);
                var i, c = new v.EventParser();
                c.addEventListener([ r.FolderDelete, r.Status ], function(e) {
                    i = e.children[0].textContent;
                }), c.addEventListener([ r.FolderDelete, r.SyncKey ], function(e) {
                    t.meta.syncKey = e.children[0].textContent;
                });
                try {
                    c.run(a);
                } catch (l) {
                    return console.error("Error parsing FolderDelete response:", l, "\n", l.stack), 
                    n("unknown"), void 0;
                }
                i === s.Success ? (t._deletedFolder(o.serverId), n(null, o)) : n("unknown");
            });
        }),
        sendMessage: g(1, function(e, n) {
            var t = this;
            e.withMessageBlob({
                includeBcc: !0
            }, function(o) {
                if (this.conn.currentVersion.gte("14.0")) {
                    var r = p.ComposeMail.Tags, s = new v.Writer("1.3", 1, "UTF-8", null, "blob");
                    s.stag(r.SendMail).tag(r.ClientId, Date.now().toString() + "@mozgaia").tag(r.SaveInSentItems).stag(r.Mime).opaque(o).etag().etag(), 
                    this.conn.postCommand(s, function(e, o) {
                        return e ? (t._reportErrorIfNecessary(e), console.error(e), n("unknown"), void 0) : (null === o ? (console.log("Sent message successfully!"), 
                        n(null)) : (console.error("Error sending message. XML dump follows:\n" + o.dump()), 
                        n("unknown")), void 0);
                    }, null, null, function() {
                        e.renewSmartWakeLock("ActiveSync XHR Progress");
                    });
                } else this.conn.postData("SendMail", "message/rfc822", o, function(e) {
                    return e ? (t._reportErrorIfNecessary(e), console.error(e), n("unknown"), void 0) : (console.log("Sent message successfully!"), 
                    n(null), void 0);
                }, {
                    SaveInSent: "T"
                }, null, function() {
                    e.renewSmartWakeLock("ActiveSync XHR Progress");
                });
            }.bind(this));
        }),
        getFolderStorageForFolderId: function(e) {
            return this._folderStorages[e];
        },
        getFolderStorageForServerId: function(e) {
            return this._folderStorages[this._serverIdToFolderId[e]];
        },
        getFolderMetaForFolderId: function(e) {
            return this._folderInfos.hasOwnProperty(e) ? this._folderInfos[e].$meta : null;
        },
        ensureEssentialOfflineFolders: function() {
            [ {
                type: "inbox",
                displayName: "Inbox",
                typeNum: _.DefaultInbox
            }, {
                type: "outbox",
                displayName: "outbox",
                typeNum: _.Unknown
            }, {
                type: "localdrafts",
                displayName: "localdrafts",
                typeNum: _.Unknown
            } ].forEach(function(e) {
                this.getFirstFolderWithType(e.type) || this._addedFolder(null, "0", e.displayName, e.typeNum, e.type, !0);
            }, this);
        },
        ensureEssentialOnlineFolders: function(e) {
            e && e();
        },
        normalizeFolderHierarchy: t.normalizeFolderHierarchy,
        scheduleMessagePurge: function(e, n) {
            n && n();
        },
        upgradeFolderStoragesIfNeeded: t.upgradeFolderStoragesIfNeeded,
        runOp: t.runOp,
        getFirstFolderWithType: t.getFirstFolderWithType,
        getFolderByPath: t.getFolderByPath,
        saveAccountState: t.saveAccountState,
        runAfterSaves: t.runAfterSaves,
        allOperationsCompleted: function() {}
    };
}), define("ext/md5", [ "require", "exports", "module" ], function(e, n, t) {
    function o(e) {
        return s(r(a(e)));
    }
    function r(e) {
        return c(l(i(e), 8 * e.length));
    }
    function s(e) {
        try {} catch (n) {
            p = 0;
        }
        for (var t, o = p ? "0123456789ABCDEF" : "0123456789abcdef", r = "", s = 0; s < e.length; s++) t = e.charCodeAt(s), 
        r += o.charAt(15 & t >>> 4) + o.charAt(15 & t);
        return r;
    }
    function a(e) {
        for (var n, t, o = "", r = -1; ++r < e.length; ) n = e.charCodeAt(r), t = r + 1 < e.length ? e.charCodeAt(r + 1) : 0, 
        n >= 55296 && 56319 >= n && t >= 56320 && 57343 >= t && (n = 65536 + ((1023 & n) << 10) + (1023 & t), 
        r++), 127 >= n ? o += String.fromCharCode(n) : 2047 >= n ? o += String.fromCharCode(192 | 31 & n >>> 6, 128 | 63 & n) : 65535 >= n ? o += String.fromCharCode(224 | 15 & n >>> 12, 128 | 63 & n >>> 6, 128 | 63 & n) : 2097151 >= n && (o += String.fromCharCode(240 | 7 & n >>> 18, 128 | 63 & n >>> 12, 128 | 63 & n >>> 6, 128 | 63 & n));
        return o;
    }
    function i(e) {
        for (var n = Array(e.length >> 2), t = 0; t < n.length; t++) n[t] = 0;
        for (var t = 0; t < 8 * e.length; t += 8) n[t >> 5] |= (255 & e.charCodeAt(t / 8)) << t % 32;
        return n;
    }
    function c(e) {
        for (var n = "", t = 0; t < 32 * e.length; t += 8) n += String.fromCharCode(255 & e[t >> 5] >>> t % 32);
        return n;
    }
    function l(e, n) {
        e[n >> 5] |= 128 << n % 32, e[(n + 64 >>> 9 << 4) + 14] = n;
        for (var t = 1732584193, o = -271733879, r = -1732584194, s = 271733878, a = 0; a < e.length; a += 16) {
            var i = t, c = o, l = r, d = s;
            t = u(t, o, r, s, e[a + 0], 7, -680876936), s = u(s, t, o, r, e[a + 1], 12, -389564586), 
            r = u(r, s, t, o, e[a + 2], 17, 606105819), o = u(o, r, s, t, e[a + 3], 22, -1044525330), 
            t = u(t, o, r, s, e[a + 4], 7, -176418897), s = u(s, t, o, r, e[a + 5], 12, 1200080426), 
            r = u(r, s, t, o, e[a + 6], 17, -1473231341), o = u(o, r, s, t, e[a + 7], 22, -45705983), 
            t = u(t, o, r, s, e[a + 8], 7, 1770035416), s = u(s, t, o, r, e[a + 9], 12, -1958414417), 
            r = u(r, s, t, o, e[a + 10], 17, -42063), o = u(o, r, s, t, e[a + 11], 22, -1990404162), 
            t = u(t, o, r, s, e[a + 12], 7, 1804603682), s = u(s, t, o, r, e[a + 13], 12, -40341101), 
            r = u(r, s, t, o, e[a + 14], 17, -1502002290), o = u(o, r, s, t, e[a + 15], 22, 1236535329), 
            t = f(t, o, r, s, e[a + 1], 5, -165796510), s = f(s, t, o, r, e[a + 6], 9, -1069501632), 
            r = f(r, s, t, o, e[a + 11], 14, 643717713), o = f(o, r, s, t, e[a + 0], 20, -373897302), 
            t = f(t, o, r, s, e[a + 5], 5, -701558691), s = f(s, t, o, r, e[a + 10], 9, 38016083), 
            r = f(r, s, t, o, e[a + 15], 14, -660478335), o = f(o, r, s, t, e[a + 4], 20, -405537848), 
            t = f(t, o, r, s, e[a + 9], 5, 568446438), s = f(s, t, o, r, e[a + 14], 9, -1019803690), 
            r = f(r, s, t, o, e[a + 3], 14, -187363961), o = f(o, r, s, t, e[a + 8], 20, 1163531501), 
            t = f(t, o, r, s, e[a + 13], 5, -1444681467), s = f(s, t, o, r, e[a + 2], 9, -51403784), 
            r = f(r, s, t, o, e[a + 7], 14, 1735328473), o = f(o, r, s, t, e[a + 12], 20, -1926607734), 
            t = g(t, o, r, s, e[a + 5], 4, -378558), s = g(s, t, o, r, e[a + 8], 11, -2022574463), 
            r = g(r, s, t, o, e[a + 11], 16, 1839030562), o = g(o, r, s, t, e[a + 14], 23, -35309556), 
            t = g(t, o, r, s, e[a + 1], 4, -1530992060), s = g(s, t, o, r, e[a + 4], 11, 1272893353), 
            r = g(r, s, t, o, e[a + 7], 16, -155497632), o = g(o, r, s, t, e[a + 10], 23, -1094730640), 
            t = g(t, o, r, s, e[a + 13], 4, 681279174), s = g(s, t, o, r, e[a + 0], 11, -358537222), 
            r = g(r, s, t, o, e[a + 3], 16, -722521979), o = g(o, r, s, t, e[a + 6], 23, 76029189), 
            t = g(t, o, r, s, e[a + 9], 4, -640364487), s = g(s, t, o, r, e[a + 12], 11, -421815835), 
            r = g(r, s, t, o, e[a + 15], 16, 530742520), o = g(o, r, s, t, e[a + 2], 23, -995338651), 
            t = h(t, o, r, s, e[a + 0], 6, -198630844), s = h(s, t, o, r, e[a + 7], 10, 1126891415), 
            r = h(r, s, t, o, e[a + 14], 15, -1416354905), o = h(o, r, s, t, e[a + 5], 21, -57434055), 
            t = h(t, o, r, s, e[a + 12], 6, 1700485571), s = h(s, t, o, r, e[a + 3], 10, -1894986606), 
            r = h(r, s, t, o, e[a + 10], 15, -1051523), o = h(o, r, s, t, e[a + 1], 21, -2054922799), 
            t = h(t, o, r, s, e[a + 8], 6, 1873313359), s = h(s, t, o, r, e[a + 15], 10, -30611744), 
            r = h(r, s, t, o, e[a + 6], 15, -1560198380), o = h(o, r, s, t, e[a + 13], 21, 1309151649), 
            t = h(t, o, r, s, e[a + 4], 6, -145523070), s = h(s, t, o, r, e[a + 11], 10, -1120210379), 
            r = h(r, s, t, o, e[a + 2], 15, 718787259), o = h(o, r, s, t, e[a + 9], 21, -343485551), 
            t = v(t, i), o = v(o, c), r = v(r, l), s = v(s, d);
        }
        return Array(t, o, r, s);
    }
    function d(e, n, t, o, r, s) {
        return v(y(v(v(n, e), v(o, s)), r), t);
    }
    function u(e, n, t, o, r, s, a) {
        return d(n & t | ~n & o, e, n, r, s, a);
    }
    function f(e, n, t, o, r, s, a) {
        return d(n & o | t & ~o, e, n, r, s, a);
    }
    function g(e, n, t, o, r, s, a) {
        return d(n ^ t ^ o, e, n, r, s, a);
    }
    function h(e, n, t, o, r, s, a) {
        return d(t ^ (n | ~o), e, n, r, s, a);
    }
    function v(e, n) {
        var t = (65535 & e) + (65535 & n), o = (e >> 16) + (n >> 16) + (t >> 16);
        return o << 16 | 65535 & t;
    }
    function y(e, n) {
        return e << n | e >>> 32 - n;
    }
    t.exports = function(e) {
        return o(e);
    };
    var p = 0;
}), define("activesync/configurator", [ "logic", "../accountcommon", "../a64", "./account", "../date", "../ext/md5", "tcp-socket", "require", "exports" ], function(e, n, t, o, r, s, a, i, c) {
    function l(e, n) {
        function t(e) {
            if (i) {
                var t = i;
                i = null;
                try {
                    t.close();
                } catch (o) {}
                n(e);
            }
        }
        var o = /^https:\/\/([^:/]+)(?::(\d+))?/.exec(e);
        if (!o) return n(null), void 0;
        var r = o[2] ? parseInt(o[2], 10) : 443, s = o[1];
        console.log("checking", s, r, "for security problem");
        var i = a.open(s, r);
        i.onopen = function() {
            i.send(new TextEncoder("utf-8").encode("GET /images/logo.png HTTP/1.1\n\n"));
        }, i.onerror = function(e) {
            var n = null;
            e && "object" == typeof e && /^Security/.test(e.name) && (n = "bad-security"), t(n);
        }, i.ondata = function() {
            t(null);
        };
    }
    var d = e.scope("ActivesyncConfigurator");
    c.account = o, c.configurator = {
        timeout: 3e4,
        _getFullDetailsFromAutodiscover: function(n, t, o, r) {
            e(d, "autodiscover:begin", {
                server: ""
            });
            var s = function(t, o) {
                if (t) {
                    var s = "no-config-info", a = {};
                    return t instanceof n.HttpError ? 401 === t.status ? s = "bad-user-or-pass" : 403 === t.status ? s = "not-authorized" : a.status = t.status : t instanceof n.AutodiscoverDomainError && e(d, "autodiscover.error", {
                        message: t.message
                    }), e(d, "autodiscover:end", {
                        err: s
                    }), r(s, null, a), void 0;
                }
                e(d, "autodiscover:end", {
                    server: o.mobileSyncServer.url
                });
                var i = {
                    type: "activesync",
                    displayName: o.user.name,
                    incoming: {
                        server: o.mobileSyncServer.url,
                        username: o.user.email
                    }
                };
                r(null, i, null);
            };
            o.length > 1 ? n.autodiscover(t.emailAddress, t.password, self.timeout, s, !1) : n.raw_autodiscover(o, t.emailAddress, t.password, self.timeout, !1, s);
        },
        tryToCreateAccount: function(e, n, t, o) {
            i([ "activesync/protocol" ], function(r) {
                return t.incoming.autodiscoverEndpoint ? (this._getFullDetailsFromAutodiscover(r, n, t.incoming.autodiscoverEndpoint, function(t, s, a) {
                    return t ? (o(t, s, a), void 0) : (this._createAccountUsingFullInfo(e, n, s, o, r), 
                    void 0);
                }.bind(this)), void 0) : (this._createAccountUsingFullInfo(e, n, t, o, r), void 0);
            }.bind(this));
        },
        _createAccountUsingFullInfo: function(n, a, i, c, u) {
            e(d, "create:begin", {
                server: i.incoming.server
            });
            var f = {
                username: i.incoming.username,
                password: a.password
            }, g = s(a.deviceId), h = this, v = new u.Connection(g);
            v.open(i.incoming.server, f.username, f.password), v.timeout = o.DEFAULT_TIMEOUT_MS, 
            v.connect(function(o) {
                if (o) {
                    var s, y = {
                        server: i.incoming.server
                    };
                    return o instanceof u.HttpError ? (401 === o.status ? s = "bad-user-or-pass" : 403 === o.status ? s = "not-authorized" : (s = "server-problem", 
                    y.status = o.status), e(d, "create:end", {
                        server: i.incoming.server,
                        err: s
                    }), c(s, null, y), void 0) : (l(i.incoming.server, function(e) {
                        var n;
                        n = e ? "bad-security" : "unresponsive-server", c(n, null, y);
                    }), void 0);
                }
                var p = t.encodeInt(n.config.nextAccountNum++), m = {
                    id: p,
                    name: a.accountName || a.emailAddress,
                    label: a.label || a.emailAddress.split("@")[1].split(".")[0],
                    defaultPriority: r.NOW(),
                    type: "activesync",
                    syncRange: "auto",
                    syncInterval: a.syncInterval || 0,
                    notifyOnNew: a.hasOwnProperty("notifyOnNew") ? a.notifyOnNew : !0,
                    playSoundOnSend: a.hasOwnProperty("playSoundOnSend") ? a.playSoundOnSend : !0,
                    credentials: f,
                    connInfo: {
                        server: i.incoming.server,
                        deviceId: g,
                        policyKey: v.policyKey
                    },
                    identities: [ {
                        id: p + "/" + t.encodeInt(n.config.nextIdentityNum++),
                        name: a.displayName || i.displayName,
                        address: a.emailAddress,
                        replyTo: null,
                        signature: null
                    } ]
                };
                e(d, "create:end", {
                    server: i.incoming.server,
                    id: p
                }), h._loadAccount(n, m, v, function(e) {
                    c(null, e, null);
                });
            });
        },
        recreateAccount: function(e, o, r, s) {
            var a = r.def, i = {
                username: a.credentials.username,
                password: a.credentials.password
            }, c = t.encodeInt(e.config.nextAccountNum++), l = {
                id: c,
                name: a.name,
                label: a.label,
                type: "activesync",
                syncRange: a.syncRange,
                syncInterval: a.syncInterval || 0,
                notifyOnNew: a.hasOwnProperty("notifyOnNew") ? a.notifyOnNew : !0,
                playSoundOnSend: a.hasOwnProperty("playSoundOnSend") ? a.playSoundOnSend : !0,
                credentials: i,
                connInfo: {
                    server: a.connInfo.server,
                    deviceId: a.connInfo.deviceId,
                    policyKey: a.connInfo.policyKey
                },
                identities: n.recreateIdentities(e, c, a.identities)
            };
            this._loadAccount(e, l, null, function(e) {
                s(null, e, null);
            });
        },
        _loadAccount: function(e, n, t, o) {
            var r = e.getNeedCancelAccount();
            if (r && r === n.name) return console.log("Cancel setup this account(account name: " + n.name + ")!!!"), 
            e.resetNeedCancelAccount(), void 0;
            var s = {
                $meta: {
                    nextFolderNum: 0,
                    nextMutationNum: 0,
                    lastFolderSyncAt: 0,
                    syncKey: "0"
                },
                $mutations: [],
                $mutationState: {}
            };
            e.saveAccountDef(n, s), e._loadAccount(n, s, t, o);
        }
    };
});