(function(e, t) {
    "function" == typeof define && define.amd ? define([ "mimefuncs", "mimetypes", "punycode", "addressparser" ], t) : "object" == typeof exports ? module.exports = t(require("mimefuncs"), require("mimetypes"), require("punycode"), require("wo-addressparser")) : e.mailbuild = t(mimefuncs, mimetypes, punycode, addressparser);
})(this, function(e, t, n, o) {
    function r(e, n) {
        this.nodeCounter = 0, n = n || {}, this.baseBoundary = n.baseBoundary || Date.now().toString() + Math.random(), 
        this.date = new Date(), this.rootNode = n.rootNode || this, n.filename && (this.filename = n.filename, 
        e || (e = t.detectMimeType(this.filename.split(".").pop()))), this.parentNode = n.parentNode, 
        this._nodeId = ++this.rootNode.nodeCounter, this._childNodes = [], this._headers = [], 
        e && this.setHeader("content-type", e);
    }
    return r.prototype.createChild = function(e, t) {
        t || "object" != typeof e || (t = e, e = void 0);
        var n = new r(e, t);
        return this.appendChild(n), n;
    }, r.prototype.appendChild = function(e) {
        return e.rootNode !== this.rootNode && (e.rootNode = this.rootNode, e._nodeId = ++this.rootNode.nodeCounter), 
        e.parentNode = this, this._childNodes.push(e), e;
    }, r.prototype.replace = function(e) {
        return e === this ? this : (this.parentNode._childNodes.forEach(function(t, n) {
            t === this && (e.rootNode = this.rootNode, e.parentNode = this.parentNode, e._nodeId = this._nodeId, 
            this.rootNode = this, this.parentNode = void 0, e.parentNode._childNodes[n] = e);
        }.bind(this)), e);
    }, r.prototype.remove = function() {
        if (!this.parentNode) return this;
        for (var e = this.parentNode._childNodes.length - 1; e >= 0; e--) if (this.parentNode._childNodes[e] === this) return this.parentNode._childNodes.splice(e, 1), 
        this.parentNode = void 0, this.rootNode = this, this;
    }, r.prototype.setHeader = function(e, t) {
        var n, o = !1;
        if (!t && e && "object" == typeof e) return e.key && e.value ? this.setHeader(e.key, e.value) : Array.isArray(e) ? e.forEach(function(e) {
            this.setHeader(e.key, e.value);
        }.bind(this)) : Object.keys(e).forEach(function(t) {
            this.setHeader(t, e[t]);
        }.bind(this)), this;
        e = this._normalizeHeaderKey(e), n = {
            key: e,
            value: t
        };
        for (var r = 0, s = this._headers.length; s > r; r++) this._headers[r].key === e && (o ? (this._headers.splice(r, 1), 
        r--, s--) : (this._headers[r] = n, o = !0));
        return o || this._headers.push(n), this;
    }, r.prototype.addHeader = function(e, t) {
        return !t && e && "object" == typeof e ? (e.key && e.value ? this.addHeader(e.key, e.value) : Array.isArray(e) ? e.forEach(function(e) {
            this.addHeader(e.key, e.value);
        }.bind(this)) : Object.keys(e).forEach(function(t) {
            this.addHeader(t, e[t]);
        }.bind(this)), this) : (this._headers.push({
            key: this._normalizeHeaderKey(e),
            value: t
        }), this);
    }, r.prototype.getHeader = function(e) {
        e = this._normalizeHeaderKey(e);
        for (var t = 0, n = this._headers.length; n > t; t++) if (this._headers[t].key === e) return this._headers[t].value;
    }, r.prototype.setContent = function(e) {
        return this.content = e, this;
    }, r.prototype.build = function() {
        var t, n, o = [], r = (this.getHeader("Content-Type") || "").toString().toLowerCase().trim();
        if (this.content && (t = (this.getHeader("Content-Transfer-Encoding") || "").toString().toLowerCase().trim(), 
        (!t || [ "base64", "quoted-printable" ].indexOf(t) < 0) && (/^text\//i.test(r) ? this._isPlainText(this.content) ? (/^.{77,}/m.test(this.content) && (n = !0), 
        t = "7bit") : t = "quoted-printable" : /^multipart\//i.test(r) || (t = t || "base64")), 
        t && this.setHeader("Content-Transfer-Encoding", t)), this.filename && !this.getHeader("Content-Disposition") && this.setHeader("Content-Disposition", "attachment"), 
        this._headers.forEach(function(t) {
            var r, s = t.key, i = t.value;
            switch (t.key) {
              case "Content-Disposition":
                r = e.parseHeaderValue(i), this.filename && (r.params.filename = this.filename), 
                i = this._buildHeaderValue(r);
                break;

              case "Content-Type":
                r = e.parseHeaderValue(i), this._handleContentType(r), n && (r.params.format = "flowed"), 
                "flowed" === String(r.params.format).toLowerCase().trim() && (n = !0), r.value.match(/^text\//) && "string" == typeof this.content && /[\u0080-\uFFFF]/.test(this.content) && (r.params.charset = "utf-8"), 
                i = this._buildHeaderValue(r);
                break;

              case "Bcc":
                return;
            }
            i = this._encodeHeaderValue(s, i), (i || "").toString().trim() && o.push(e.foldLines(s + ": " + i, 76));
        }.bind(this)), this.rootNode === this && (this.getHeader("Date") || o.push("Date: " + this.date.toUTCString().replace(/GMT/, "+0000")), 
        this.getHeader("Message-Id") || o.push("Message-Id: <" + [ 0, 0, 0 ].reduce(function(e) {
            return e + "-" + Math.floor(4294967296 * (1 + Math.random())).toString(16).substring(1);
        }, Date.now()) + "@" + (this.getEnvelope().from || "localhost").split("@").pop() + ">"), 
        this.getHeader("MIME-Version") || o.push("MIME-Version: 1.0")), o.push(""), this.content) {
            switch (t) {
              case "quoted-printable":
                o.push(e.quotedPrintableEncode(this.content));
                break;

              case "base64":
                o.push(e.base64Encode(this.content, "object" == typeof this.content && "binary" || !1));
                break;

              default:
                n ? o.push(e.foldLines(this.content.replace(/\r?\n/g, "\r\n").replace(/^( |From|>)/gim, " $1"), 76, !0)) : o.push(this.content.replace(/\r?\n/g, "\r\n"));
            }
            this.multipart && o.push("");
        }
        return this.multipart && (this._childNodes.forEach(function(e) {
            o.push("--" + this.boundary), o.push(e.build());
        }.bind(this)), o.push("--" + this.boundary + "--"), o.push("")), o.join("\r\n");
    }, r.prototype.getEnvelope = function() {
        var e = {
            from: !1,
            to: []
        };
        return this._headers.forEach(function(t) {
            var n = [];
            "From" === t.key || !e.from && [ "Reply-To", "Sender" ].indexOf(t.key) >= 0 ? (this._convertAddresses(this._parseAddresses(t.value), n), 
            n.length && n[0] && (e.from = n[0])) : [ "To", "Cc", "Bcc" ].indexOf(t.key) >= 0 && this._convertAddresses(this._parseAddresses(t.value), e.to);
        }.bind(this)), e;
    }, r.prototype._parseAddresses = function(e) {
        return [].concat.apply([], [].concat(e).map(function(e) {
            return e && e.address && (e = this._convertAddresses(e)), o.parse(e);
        }.bind(this)));
    }, r.prototype._normalizeHeaderKey = function(e) {
        return (e || "").toString().replace(/\r?\n|\r/g, " ").trim().toLowerCase().replace(/^MIME\b|^[a-z]|\-[a-z]/gi, function(e) {
            return e.toUpperCase();
        });
    }, r.prototype._buildHeaderValue = function(t) {
        var n = [];
        return Object.keys(t.params || {}).forEach(function(o) {
            "filename" === o ? e.continuationEncode(o, t.params[o], 50).forEach(function(e) {
                n.push(e.key + "=" + e.value);
            }) : n.push(o + "=" + this._escapeHeaderArgument(t.params[o]));
        }.bind(this)), t.value + (n.length ? "; " + n.join("; ") : "");
    }, r.prototype._escapeHeaderArgument = function(e) {
        return e.match(/[\s'"\\;\/=]|^\-/g) ? '"' + e.replace(/(["\\])/g, "\\$1") + '"' : e;
    }, r.prototype._handleContentType = function(e) {
        this.contentType = e.value.trim().toLowerCase(), this.multipart = this.contentType.split("/").reduce(function(e, t) {
            return "multipart" === e ? t : !1;
        }), this.boundary = this.multipart ? e.params.boundary = e.params.boundary || this.boundary || this._generateBoundary() : !1;
    }, r.prototype._generateBoundary = function() {
        return "----sinikael-?=_" + this._nodeId + "-" + this.rootNode.baseBoundary;
    }, r.prototype._encodeHeaderValue = function(t, n) {
        switch (t = this._normalizeHeaderKey(t)) {
          case "From":
          case "Sender":
          case "To":
          case "Cc":
          case "Bcc":
          case "Reply-To":
            return this._convertAddresses(this._parseAddresses(n));

          case "Message-Id":
          case "In-Reply-To":
          case "Content-Id":
            return n = (n || "").toString().replace(/\r?\n|\r/g, " "), "<" !== n.charAt(0) && (n = "<" + n), 
            ">" !== n.charAt(n.length - 1) && (n += ">"), n;

          case "References":
            return n = [].concat.apply([], [].concat(n || "").map(function(e) {
                return e = (e || "").toString().replace(/\r?\n|\r/g, " ").trim(), e.replace(/<[^>]*>/g, function(e) {
                    return e.replace(/\s/g, "");
                }).split(/\s+/);
            })).map(function(e) {
                return "<" !== e.charAt(0) && (e = "<" + e), ">" !== e.charAt(e.length - 1) && (e += ">"), 
                e;
            }), n.join(" ").trim();

          default:
            return n = (n || "").toString().replace(/\r?\n|\r/g, " "), e.mimeWordsEncode(n, "Q", 52);
        }
    }, r.prototype._convertAddresses = function(t, o) {
        var r = [];
        return o = o || [], [].concat(t || []).forEach(function(t) {
            t.address ? (t.address = t.address.replace(/^.*?(?=\@)/, function(t) {
                return e.mimeWordsEncode(t, "Q", 52);
            }).replace(/@.+$/, function(e) {
                return "@" + n.toASCII(e.substr(1));
            }), t.name ? t.name && r.push(this._encodeAddressName(t.name) + " <" + t.address + ">") : r.push(t.address), 
            o.indexOf(t.address) < 0 && o.push(t.address)) : t.group && r.push(this._encodeAddressName(t.name) + ":" + (t.group.length ? this._convertAddresses(t.group, o) : "").trim() + ";");
        }.bind(this)), r.join(", ");
    }, r.prototype._encodeAddressName = function(t) {
        return /^[\w ']*$/.test(t) ? t : /^[\x20-\x7e]*$/.test(t) ? '"' + t.replace(/([\\"])/g, "\\$1") + '"' : e.mimeWordEncode(t, "Q", 52);
    }, r.prototype._isPlainText = function(e) {
        return "string" != typeof e || /[\x00-\x08\x0b\x0c\x0e-\x1f\u0080-\uFFFF]/.test(e) ? !1 : !0;
    }, r;
});