define([ "./main-router", "./configparser-main", "./cronsync-main", "./devicestorage-main", "./maildb-main", "./net-main", "../mailapi", "exports" ], function($router, $configparser, $cronsync, $devicestorage, $maildb, $net, $mailapi, exports) {
    var realisticBridge = {
        name: "bridge",
        sendMessage: null,
        process: function(e, n, t) {
            bouncedBridge.sendMessage(e, n, t);
        }
    }, bouncedBridge = {
        name: "bounced-bridge",
        sendMessage: null,
        process: function(e, n, t) {
            realisticBridge.sendMessage(e, n, t);
        }
    };
    $router.register(realisticBridge), $router.register(bouncedBridge);
    var gMailAPI = null, testHelper = {
        name: "testhelper",
        sendMessage: null,
        process: function(uid, cmd, args) {
            if ("create-mailapi" === cmd) gMailAPI = new $mailapi.MailAPI(), gMailAPI.__bridgeSend = function(e) {
                mainBridge.sendMessage(null, null, e);
            }; else if ("runWithBody" === cmd) try {
                var func;
                eval("func = " + args.func + ";"), gMailAPI._getBodyForMessage({
                    id: args.headerId,
                    date: args.headerDate
                }, null, function(e) {
                    console.log("got body, invoking func!");
                    try {
                        func(args.arg, e, function(n) {
                            testHelper.sendMessage(uid, cmd, [ n ]), e.die();
                        });
                    } catch (n) {
                        console.error("problem in runWithBody func", n, "\n", n.stack);
                    }
                });
            } catch (ex) {
                console.error("problem with runWithBody", ex, "\n", ex.stack);
            } else if ("checkDatabaseDoesNotContain" === cmd) {
                var tablesAndKeyPrefixes = args, idb = $maildb._debugDB._db, desiredStores = [], i, checkArgs;
                for (i = 0; i < tablesAndKeyPrefixes.length; i++) checkArgs = tablesAndKeyPrefixes[i], 
                desiredStores.push(checkArgs.table);
                var trans = idb.transaction(desiredStores, "readonly"), results = [], sendResults = function() {
                    testHelper.sendMessage(uid, "checkDatabaseDoesNotContain", [ results ]);
                }, waitCount = tablesAndKeyPrefixes.length;
                tablesAndKeyPrefixes.forEach(function(e) {
                    var n = trans.objectStore(e.table), t = IDBKeyRange.bound(e.prefix, e.prefix + "￰", !1, !1), r = n.get(t);
                    r.onerror = function(e) {
                        results.push({
                            errCode: e.target.errorCode
                        }), 0 === --waitCount && sendResults();
                    }, r.onsuccess = function() {
                        results.push({
                            errCode: null,
                            table: e.table,
                            prefix: e.prefix,
                            hasResult: void 0 !== r.result
                        }), 0 === --waitCount && sendResults();
                    };
                });
            }
        }
    };
    $router.register(testHelper);
    var mainBridge = {
        name: "main-bridge",
        sendMessage: null,
        process: function(e, n, t) {
            gMailAPI && gMailAPI.__bridgeReceive(t);
        }
    };
    $router.register(mainBridge);
    var deviceStorageTestHelper = {
        name: "th_devicestorage",
        sendMessage: null,
        process: function(e, n, t) {
            function r(e, n) {
                e && console.log("devicestorage:", e, n), --s > 0 || (console.log("devicestorage: detach cleanup completed"), 
                a._storage.removeEventListener("change", a._bound_onChange), a._storage = null, 
                a.sendMessage(null, "detached", null));
            }
            if ("attach" === n) this._storage = navigator.getDeviceStorage(t), this._storage.addEventListener("change", this._bound_onChange), 
            this.sendMessage(null, "attached", null); else if ("detach" === n) {
                var s = 1, a = this;
                t.nuke.forEach(function(e) {
                    s++;
                    var n = a._storage.delete(e);
                    n.onsuccess = r.bind(null, e, "success"), n.onerror = r.bind(null, e, "error");
                }), r();
            } else if ("get" === n) {
                var o = this._storage.get(t.path);
                o.onsuccess = function() {
                    this.sendMessage(null, "got", {
                        id: t.id,
                        error: null,
                        blob: o.result
                    });
                }.bind(this), o.onerror = function() {
                    this.sendMessage(null, "got", {
                        id: t.id,
                        error: "" + o.error,
                        blob: null
                    });
                }.bind(this);
            }
        },
        _storage: null,
        _bound_onChange: null,
        _onChange: function(e) {
            this.sendMessage(null, "change", {
                reason: e.reason,
                path: e.path
            });
        }
    };
    deviceStorageTestHelper._bound_onChange = deviceStorageTestHelper._onChange.bind(deviceStorageTestHelper), 
    $router.register(deviceStorageTestHelper), $router.register($configparser), $router.register($cronsync), 
    $router.register($devicestorage), $router.register($maildb), $router.register($net);
});