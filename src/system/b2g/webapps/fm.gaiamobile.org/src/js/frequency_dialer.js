'use strict';(function(exports){var FrequencyDialer=function(){this.currentFreqency=0.0;};FrequencyDialer.prototype.update=function(){var favorite=FrequencyManager.checkFrequencyIsFavorite(this.currentFreqency);var span=favorite?'<span id="favorite-star-icon" class="favorite-icon"></span>':'<span id="favorite-star-icon" class="favorite-icon hidden"></span>';FMElementFrequencyDialer.innerHTML=this.currentFreqency.toFixed(1)+span;if(!mozFMRadio.enabled){return;}
if(StatusManager.status!==StatusManager.STATUS_FAVORITE_SHOWING){return;}
if(typeof FocusManager==='undefined'){return;}
var focusedItem=FocusManager.getCurrentFocusElement();if(focusedItem.id==='frequency'){return;}
var focusedFreqency=FrequencyList.getFrequencyByElement(focusedItem);if(focusedFreqency===this.currentFreqency){return;}
FocusManager.update();};FrequencyDialer.prototype.updateFrequency=function(frequency){if(frequency){this.currentFreqency=frequency;}
this.update();};FrequencyDialer.prototype.getFrequency=function(){return this.currentFreqency;};exports.FrequencyDialer=new FrequencyDialer();})(window);