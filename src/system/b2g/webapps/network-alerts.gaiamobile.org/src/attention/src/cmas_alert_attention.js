import 'l10n';
import 'common-scss';
import React from 'react';
import ReactDOM from 'react-dom';
import BaseComponent from 'base-component';
import ReactSoftKey from 'react-soft-key';

import '../../scss/main.scss';
import '../../scss/alert_attention.scss';
import '../../locales/network-alerts.en-US.properties';
import Rings from '../../src/cmas_alert_rings';
import Store from '../../src/cmas_alert_store';
import Utils from '../../src/cmas_alert_utils';
import DialogRenderer from '../../src/dialog_renderer';
import ScreenManager from '../../src/cmas_screen_manager';
import OptionMenuRenderer from '../../src/option_menu_renderer';

class Attention extends BaseComponent {
  DEBUG = true;
  name = 'Attention';
  telephony = window.navigator.mozTelephony;

  debug(s) {
    if (this.DEBUG) {
      console.log(`-*- CMAS ${this.name} -*- ${s}`);
    }
  }

  constructor(props) {
    super(props);
    this.state = {
      message: Utils.getMessageFromUrlParams()
    };
    window.navigator.mozTelephony.addEventListener('callschanged',
      this.handleCallsChanged.bind(this));
    window.addEventListener('message', this, false);
    window.addEventListener('largetextenabledchanged', () => {
      document.body.classList.toggle('large-text', navigator.largeTextEnabled);
    });
    document.body.classList.toggle('large-text', navigator.largeTextEnabled);
  }

  handleEvent(event) {
    let data = event.data;
    let messageType = data.type;
    let message = data.message;
    this.debug('handleEvent message type -> ' + messageType);
    this.debug('handleEvent message -> ' + JSON.stringify(message));

    switch (messageType) {
      case 'silent':
        Rings.stopSoundAndVibrate();
        break;
      case 'update':
        this.setState({
          message: Store.generateImmutableData(message)
        });
        break;
      default:
        break;
    }
  }

  handleCallsChanged(event) {
    if ('callschanged' !== event.type) {
      return;
    }

    this.telephony.calls.forEach((call) => {
      this.debug(`handleCallsChanged state : ${call.state}`);
      if ('incoming' === call.state) {
        Rings.stopSoundAndVibrate();
      }
    }, this);
  }

  componentDidMount() {
    this.debug('componentDidMount ...');
    Utils.updateAttention(this.state.message.toJS());
  }

  render() {
    this.debug('render ....');

    return (
      <div tabIndex="-1">
        <div className="statusbar-placeholder" />
        <div className="header h1" />
        <ScreenManager message={this.state.message} />
        <ReactSoftKey ref="softkey" />
        <OptionMenuRenderer />
        <DialogRenderer />
      </div>
    );
  }
}

window.navigator.mozL10n.ready(() => {
  ReactDOM.render(<Attention />, document.getElementById('root'));
});
