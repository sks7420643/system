'use strict';

const PRIMARY_SIM_CARD_ID = 0;
const SECOND_SIM_CARD_ID = 1;

// 6.3.2 random time value between 5 and 10 minuts
// to re-post the information to server
const SECOND_TIME_POST_INTERVAL_MIN = 5*60*1000;
const SECOND_TIME_POST_INTERVAL_MAX = 10*60*1000;

// 6.3.2
// timeout for terminal waiting http response is 10 seconds
const XHR_TIMEOUT_MS = 10000;

// every 6 hours to start a new round(2 times = 1 round)
const NEW_ROUND_POST_INTERVAL = 6*60*60*1000;

// 5.1.2
// if wifi is not connected when phone boot up,
// terminal should wait 30 minuts for wifi connection
const WAIT_WIFI_POST_INTERVAL = (1000 * 60 * 30);


//register.success: 
//1 register success without sim information
//2 register success with sim information
const REG_WITHOUT_SIM = 1;
const REG_WITH_SIM = 2;

/*MCC     MNC        SPN
  460     00         China Mobile
  460     01         China Unicom
  460     02         China Mobile
  460     03         China TELECOM
  460     06         China Unicom
  460     07         China Mobile
  460     05         China TELECOM
*/
const  CMCC_MCC_MNC = ["46000", "46002", "46007"];//China Mobile
const  CUCC_MCC_MNC = ["46001", "46006"];//China Unicom
const  CTCC_MCC_MNC = ["46003", "46005"];//China TELECOM

const CMCC = 'CMCC';
const CUCC = 'CUCC';
const CTCC = 'CTCC';
const CU_CUCC = 'TA1059-AAROW1DS27ROW2'; 
const MAX_ROUNDS = 10;
const MAX_ROUNDS_FOR_ONE_BOOT = 3;
const MAX_TIMES_FOR_ONE_ROUND = 3;

function debug(info){
  dump('aiyan DeviceManagerService:: '+info);
}

var DeviceManagerService = {
  
  _totalRounds: 0, //<=MAX_ROUNDS rounds
  _oneBootRounds: 0, //<=MAX_ROUNDS_FOR_ONE_BOOT rounds
  _oneRoundTimes: 0, // <= MAX_TIMES_FOR_ONE_ROUND times,

  // 6.6.1 
  // http://dm.wo.com.cn:6001/register?ver=1.1&model=&manuf=& sign=
  //_url: 'http://dm.wo.com.cn:6001/register?',
  _url: 'http://dm.wo.com.cn:6001/register?',

  _connections: null,
  _iccManager: null,
  _wifiManager: null,



  _isWifiRegisterdDM: false,
  _isDataRegisterdDM: false,
  _isCardAndRadioReady: false,

  preinit: function() {
    this._connections = window.navigator.mozMobileConnections;
    this._iccManager = window.navigator.mozIccManager;
    this._wifiManager = window.navigator.mozWifiManager;

  },

  init: function() {
    debug("init:");
    
    if(!this._connections || !this._iccManager || !this._wifiManager){
      return;
    }

    this.removeAllExistAlarm();
    this.getSettings('register.success')
          .then((value, error) => {
      debug("init register.success value = " + value);
      if (REG_WITH_SIM === value) {
        return;
      } else {
        this._oneBootRounds = 1;
        this._oneRoundTimes = 0;
        this.getSettings('check.total.rounds').then((value, error) => {
          debug("initRounds: total rounds is " + value);
          this._totalRounds = value + 1;
          if (this._totalRounds > 0 && this._totalRounds <= MAX_ROUNDS) {
            this.start();
          }
        }).catch(e => {debug(e);});
      }
    }).catch(e => {debug(e);});
  },

  start: function(){
    debug("start");
    
    window.addEventListener('wifi-statuschange', this);

    // wifi will be connected very soon
    // when insert sim card, but sim card is not ready, 
    // we can not get the information from sim card
    // so we need to wait the cardstatechange
    this.addCardAndRadioStateChangeListener();

    if((this.isCardAndRadioStateReady() || !this.isSimInserted()) &&
        this.getWifiConnectionState()){
      this.checkIfRegister();
    } else if (!this.getWifiConnectionState()) {
      //5.1.2 wait wifi
      this.addAlarm('WAIT_WIFI_POST', WAIT_WIFI_POST_INTERVAL);
    }

    navigator.mozSetMessageHandler('alarm', this.handleAlarm.bind(this));

  },

  close: function mtm_close(){
    debug(" close");
    this.removeNetListerner();

    this.removeAllExistAlarm();
    this.removeCardAndRadioListener();
  },

  removeNetListerner: function(){
    debug("removeNetListerner");
    if (this._connections) {
      for (var i = 0; i < this._connections.length; i++) {
        let conn = this._connections[i];
        if (conn) {
          conn.removeEventListener('datachange', this);
        }
      }
    }

    window.removeEventListener('wifi-statuschange', this);
  },

  removeCardAndRadioListener: function(){
    debug("removeCardAndRadioListener");
    if (this._connections) {
      for (var i = 0; i < this._connections.length; i++) {
        let conn = this._connections[i];
        if (conn) {
          conn.addEventListener('radiostatechange', this);
        }
        if(conn && conn.iccId){
          let iccCard = this._iccManager.getIccById(conn.iccId);
          if (iccCard) {
            iccCard.removeEventListener('cardstatechange', this);
          }
        }
      }
    }
  },

  /**
   * check if phone has registered on server or not.
   * @param {String} acctype: access server network type(wifi or mobile).
   */
  checkIfRegister: function() {
    debug("checkIfRegister");

    let accessType = '';

    if(this.getWifiConnectionState()) {
      accessType = 'WiFi';
    } else if(this.getDataConnectionState()){
      accessType = 'Mobile';
    } else {
      debug("no network!");
      this.restartNetListerners();
      return;
    }

    this.getSettings('register.success')
          .then((value, error) => {
      if (REG_WITH_SIM === value 
        || (REG_WITHOUT_SIM === value && !(this.isSimInserted()))) {
        debug("register state:" + value 
          + ", isSimInserted=" + this.isSimInserted());
        this.close();
        return;
      } else {
        this.updateTotalCheckTimes().then(value => {
          if(this._oneBootRounds > 0 && this._oneBootRounds <= MAX_ROUNDS_FOR_ONE_BOOT) {
            this.startCheck(accessType);
          } else if(this._oneBootRounds > MAX_ROUNDS_FOR_ONE_BOOT){
              this.close();
          }
        }).catch(e => {debug(e);});
      }
    }).catch(e => {debug(e);});
  },

  startCheck: function(accessType) {
    this.generateDeviceInfo(accessType).then((value) => {
      if (!value){
        debug("get device information error");
        return;
      }

      let src = JSON.stringify(value);
      this.generateUrlStr(src).then((url) => {
        if (!url){
          debug("get url error");
          return;
        }
        // encry device info
        this.aesEncrypt(src).then(data => {
          if (!data){
            debug("aesEncrypt error");
            return;
          }
          this.post(url, data, this.postSucess.bind(this), this.postFailed.bind(this));
        }).catch(e => {debug(e);});
      }).catch(e => {debug(e);});
    }).catch(e => {debug(e);});
  },

  updateTotalCheckTimes: function() {
    var self = this;
    return new Promise(function(resolve, reject) {
      self._oneRoundTimes ++;
      if(self._oneRoundTimes > MAX_TIMES_FOR_ONE_ROUND) {
        self._oneRoundTimes = 1;
        self._oneBootRounds ++;
        self.getSettings('check.total.rounds').then((value, error) => {
          self._totalRounds = value + 1;

          debug("updateTotalCheckTimes1:(" + self._totalRounds + ", " 
                                              + self._oneBootRounds + ", "
                                              + self._oneRoundTimes + ")");
        navigator.mozSettings.createLock().set({
            'check.total.rounds': self._totalRounds
        });
          resolve(self._oneBootRounds);
      }).catch(e => {debug(e);});
      } else {
        debug("updateTotalCheckTimes2:(" + self._totalRounds + ", " 
                                              + self._oneBootRounds + ", "
                                              + self._oneRoundTimes + ")");
        navigator.mozSettings.createLock().set({
            'check.total.rounds': self._totalRounds
          });
        resolve(self._oneBootRounds);
      }
    });
  },

  postSucess: function mtm_postSucess(){
    debug("postSucess:(" + this._totalRounds + ", " 
                                            + this._oneBootRounds + ", "
                                            + this._oneRoundTimes + ")");
    // 1: register success without sim information
    // 2: register success with sim information
    // other: not registerd success
    let registerStatus = 0;
    debug("postSucess:isSimInserted:" + this.isSimInserted());
    if(!this.isSimInserted()){
      registerStatus = REG_WITHOUT_SIM;
    } else {
      registerStatus = REG_WITH_SIM;
    }
    debug("postSucess:set register.success:" + registerStatus);
    navigator.mozSettings.createLock().set({
        'register.success': registerStatus
    });
    this.close();
  },

  postFailed: function mtm_postSucess(){
    this.close();
    debug("postFailed:(" + this._totalRounds + ", " 
                                            + this._oneBootRounds + ", "
                                            + this._oneRoundTimes + ")");
    if(this._totalRounds >= MAX_ROUNDS){
      this.close();
    } else if(this._oneBootRounds === MAX_ROUNDS_FOR_ONE_BOOT &&
        this._oneRoundTimes === MAX_TIMES_FOR_ONE_ROUND){
      this.close();
    } else if(this._oneBootRounds <= MAX_ROUNDS_FOR_ONE_BOOT){
      if(this._oneRoundTimes > 0 && this._oneRoundTimes < MAX_TIMES_FOR_ONE_ROUND) {
      // random value between 5 and 10 minuts(5*60*1000 10*60*1000)
      let secondPostInterval = Math.floor(Math.random()
        *(SECOND_TIME_POST_INTERVAL_MAX 
        - SECOND_TIME_POST_INTERVAL_MIN + 1)
        + SECOND_TIME_POST_INTERVAL_MIN);
      debug("secondPostInterval = " + secondPostInterval 
        + " between ("+ SECOND_TIME_POST_INTERVAL_MIN
        +", "+ SECOND_TIME_POST_INTERVAL_MAX+")");
      this.addAlarm('SECOND_TIME_POST', secondPostInterval);
      } else if(this._oneRoundTimes >= MAX_TIMES_FOR_ONE_ROUND) {
        this.getSettings("new.round.interval").then((value, error) => {
          if(value) {
            this.addAlarm('NEW_ROUND_POST', value);
          } else {
      this.addAlarm('NEW_ROUND_POST', NEW_ROUND_POST_INTERVAL);
          }
        });
      }
    } else {
      this.close();
    }
  },

  /**
   * get information from server through http request
   * 200 for registered succuss
   * other response for registered failed
   * https://segmentfault.com/a/1190000004322487
   */
  post: function(url, data, onsuccess, onerror) {
    if (!window.XMLHttpRequest){
      debug("do not support window.XMLHttpRequest");
      return;
    }

    var xhr = new XMLHttpRequest({ mozSystem: true });
    xhr.open('POST', url, true);
    xhr.timeout = XHR_TIMEOUT_MS;

    xhr.setRequestHeader('Content-Type', 'application/encrypted-json');//6.6.1

    xhr.setRequestHeader('Content-Length', data.length);

    xhr.onreadystatechange = function() {
      //readyState(0:unsent,1:opened; 2:headers_received; 3:loading; 4: done)
      // xhr.status
      // 200 ok
      // 400 model or sign is null
      // 410 not exit the phone model in server database
      // 411 (this is the uri decode error)
      // 412 RSA dectypt error
      // 413 data error
      // 414 AES dectypt error

      debug("onreadystatechange:readyState="+ xhr.readyState);
      debug("onreadystatechange:status="+ xhr.status);
      debug("onreadystatechange:responseText="+ xhr.responseText);
      debug("onreadystatechange:responseType="+ xhr.responseType);
      debug("onreadystatechange:responseURL ="+ xhr.responseURL );
      debug("onreadystatechange:responseXML="+ xhr.responseXML);
    };
    xhr.onload = function fmdr_xhr_onload() {
      //// readyState will be 4
      if (xhr.status == 200) {
        onsuccess();
      } else {
        onerror();
      }
    };

    xhr.onerror = function fmd_xhr_onerror() {
      onerror();
    };

    xhr.ontimeout = function fmd_xhr_ontimeout() {
      onerror();
    };

    xhr.send(data);
  },

  /* 6.6.1 http://dm.wo.com.cn:6001/register?ver=1.1&model=&manuf=& sign=
   * ver is 1.1
   * model: terminal type
   * manuf: manufaturen code 
   * deviceinfo: JSON string for device infomation
   */
  generateUrlStr:function(deviceinfo){
    var self = this;
    var urlItem = {};
    urlItem['ver'] = 1.1;
    return new Promise(function(resolve, reject) {
      self.getSettings('deviceinfo.product_model')
              .then((value, error) => {
        //urlItem['model'] = value;
        urlItem['model'] = "TA-1059";
        self.getSettings('deviceinfo.product_manufacturer')
              .then((value, error) => {
          //urlItem['manuf'] = value;
          urlItem['manuf'] = "HMD";
          self.getSign(deviceinfo).then(sign => {
            if (!sign){
              debug("get sign error");
              reject();
              return;
            }
            urlItem['sign'] = sign;
            let arr = new Array();
            let i = 0;
            for(let attr in urlItem) {
              arr[i] = encodeURIComponent(attr) + "=" + encodeURIComponent(urlItem[attr]);
              i++;
            }
            let result = arr.join("&");
            self.getSettings('register.server.url').then((value, error) => {
              let url = value? value : self._url;
              let finalUrl = url + result; 
              debug("generateUrlStr:" + finalUrl);
              resolve(finalUrl);
            }).catch(e => {debug(e);});
          }).catch(e => {debug(e);});
        }).catch(e => {debug(e);});
      }).catch(e => {debug(e);});
    });
  },

  /**
   * 6.1 the device information
   * device information should be JSON formate.
   * return value is JSON object
   */
  generateDeviceInfo: function(acctype){
    var self = this;
    var deviceInfo = {};
    debug("generateDeviceInfo start");
    return new Promise(function(resolve, reject) {
      self.getDeviceInfo(acctype).then((value) => {
        for (let a in value){
          deviceInfo[a] = value[a];
        }
        self.getSimInfor(PRIMARY_SIM_CARD_ID).then((value) => {
          for (let b in value){
            deviceInfo[b] = value[b];
          }
          self.getSimInfor(SECOND_SIM_CARD_ID).then((value) => {
            for (let c in value){
              deviceInfo[c] = value[c];
            }
            deviceInfo['Tag'] = self._totalRounds + "." 
                            + self._oneBootRounds + "."
                            + self._oneRoundTimes
            debug("deviceInfo="+JSON.stringify(deviceInfo));
            resolve(deviceInfo);
          }).catch(e => {debug(e);});
        }).catch(e => {debug(e);});
      }).catch(e => {debug(e);});
    });
  },

  getDeviceInfo: function(acctype) {
    var self = this;
    var deviceInfo = {};
    debug("getDeviceInfo start");
    return new Promise(function(resolve, reject) {
      self.getSettings('deviceinfo.product_manufacturer')
              .then((value, error) => {
          //JWJ: this value is QUALCOMM now in gecko settings.js,I'm afraid that it will affect other modules
          // if change it to HMD, so hardcode it in DM now
          //deviceInfo['Manuf'] = value;
          deviceInfo['Manuf'] = "HMD";
        self.getSettings('deviceinfo.product_model')
              .then((value, error) => {
          //JWJ: Hardcode it as TA-1059 since the setting value is "Nokia 8810 4G" now, not match with ChinaUnicom
          //deviceInfo['Model'] = value;
          deviceInfo['Model'] = "TA-1059";
          /* HWVersion: hardware version*/
          self.getSettings('deviceinfo.hardware')
              .then((value, error) => {
            deviceInfo['HWVersion'] = value? value : '';
            /* SWVersion: software version*/
            //self.getSettings('deviceinfo.software_tag')
            //    .then((value, error) => {
            //  deviceInfo['SWVersion'] = value? value : '';
              //deviceInfo['SWVersion'] = "2.5";
           //JWJ: Get SW version by this way
            var swVersion = navigator.kaiosExtension.getPropertyValue('ro.build.version.fih');
            deviceInfo['SWVersion'] = swVersion? swVersion : '';
              /* os*/
              deviceInfo['OS'] = 'KaiOS';
              self.getSettings('deviceinfo.os')
                  .then((value, error) => {
                deviceInfo['OSVersion'] = value? value : '';
                //time
                self.getSettings('first.bootup.date')
                  .then((value, error) => {
                  deviceInfo['Time'] = value? value : self.saveCurrentTime();
                  //deviceInfo['Time'] = "2018-9-28 12:12:12";
                  deviceInfo['AccType'] = acctype;
                  debug("getDeviceInfo start:"+JSON.stringify(deviceInfo));

                  resolve(deviceInfo);
                }).catch(e => {debug(e);});
              }).catch(e => {debug(e);});
            //}).catch(e => {debug(e);});
          }).catch(e => {debug(e);});
        }).catch(e => {debug(e);});
      }).catch(e => {debug(e);});
    });
  },

  isSimInserted: function() {
    var result = false;
    if (this._connections) {
        for (var i = 0; i < this._connections.length; i++) {
        let conn = this._connections[i];
         if(conn && conn.iccId){
          result = true;
        }
      }
    }
    return result;
  },

  getSimInfor: function mtm_getSimInfo(index){
    var self = this;
    var deviceInfo = {};
    debug("getSimInfor start");
    return new Promise(function(resolve, reject) {
      self.getIMEI(index)
        .then((value, error) => {
        if (value){
          deviceInfo['IMEI' + (index + 1)] = value;
        }

        var conn = self._connections[index];
        if(!conn || !conn.iccId){
          resolve(deviceInfo);
          debug("1getSimInfor:"+JSON.stringify(deviceInfo));
          return;
        }
        

        // 中国移动iccid为20个1、msisdn为11个1、imsi为15个1、cellid和lac为4个1。
        // 中国电信iccid为20个3、msisdn为11个3、imsi为15个3、cellid和lac为4个3
        
        let iccObj = self._iccManager.getIccById(conn.iccId);
        let iccInfo = iccObj ? iccObj.iccInfo : null;
        if (iccInfo) {
          let mcc_mnc = iccInfo.mcc + iccInfo.mnc;
          let carrier = self.getCarrier(mcc_mnc);
          debug("carrier = " + carrier);
          if(carrier === CMCC){
            deviceInfo['ICCID' + (index + 1)] =  '11111111111111111111';
            deviceInfo['IMSI' + (index + 1)] =   '111111111111111';
            deviceInfo['MSISDN' + (index + 1)] = '11111111111';
          } else if(carrier === CTCC){
            deviceInfo['ICCID' + (index + 1)] =  '33333333333333333333';
            deviceInfo['IMSI' + (index + 1)] =   '333333333333333';
            deviceInfo['MSISDN' + (index + 1)] = '33333333333';
          } else {
            deviceInfo['ICCID' + (index + 1)] = conn.iccId;
            deviceInfo['IMSI' + (index + 1)] = iccInfo.imsi;
            deviceInfo['MSISDN' + (index + 1)] = iccInfo.msisdn || iccInfo.mdn;
          }

          deviceInfo['MNC' + (index + 1)] = mcc_mnc;
          deviceInfo['NCLS' + (index + 1)] = self.getNetworkType(conn);

          if(conn.voice && conn.voice.cell){
            let gsmLocationAreaCode = conn.voice.cell.gsmLocationAreaCode;
            let gsmCellId = conn.voice.cell.gsmCellId;
            if(carrier === CMCC){
              deviceInfo['CellID' + (index + 1)] = 1111;
              deviceInfo['LAC' + (index + 1)] = 1111;
            } else if(carrier === CTCC){
              deviceInfo['CellID' + (index + 1)] = 3333;
              deviceInfo['LAC' + (index + 1)] = 3333;
            } else {
              deviceInfo['CellID' + (index + 1)] = gsmCellId;
              deviceInfo['LAC' + (index + 1)] = gsmLocationAreaCode;
            }
          }
          resolve(deviceInfo);
          debug("2getSimInfor card "+index+"::"+JSON.stringify(deviceInfo));
        }
      }).catch(e => {debug(e);});
    });
  },

  getCarrier: function(mcc_mnc){
    var carrier = "";
    if(CMCC_MCC_MNC.indexOf(mcc_mnc) != -1){
      carrier = CMCC;
    } else  if(CUCC_MCC_MNC.indexOf(mcc_mnc) != -1){
      carrier = CUCC;
    } else  if(CTCC_MCC_MNC.indexOf(mcc_mnc) != -1){
      carrier = CTCC;
    }
    return carrier;
  },

  getIMEI: function mtm_getIMEI(aServiceId) {
    if (!this._connections) {
      debug('No mozMobileConnections!!!');
      return;
    }
    var connection = this._connections[aServiceId];

    return new Promise(function(resolve, reject) {
      var request = connection.getDeviceIdentities();
      request.onsuccess = function() {
        var value = request.result.imei;
        resolve(value);
      };
      request.onerror = function() {
        var errorMsg = 'Could not retrieve the IMEI code';
        reject(errorMsg);
      };
    });
  },

  getNetworkType: function mtm_getSupportedNetworkType(connection) {
    /**
     * Type of connection.
     *
     * Possible values: 'gsm', 'gprs', 'edge', 'umts', 'hsdpa', 'hsupa', 'hspa',
     *                  'hspa+', 'is95a', 'is95b', '1xrtt', 'evdo0', 'evdoa',
     *                  'evdob', 'ehrpd', 'lte' or null (unknown).
     */

    // the function is just for telecom, can not be used for other operator
    if(connection.voice){
      if('gsm' === connection.voice.type) {
        return "GSM";
      } else if('lte' === connection.voice.type) {
        return "LTE";
      } else {
        return "WCDMA";
      }
    }
  },


  /**
   * get setting value from DB.
   * @param {String} name.
   */
  getSettings: function(key) {
    var settings = navigator.mozSettings;
    if (!settings) {
      debug('No mozSettings!!!');
    }

    return new Promise(function(resolve, reject) {

      var request = settings.createLock().get(key);
      request.onsuccess = function() {
        var value = request.result[key];
        //debug("getSettings:onsuccess:"+key+"="+value);
        resolve(value);
      };
      request.onerror = function(error) {
        //debug("getSettings " + key +" : " + 'error');
        reject(error);
      };
    });
  },
  //==================================================================================
  /* 6.5 AES encrypt the device information
  */
  aesEncrypt: function mtm_aesEncrypt(src){
    var self = this;
    return new Promise(function(resolve, reject) {
      LazyLoader.load(['js/dmservice/CryptoJSv3.1.2/components/core.js',
        'js/dmservice/CryptoJSv3.1.2/components/enc-base64.js',
        'js/dmservice/CryptoJSv3.1.2/components/md5.js',
        'js/dmservice/CryptoJSv3.1.2/components/evpkdf.js',
        'js/dmservice/CryptoJSv3.1.2/components/cipher-core.js',
        'js/dmservice/CryptoJSv3.1.2/components/mode-ecb.js',
        'js/dmservice/CryptoJSv3.1.2/components/aes.js',
        'js/dmservice/jsrsasign-all-min.js'], function() {
          
        // generate AES key with the md5 
        // abstract is a 128 bit string
        // 128(bit)=32(16 byte hexadecimal)

        let abstract = CryptoJS.MD5(src);
        debug("aesEncrypt:abstract =" + abstract);

        // the key must be the result of CryptoJS.enc.Utf8.parse()
        // or there will be "Error: Malformed UTF-8 data"
        // let key = CryptoJS.enc.Utf8.parse(abstract);
        // the result of md5 is already hex
        let key = abstract;
        debug("CryptoJS:key =" + key);

        // make the abstract as the cipher key for AES
        // AES/ECB/PKCS5Padding
        let encryptedData = CryptoJS.AES.encrypt(src, key, {
          mode: CryptoJS.mode.ECB,
          padding: CryptoJS.pad.Pkcs7
        });
        let encryptedDataStr = encryptedData.toString();
        debug("CryptoJS:encryptedDataStr =" + encryptedDataStr);


        let encryptedStr = encryptedData.ciphertext.toString(); //hex
        debug("CryptoJS:encryptedStr =" + encryptedStr);

        // decrypt method for test======================begin.
        let decryptedData = CryptoJS.AES.decrypt({
          ciphertext: CryptoJS.enc.Hex.parse(encryptedStr)
          }, key, {
          mode: CryptoJS.mode.ECB,
          padding: CryptoJS.pad.Pkcs7
        });
        let decryptedStr = decryptedData.toString(CryptoJS.enc.Utf8);
        debug("CryptoJS:decryptUtf8String =" + decryptedStr);
        // decrypt method for test======================end.

        //need post byte array to server
        var longInt8View = new Int8Array(encryptedStr.length/2);
        debug("longInt8View.length="+longInt8View.length)

        for (var a = 0; a < encryptedStr.length - 1; a += 2) {
          longInt8View[a/2] = parseInt(encryptedStr.substr(a, 2), 16);
          //debug("longInt8View["+a/2+"] = " + longInt8View[a/2]);
        }

        resolve(longInt8View);
      });
    });
  },
  

  /* 6.5 RSA encrypt 
  */
  getSign: function mtm_getSign(src){
    var self = this;
    return new Promise(function(resolve, reject) {
      LazyLoader.load(['js/dmservice/jsrsasign-all-min.js',
        'js/dmservice/CryptoJSv3.1.2/components/core.js',
        'js/dmservice/CryptoJSv3.1.2/components/enc-base64.js',
        'js/dmservice/CryptoJSv3.1.2/components/md5.js'], function() {

        let abstract = CryptoJS.MD5(src).toString();
        debug("getSign:abstract =" + abstract);

        let abstractStr1 = hextorstr(abstract);
        //debug("getSign: abstractStr1 = "+abstractStr1);

        /*****do not change the private key *****/
        let priHex = "30820277020100300D06092A864886F70D0101010500048202613082025D02010002818100A6A4A6632794C861FB336CCC947B1440CEAD48B311A6ABA276F28F72A80B7EA56082B8AAC46D65E6FB72AF98A51E4F9016B0698B285BEEA411CDA3FBA383CB845956F64478CE28E077D257FAD6BBF1C6D32140DB5BEC1DD505A8A029424254822A76860917C2FC45BEA29AD744F4B64C95670153277C47BB012A7724ACF092E502030100010281806B7605EE2F1A1CD6427D790DBC3A5A6728604A5F5F64918DE512AA3A8FEF49AE2FBC5C8592C1D152D097B670C319F6C8BE38E8F6DA01DEB462DEAF7E988566F954817A97AA18B5DA1A0C2B93E038557DEF14F769BFE2DEFA69220BA2F33C05FCD44E7874F09520DCB3B6E0D9758212B14C39B1E661C1CA60333F3D44686E6EC9024100FB0DDDD7727D1BA5A6179B2F6BB7AD6F46E0717B2E969BE29C38CAED20D4C5B39E70778DFEF139234435A938F3DD0F7F2F7378185F8F3DF589E17D4D6E08BD27024100A9ED1379DFD97A580131A8DD84D828896295A5D1369F54A121DF113BE5268236DAC0230802DDE48323B8C4801FCFD233A4786640E0783CD2D7D505668AE8CF130241009C0A6C5E62FCBBD863B9BC6CE708CD35A2DA11EEBC704407BF8DD5233A0AC5AEF4C576A9BD51F56FA40FAE5485CE5AB974A6D27863BCC687FCE1385E8E0BC47502402DA8E094DD0B890E60B3C15654BDE8969200F81F7177FABB9968612F20F1EA07675C3D8D8787AC4178B5CB474A45038C90E33146B98A557DAA353950467F2C35024100C08F59FFA9857C6E09502B78940136D8AD8B31750E6458410037F85753161E49B50E80F83D834DAC2042606C11B9188C02A5658874D2B2E8B2BE477E2435E33F";

        let rsa = new RSAKey();
        rsa.readPKCS8PrvKeyHex(priHex);

        let hSig = rsa.MYsign(abstractStr1);
        let hipherText = hex2b64(hSig);
        //debug("getSign:hSig="+hSig);
        debug("getSign:hipherText="+hipherText);

        resolve(hipherText);
      });
    });
  },

  //==================================================================================
  getWifiConnectionState: function mtm_getWifiConnectionState() {
    let result = false;

    if (!this._wifiManager) {
      result = false;
    }
    if (this._wifiManager.connection.status === 'connected') {
      result = true;
    } else {
      result = false;
    }
    debug("getWifiConnectionState:" + result);
    return result;
  },

  getDataConnectionState: function mtm_getDataConnectionState() {
    var isDataConnected = false;
    if (!this._connections) {
      debug('No mozMobileConnections!!!');
      return isDataConnected;
    }

    var len = this._connections.length;
    

    for (var i = 0; i < len; i++) {
      var con = this._connections[i];
      if (con && con.data) {
        
        isDataConnected = (con.data.connected 
          && !(con.data.roaming) || isDataConnected);
      }
    }
    debug("getDataConnectionState isDataConnected = " + isDataConnected);
    return isDataConnected;
  },

  addDataListener: function(){
    if (this._connections) {
      for (var i = 0; i < this._connections.length; i++) {
        let conn = this._connections[i];
        if (conn && conn.data) {
          conn.addEventListener('datachange', this);
        }
      }
    }
  },

  addCardAndRadioStateChangeListener: function(){
    if (this._connections) {
      for (var i = 0; i < this._connections.length; i++) {
        let conn = this._connections[i];
        if(conn && conn.iccId){
          let iccCard = this._iccManager.getIccById(conn.iccId);
          if (iccCard) {
            iccCard.addEventListener('cardstatechange', this);
          }
        }
        if(conn){
          conn.addEventListener('radiostatechange', this);
        }
      }
    }
  },

  isCardAndRadioStateReady: function(){
    let isReady = false;
    if (this._connections) {
      for (var i = 0; i < this._connections.length; i++) {
        let conn = this._connections[i];
        if(conn && conn.iccId){
          let iccCard = this._iccManager.getIccById(conn.iccId);
          if (iccCard) {
            let cardState = iccCard.cardState;
            let radioState = conn.radioState;
            debug("isCardAndRadioStateReady:cardState = " + cardState);
            debug("isCardAndRadioStateReady:radioState = " + radioState);
            if (cardState === 'ready' && radioState === 'enabled') {
              isReady = true;
              this.removeCardAndRadioListener();
            }
          }
        }
      }
    }
    return isReady;
  },

  restartNetListerners: function () {
    this._isWifiRegisterdDM = false;
    window.addEventListener('wifi-statuschange', this);
    this.isAlarmExist('WAIT_WIFI_POST').then(value => {
      if(!value) {
        this._isDataRegisterdDM = false;
        this.addDataListener();
      }
    });
  },
  handleEvent: function sb_handleEvent(evt) {
    debug("handleEvent:" + evt.type);
    switch (evt.type) {
      case 'wifi-statuschange':
      //sometimes, this event will be received more than one time
      if (this._isWifiRegisterdDM === false && this.getWifiConnectionState()) {
        this.removeExistAlarm('WAIT_WIFI_POST');
        if(this.isCardAndRadioStateReady() || !this.isSimInserted()){
          this.checkIfRegister();
          this._isWifiRegisterdDM = true;
        } 
      }
      break;
      case 'datachange':
      //sometimes, this event will be received more than one time
      if(this._isDataRegisterdDM === false && this.getDataConnectionState()){
        this.checkIfRegister();
        this._isDataRegisterdDM = true;
      }
      break;
      case 'cardstatechange':
      case 'radiostatechange':
      let isReady = this.isCardAndRadioStateReady();
      if(this._isCardAndRadioReady === false && isReady){
        this.checkIfRegister();
        this._isCardAndRadioReady = isReady;
      }
      break;
      default:
      break;
    }
  },

  //=========================================================================
  addAlarm: function mtm_addAlarm(key, interval) {
    debug("addAlarm " + key + " after " + interval);
    if (interval <= 0) {
      debug('addAlarm, interval=' + interval + '; no need to add alarm');
      return;
    }
    var alarmService = window.navigator.mozAlarms;
    if(!alarmService){
      debug('no mozAlarms');
      return;
    }

    var alarmDate = new Date(Date.now() + interval);
    var request = alarmService.add(alarmDate, 'ignoreTimezone', {
        dmAlarmType: key
      });

    request.onsuccess = function () {
      debug('next ' + key + ' AlarmDate is ' + alarmDate);
    }
    request.onerror = function () {
      debug('add alarm failed: ' + this.error);
    }
  },

  handleAlarm: function mtm_handleAlarm(message){
    if(!message || !message.data || !(message.data.dmAlarmType)) {
      return;
    }
    debug("handleAlarm:"+ message.data.dmAlarmType)
    switch (message.data.dmAlarmType) {
      case 'SECOND_TIME_POST':
      case 'NEW_ROUND_POST':
        // if alarm started, but no network, 
        // we need to add the network listen
        this.checkIfRegister();
        break;
      case 'WAIT_WIFI_POST':
        if(this.getWifiConnectionState()){
          this.checkIfRegister();
        } else if(this.getDataConnectionState()){
          this.checkIfRegister();
        } else {
          this.addDataListener();
        }
        break;
      default:
        break;
    }
  },


  removeExistAlarm: function mtm_removeExistAlarm(type) {
    var requst = navigator.mozAlarms.getAll();
    requst.onsuccess = function(e) {
      var result = e.target.result;
      for (var i = 0; i < result.length; i++) {
        if (result[i].id && result[i].data &&
          result[i].data.dmAlarmType === type) {
          debug("removeExistAlarm:" + result[i].data.dmAlarmType);
          navigator.mozAlarms.remove(Number(result[i].id));
        }
      }
    };
    requst.onerror = function(e) {
    };
  },
  isAlarmExist: function (type) {
    var requst = navigator.mozAlarms.getAll();
    var self = this;
    return new Promise(function(resolve, reject) {
      requst.onsuccess = function(e) {
        var result = e.target.result;
        for (var i = 0; i < result.length; i++) {
          if (result[i].id && result[i].data &&
            result[i].data.dmAlarmType === type) {
            debug("isAlarmExist:" + result[i].data.dmAlarmType + "is true!");
            resolve(true);
          }
        }
        resolve(false);
      };
      requst.onerror = function(e) {
        resolve(false);
      };
    });
  },

  removeAllExistAlarm: function mtm_removeAllExistAlarm() {
    var requst = navigator.mozAlarms.getAll();
    requst.onsuccess = function(e) {
      var result = e.target.result;
      for (var i = 0; i < result.length; i++) {
        if (result[i].id && result[i].data &&
          result[i].data.dmAlarmType) {
          debug("removeExistAlarm:" + result[i].data.dmAlarmType);
          navigator.mozAlarms.remove(Number(result[i].id));
        }
      }
    };
    requst.onerror = function(e) {
    };
  },

  // 6.1 first time to boot up
  // time formate: YYYY-MM-DD HH:mm:SS
  //# see http://pubs.opengroup.org/onlinepubs/007908799/xsh/strftime.html
  saveCurrentTime: function() {
    let date = new Date(Date.now());

    let f = new navigator.mozL10n.DateTimeFormat();
    let result = f.localeFormat(date, '%Y-%m-%d') 
            + ' ' + f.localeFormat(date, '%H:%M:%S');
    debug("saveCurrentTime:" + result);
    navigator.mozSettings.createLock().set({
      'first.bootup.date': result
    });
    return result;
  },

  setServer: function() {
    let url = this._url
    navigator.mozSettings.createLock().set({
      'register.server.url': url
    });
  },

  initRoundsForFirstBootUp: function() {
    this._totalRounds = 0;
    this._oneBootRounds = 0;
    this._oneRoundTimes = 0;
    navigator.mozSettings.createLock().set({
          'check.total.rounds': 0
        });
  },
};


/**
 * the first time phone boot up, DeviceManagerService receive 
 * the message "ftudone"
 */
//JWJ: Since the CU value is set after ftudone, it's hard to get it in the same event,
//Gavin added another event fotainited which happens after the CU value is set
//window.addEventListener('ftudone', function initMobileTerminalManager(evt) {
//  window.removeEventListener('ftudone', initMobileTerminalManager);
//  debug('init triggered by ftudone');
window.addEventListener('fotainited', function initMobileTerminalManager(evt) {
  window.removeEventListener('fotainited', initMobileTerminalManager);
  debug('init triggered by fotainited');
  // navigator.mozSettings.createLock().get('first.bootup.date')
  // need save the right firts boot up time, 
  // but the time is updated so sllowly
  // i have to wait
  let curef = navigator.kaiosExtension.getPrefValue("fota.commercial.ref", "na");
  if (curef === CU_CUCC) {
     debug("ftudone: It's CUCC, start DM process");
     DeviceManagerService.preinit();
     if(DeviceManagerService.isCardAndRadioStateReady() || 
        DeviceManagerService.getWifiConnectionState()) {
        window.setTimeout(() => {
        DeviceManagerService.saveCurrentTime();
        DeviceManagerService.init();
        }, 1000*60);
     } else {
        window.addEventListener('moztimechange', function timeChange() {
        debug("ftudone moztimechange");
        DeviceManagerService.saveCurrentTime();
        DeviceManagerService.init();
        window.removeEventListener('moztimechange', timeChange);
        });
     }
     DeviceManagerService.initRoundsForFirstBootUp();
   }
  else { 
    debug(" ftudone: Not CUCC, NO DM"); 
   }
});

/**
 * every time phone boot up, DeviceManagerService receive 
 * the message "ftuskip"
 */
window.addEventListener('ftuskip', function initMobileTerminalManager(evt) {
  window.removeEventListener('ftuskip', initMobileTerminalManager);
  debug('init triggered by ftuskip');
  let curef = navigator.kaiosExtension.getPrefValue("fota.commercial.ref", "na");
  if (curef === CU_CUCC) {
    debug("ftuskip: It's CUCC, start DM process");
    DeviceManagerService.preinit();
    DeviceManagerService.init();
  }
  else {
    debug("ftuskip: Not CUCC, NO DM");
  }
});
