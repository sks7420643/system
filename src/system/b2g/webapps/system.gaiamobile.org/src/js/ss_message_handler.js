'use strict';(function(exports){const COMM_ID="communications";const SMS_ID="sms";const EMAIL_ID="email";const CLOCK_ID="clock";const CALENDAR_ID="calendar";const MUSIC_ID="music";const RADIO_ID="fm";const TEST_ID="sstest";const SYSTEM_ID="system";const APP_BASE=".gaiamobile.org";const APP_PREFIX="app://";const SS_TIMER=3000;var _ssTimer;window.addEventListener('iac-SecondScreen',function(evt){var payload=evt.detail;processMessage(payload);});function ClearMessages(id){SsMessageStack.remove(id);}
function Matches(id,appId){return id===APP_PREFIX+appId+APP_BASE;}
function processMessage(payload){if(payload&&payload.id&&payload.message&&payload.message.reason){var isMatched=false;switch(payload.message.reason){case"Clear":ClearMessages(payload.id);break;case"IncomingCall":if(Matches(payload.id,SYSTEM_ID)){isMatched=true;}
break;case"MissedCall":if(Matches(payload.id,COMM_ID)){isMatched=true;}
break;case"IncomingSMS":if(Matches(payload.id,SMS_ID)){isMatched=true;}
break;case"IncomingMail":if(Matches(payload.id,EMAIL_ID)){isMatched=true;}
break;case"Alarm":if(Matches(payload.id,CLOCK_ID)){isMatched=true;}
break;case"Timer":if(Matches(payload.id,CLOCK_ID)){isMatched=true;}
break;case"Calendar":if(Matches(payload.id,CALENDAR_ID)){isMatched=true;}
break;case"Music":if(Matches(payload.id,MUSIC_ID)){isMatched=true;}
break;case"Radio":if(Matches(payload.id,RADIO_ID)){isMatched=true;}
break;case"Volume":if(Matches(payload.id,SYSTEM_ID)){isMatched=true;}
break;case"Test":if(Matches(payload.id,TEST_ID)){isMatched=true;}
break;}
if(isMatched){SsMessageStack.push(payload);}}}
window.addEventListener('second-screen-updated',function(evt){var needHilite=!evt.detail.clock;var imgData;try{imgData=window.secondaryScreen.canvas.getImageData(0,0,128,128);}catch(ex){console.error('getImageData Exception: '+ex.message);}
if(!!_ssTimer){clearInterval(_ssTimer);}
if(needHilite){ssHilite();}
var fb=window.navigator.mozFb;if(typeof fb!=='undefined'){fb.displayFrame(RGBAtoRGB565(imgData.data));}
_ssTimer=setTimeout(ssDim,SS_TIMER);});window.addEventListener('second-screen-message',function(evt){var payload=evt.detail.message;processMessage(payload);});window.addEventListener('led-press',function(evt){if(!!_ssTimer){clearInterval(_ssTimer);}
ssHilite();_ssTimer=setTimeout(ssDim,SS_TIMER);});function ssHilite(){var fb=window.navigator.mozFb;if(typeof fb!=='undefined'){fb.subScreenBrightness=1;}}
function ssDim(){var fb=window.navigator.mozFb;if(typeof fb!=='undefined'){fb.subScreenBrightness=0.005;}}
function RGBAtoRGB565(data){var outScreen=new Uint8ClampedArray(32768);var i=0;var j=0;var r,g,b;var hi,lo;for(;i<65536;i+=4,j+=2){r=data[i];g=data[i+1];b=data[i+2];hi=((g&28)<<3)|((b&248)>>3);lo=(r&248)|((g&224)>>5);outScreen[j]=hi;outScreen[j+1]=lo;}
return outScreen;}})(window);