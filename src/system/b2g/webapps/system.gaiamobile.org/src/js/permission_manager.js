/* global LazyLoader, Service, applications, ManifestHelper*/
/* global Template*/
'use strict';
(function(exports) {
  /**
   * Handle Web API permissions such as geolocation, getUserMedia
   * @class PermissionManager
   * @requires Applications
   */
  function PermissionManager() {
  }

  PermissionManager.prototype = {

    currentOrigin: undefined,
    permissionType: undefined,
    currentPermissions: undefined,
    currentChoices: {}, //select choices
    isFullscreenRequest: false,
    isVideo: false,
    isAudio: false,
    /**
     * special dialog for camera selection while in app mode and
     * permission is granted
     */
    isCamSelector: false,
    responseStatus: undefined,
    /**
     * A queue of pending requests.
     */
    pending: [],

    /**
     * The ID of the request currently visible on the screen. This has the value
     * "undefined" when there is no request visible on the screen.
     */
    currentRequestId: undefined,
    canFousedItems : [],
    frontState : false,
    currentFocus : 0,
    /**
     * start the PermissionManager to init variables and listeners
     * @memberof PermissionManager.prototype
     */
    start: function pm_start() {
      // Div over in which the permission UI resides.
      this.overlay = document.getElementById('permission-screen');
      this.dialog = document.getElementById('permission-dialog');
      this.title = document.getElementById('permission-title');
      this.message = document.getElementById('permission-message');
      this.moreInfo = document.getElementById('permission-more-info');
      this.infoLink = document.getElementById('info-link');
      this.moreInfoBox = document.getElementById('permission-more-info-box');

      // Remember the choice checkbox
      this.remember = document.getElementById('permission-remember-checkbox');
      this.rememberSection =
        document.getElementById('permission-remember-section');
      this.deviceSelector =
        document.getElementById('permission-device-selector');
      this.devices = document.getElementById('permission-devices');

      window.addEventListener('mozChromeEvent', this);
      window.addEventListener('attentionopening', this);
      window.addEventListener('attentionopened', this);
      window.addEventListener('lockscreen-appopened', this);
      window.addEventListener('screenchange', this);

      /* On home/holdhome pressed, discard permission request.
       * XXX: We should make permission dialog be embededd in appWindow
       * Gaia bug is https://bugzilla.mozilla.org/show_bug.cgi?id=853711
       * Gecko bug is https://bugzilla.mozilla.org/show_bug.cgi?id=852013
       */
      this.discardPermissionRequest = this.discardPermissionRequest.bind(this);
      window.addEventListener('home', this.discardPermissionRequest);
      window.addEventListener('holdhome', this.discardPermissionRequest);

      /* If an application that is currently running needs to get killed for
       * whatever reason we want to discard it's request for permissions.
       */
      window.addEventListener('appterminated', (function(evt) {
        if (evt.detail.origin == this.currentOrigin) {
          this.discardPermissionRequest();
        }
      }).bind(this));

      // Ensure that the focus is not stolen by the permission overlay, as
      // it may appears on top of a <select> element, and just cancel it.
      this.overlay.addEventListener('mousedown', function onMouseDown(evt) {
        evt.preventDefault();
      });

      var self = this;
      this.infoLink.selected = function(){
        self.toggleInfo();
       };

      this.remember.classList.add('checkbox');
      this.rememberSection.selected = function(){
        self.remember.classList.toggle("checked");;
      };

      this.moreInfoBox.selected = function(){
        self.moreInfoBox.classList.toggle("checked");
      };
      this.rememberSection.classList.add('canfocused');

      this.params = {
        menuClassName: 'menu-button',
        header: { l10nId:'message' },
        items: [{
          name: 'Cancel',
          l10nId: 'cancel',
          priority: 1,
          method:this.cancelAction.bind(this)
        },
        {
           name: '',
           priority: 2,
           method:this.SelectAction.bind(this)
        },
        {
          name: 'Confirm',
          l10nId: 'confirm',
          priority: 3,
          method: this.okAction.bind(this)
        }]
      };

      this.params_singal = {
        menuClassName: 'menu-button',
        header: { l10nId:'message' },
        items: [{
          name: 'OK',
          l10nId: 'ok',
          priority: 2,
          method: this.okAction.bind(this)
        }]
      };
    },

    /**
     * Request all strings to show
     * @memberof PermissionManager.prototype
     */
    getStrings: function getStrings(detail) {
      var _ = navigator.mozL10n.get;

      // If we are in fullscreen, the strings are slightly different.
      if (this.isFullscreenRequest) {
        var fullscreenMessage = _(
          'fullscreen-request',
          {
            'origin': detail.fullscreenorigin
          }) || '';
        return {
          message: fullscreenMessage,
          moreInfoText: null
        };
      }

      // If it's a regular request (non-fullscreen), we review
      // the permission and create the strings accordingly
      var permissionID = 'perm-' + this.permissionType.replace(':', '-');
      var app = applications.getByManifestURL(detail.manifestURL);
      var message = '';
      if (detail.isApp) {
        var appName = new ManifestHelper(app.manifest).name;
        message =
          _(
            permissionID + '-appRequest',
            {
              'app': appName
            }
          );
      } else {
        message =
          _(
            permissionID + '-webRequest',
            {
              'site': detail.origin
            }
          );
      }
      var moreInfoText = _(permissionID + '-more-info') || null;
      return {
        message : message,
        moreInfoText: moreInfoText
      };
    },

    /**
     * stop the PermissionManager to reset variables and listeners
     * @memberof PermissionManager.prototype
     */
    stop: function pm_stop() {
      this.currentOrigin = null;
      this.permissionType = null;
      this.currentPermissions = null;
      this.currentChoices = {};
      this.fullscreenRequest = null;
      this.isVideo = false;
      this.isAudio = false;
      this.isCamSelector = false;

      this.responseStatus = null;
      this.pending = [];
      this.currentRequestId = null;

      this.overlay = null;
      this.dialog = null;
      this.title = null;
      this.message = null;
      this.moreInfo = null;
      this.infoLink = null;
      this.moreInfoBox = null;

      this.remember = null;
      this.rememberSection = null;
      this.deviceSelector = null;
      this.devices = null;

      window.removeEventListener('mozChromeEvent', this);
      window.removeEventListener('attentionopening', this);
      window.removeEventListener('attentionopened', this);
      window.removeEventListener('lockscreen-appopened', this);
      window.removeEventListener('screenchange', this);
      window.removeEventListener('home', this.discardPermissionRequest);
      window.removeEventListener('holdhome', this.discardPermissionRequest);
    },

    /**
     * Reset current values
     * @memberof PermissionManager.prototype
     */
    cleanDialog: function pm_cleanDialog() {
      delete this.overlay.dataset.type;
      this.permissionType = undefined;
      this.currentPermissions = undefined;
      this.currentChoices = {};
      this.isVideo = false;
      this.isAudio = false;
      this.isCamSelector = false;
      this.restore();
      //handled in showPermissionPrompt
      if (this.message.classList.contains('hidden')) {
        this.message.classList.remove('hidden');
      }
      if (!this.moreInfoBox.classList.contains('hidden')) {
        this.moreInfoBox.classList.add('hidden');
        navigator.mozL10n.setAttributes(this.infoLink, 'more-info');
      }
      this.devices.innerHTML = '';
      if (!this.deviceSelector.classList.contains('hidden')) {
        this.deviceSelector.classList.add('hidden');
      }
    },

    restore: function pm_restore(){
      var item = this.overlay.querySelectorAll('.permission-focused');
      if(item.length > 0){
        item[0].classList.remove('permission-focused');
      }
      this.moreInfoBox.classList.remove('checked');
      this.moreInfoBox.scrollTo(0, 0);
    },

    /**
     * Queue or show the permission prompt
     * @memberof PermissionManager.prototype
     */
    queuePrompt: function(detail) {
      this.pending.push(detail);
    },

    /**
     * Event handler interface for mozChromeEvent.
     * @memberof PermissionManager.prototype
     * @param {DOMEvent} evt The event.
     */
    handleEvent: function pm_handleEvent(evt) {
      var detail = evt.detail;
      switch (detail.type) {
        case 'permission-prompt':
          if (!!this.currentRequestId) {
            this.queuePrompt(detail);
            return;
          }
          this.handlePermissionPrompt(detail);
          break;
        case 'cancel-permission-prompt':
          this.discardPermissionRequest();
          break;
        case 'fullscreenoriginchange':
          delete this.overlay.dataset.type;
          this.cleanDialog();
          this.handleFullscreenOriginChange(detail);
          break;
      }

      switch (evt.type) {
        case 'attentionopened':
        case 'attentionopening':
          if (this.currentOrigin !== evt.detail.origin) {
            this.discardPermissionRequest();
          }
          break;
        case 'lockscreen-appopened':
          if (this.currentRequestId == 'fullscreen') {
            this.discardPermissionRequest();
          }
          break;
        case 'screenchange':
          if (Service.query('locked') && !detail.screenEnabled) {
            this.discardPermissionRequest();
          }
          break;
        case 'keydown':
          this.handleKeyEvent(evt);
          break;
        case 'keyup':
          evt.preventDefault();
          break;
      }
    },

    handleKeyEvent: function um_handleKeyEvent(evt) {
      if(!this.frontState)
        return;
      switch (evt.key) {
        case "ArrowUp":
          this.focusChange(-1);
          break;
        case "ArrowDown":
          this.focusChange(1);
          break;
      }
      evt.preventDefault();
    },

    focusChange:function um_focusChange(direction){
      var actionDone = false;
      if(this.moreInfoBox.classList.contains('checked')){
        actionDone = this.scrollMoreInfoBox(direction);
      }

      if(!actionDone){
        this.canFousedItems[this.currentFocus].classList.remove('permission-focused');
        this.currentFocus += direction;
        if(this.currentFocus < 0){
          this.currentFocus = this.canFousedItems.length - 1;
        }
        else if (this.currentFocus > this.canFousedItems.length - 1){
          this.currentFocus = 0;
        }
        this.canFousedItems[this.currentFocus].classList.add('permission-focused');
      }
    },

    scrollMoreInfoBox: function um_scrollMoreInfoBox(direction) {
      var maxOffset = this.moreInfoBox.scrollHeight - this.moreInfoBox.clientHeight;
      if((this.moreInfoBox.scrollTop == 0 && direction < 0)
        ||(this.moreInfoBox.scrollTop == maxOffset && direction > 0)){
        this.moreInfoBox.classList.remove('checked');
        this.moreInfoBox.classList.remove('permission-focus');
        return false;
      }
      else{
        var scorlloffset ;
        var distance = this.moreInfoBox.clientHeight - 21;
        if(direction > 0){
          scorlloffset= this.moreInfoBox.scrollTop +  distance;
        }
        else if(direction < 0){
          scorlloffset= this.moreInfoBox.scrollTop -  distance;
        }
        if(scorlloffset < 0){
          scorlloffset = 0;
        }
        else if (scorlloffset > maxOffset){
          scorlloffset = maxOffset;
        }
        this.moreInfoBox.scrollTo(0, scorlloffset);
        return true;
       }
    },

    /**
     * Show the request for the new domain
     * @memberof PermissionManager.prototype
     * @param {Object} detail The event detail object.
     */
    handleFullscreenOriginChange:
      function pm_handleFullscreenOriginChange(detail) {
      // If there's already a fullscreen request visible, cancel it,
      // we'll show the request for the new domain.
      if (this.isFullscreenRequest) {
        this.cancelRequest(this.currentRequestId);
        this.isFullscreenRequest = false;
      }
      if (detail.fullscreenorigin !== Service.currentApp.origin) {
        this.isFullscreenRequest = true;
        detail.id = 'fullscreen';
        this.showPermissionPrompt(detail);
      }
    },

    /**
     * Prepare for permission prompt
     * @memberof PermissionManager.prototype
     * @param {Object} detail The event detail object.
     */
    handlePermissionPrompt: function pm_handlePermissionPrompt(detail) {
      // Clean dialog if was rendered before
      this.cleanDialog();
      this.isFullscreenRequest = false;
      this.currentOrigin = detail.origin;
      this.currentRequestId = detail.id;
      if (detail.permissions) {
        if ('video-capture' in detail.permissions) {
          this.isVideo = true;
          LazyLoader.load('shared/js/template.js');

          // video selector is only for app
          if (detail.isApp && detail.isGranted &&
            detail.permissions['video-capture'].length > 1) {
            this.isCamSelector = true;
          }
        }
        if ('audio-capture' in detail.permissions) {
          this.isAudio = true;
        }
      } else { // work in <1.4 compatible mode
        if (detail.permission) {
          this.permissionType = detail.permission;
          if ('video-capture' === detail.permission) {
            this.isVideo = true;

            LazyLoader.load('shared/js/template.js');
          }
          if ('audio-capture' === detail.permission) {
            this.isAudio = true;
          }
        }
      }

      // Set default permission
      if (this.isVideo && this.isAudio) {
        this.permissionType = 'media-capture';
      } else {
        if (detail.permission) {
          this.permissionType = detail.permission;
        } else if (detail.permissions) {
          this.permissionType = Object.keys(detail.permissions)[0];
        }
      }
      this.overlay.dataset.type = this.permissionType;

      if (this.isAudio || this.isVideo) {
        if (!detail.isApp) {
          // Not show remember my choice option in website
          this.rememberSection.style.display = 'none';
          this.rememberSection.classList.remove('canfocused');
        } else {
          this.rememberSection.style.display = 'block';
        }

        // Set default options
        this.currentPermissions = detail.permissions;
        for (var permission2 in detail.permissions) {
          if (detail.permissions.hasOwnProperty(permission2)) {
            // gecko might not support audio/video option
            if (detail.permissions[permission2].length > 0) {
              this.currentChoices[permission2] =
                detail.permissions[permission2][0];
            }
          }
        }
      }

      if ((this.isAudio || this.isVideo) && !detail.isApp &&
        !this.isCamSelector) {
        // gUM always not remember in web mode
        this.remember.classList.remove('checked');
      } else {
          if(detail.remember){
            this.remember.classList.add('checked');
          }
          else{
            this.remember.classList.remove('checked');
          }
      }

      if (detail.isApp) { // App
        var app = applications.getByManifestURL(detail.manifestURL);

        if (this.isCamSelector) {
          this.title.setAttribute('data-l10n-id', 'title-cam');
        } else {
          this.title.setAttribute('data-l10n-id', 'title-app');
        }
        navigator.mozL10n.setAttributes(
          this.deviceSelector,
          'perm-camera-selector-appRequest',
          { 'app': new ManifestHelper(app.manifest).name }
        );
      } else { // Web content
        this.title.setAttribute('data-l10n-id', 'title-web');
        navigator.mozL10n.setAttributes(
          this.deviceSelector,
          'perm-camera-selector-webRequest',
          { 'site': detail.origin }
        );
      }

      var self = this;
      this.showPermissionPrompt(detail);
      this.updateNavigation();
      this.canFousedItems[0].classList.add('permission-focused');
      this.currentFocus = 0;
    },

    updateNavigation:function pm_updateNavigation(){
      this.canFousedItems = [];
      this.canFousedItems = this.overlay.querySelectorAll('.canfocused');
    },

    /**
     * Send permission choice to gecko
     * @memberof PermissionManager.prototype
     */
    dispatchResponse: function pm_dispatchResponse(id, type, remember) {
      if (this.isCamSelector) {
        remember = true;
      }
      this.responseStatus = type;

      var response = {
        id: id,
        type: type,
        remember: remember
      };

      if (this.isVideo || this.isAudio || this.isCamSelector) {
        response.choices = this.currentChoices;
      }
      var event = document.createEvent('CustomEvent');
      event.initCustomEvent('mozContentEvent', true, true, response);
      window.dispatchEvent(event);
    },

    /**
     * Hide prompt
     * @memberof PermissionManager.prototype
     */
    hidePermissionPrompt: function pm_hidePermissionPrompt(){
      this.restore();
      SoftkeyHelper.hide("permission_manager");
      this.overlay.classList.remove('visible');
      this.devices.classList.remove('visible');
      window.removeEventListener("keydown", this);
      window.removeEventListener("keyup", this);
      this.currentRequestId = undefined;
      // Cleanup the event handlers.
      this.moreInfo.classList.add('hidden');
      // XXX: This is telling AppWindowManager to focus the active app.
      // After we are moving into AppWindow, we need to remove that
      // and call this.app.focus() instead.
      this.publish('permissiondialoghide');
      this.frontState = false;
    },

    publish: function(eventName, detail) {
      var event = document.createEvent('CustomEvent');
      event.initCustomEvent(eventName, true, true, detail);
      window.dispatchEvent(event);
    },

    /**
     * Show the next request, if we have one.
     * @memberof PermissionManager.prototype
     */
    showNextPendingRequest: function pm_showNextPendingRequest() {
      if (this.pending.length === 0) {
        return;
      }

      var request = this.pending.shift();

      if ((this.currentOrigin === request.origin) &&
        (this.permissionType === Object.keys(request.permissions)[0])) {
        if (this.responseStatus !== undefined) {
          this.dispatchResponse(request.id, this.responseStatus,
          this.remember.classList.contains('checked'));
        }
        this.showNextPendingRequest();
        return;
      }

      this.handlePermissionPrompt(request);
    },

    /**
     * Event listener function for the yes/no buttons.
     * @memberof PermissionManager.prototype
     */
    cancelAction:function pm_cancelAction(){
      if (this.currentRequestId != 'fullscreen'){
        this.responseStatus = 'permission-deny';
        this.dispatchResponse(
          this.currentRequestId,
          'permission-deny',
          this.remember.classList.contains('checked')
        );
      }
      else{
        document.mozCancelFullScreen();
      }
      this.hidePermissionPrompt();
      this.showNextPendingRequest();
    },

    okAction:function pm_okAction(){
      if (this.currentRequestId != 'fullscreen'){
        this.responseStatus = 'permission-allow';
        this.dispatchResponse(
          this.currentRequestId,
          'permission-allow',
          this.remember.classList.contains('checked')
        );
      }
      this.hidePermissionPrompt();
      this.showNextPendingRequest();
    },

    SelectAction:function pm_SelectAction(){
      var item = this.overlay.querySelectorAll('.permission-focused');
      item[0].selected();
    },

    toggleInfo: function pm_toggleInfo() {
      this.moreInfoBox.classList.toggle('hidden');
      if(this.moreInfoBox.classList.contains('hidden')){
        navigator.mozL10n.setAttributes(this.infoLink, 'more-info');
        this.moreInfoBox.classList.remove('checked');
        this.moreInfoBox.classList.remove('canfocused');
      }
      else{
        navigator.mozL10n.setAttributes(this.infoLink, 'hide-info');
        this.moreInfoBox.classList.add('canfocused');
      }
      this.updateNavigation();
    },

    /**
     * Form the media source selection list
     * @memberof PermissionManager.prototype
     */
    listDeviceOptions: function pm_listDeviceOptions() {
      var _ = navigator.mozL10n.get;
      var self = this;
      var checked;
      var firstChild = null;

      // show description
      this.deviceSelector.classList.remove('hidden');
      // build device list
      this.currentPermissions['video-capture'].forEach(function(option) {
        var item_li = document.createElement('li');
        item_li.className = 'device-cell';
        var info = document.createElement('div');
        info.textContent = _('device-' + option);
        item_li.appendChild(info);

        var checkContainer = document.createElement('label');
        checkContainer.classList.add('checkbox');
        item_li.appendChild(checkContainer);
        item_li.selected = function(){
          if(!checkContainer.classList.contains('checked')){
            var item = self.devices.querySelectorAll('.checked');
            if(item.length > 0){
              item[0].classList.remove('checked');
            }
            checkContainer.classList.add("checked");
            self.currentChoices['video-capture'] = option;
          }
        };
        self.devices.appendChild(item_li);
        item_li.classList.add('canfocused');
        if(firstChild == null){
            firstChild = checkContainer;
        }
      });
      firstChild.classList.add("checked");
      this.devices.classList.add('visible');
    },

    /**
     * Put the message in the dialog.
     * @memberof PermissionManager.prototype
     */
    showPermissionPrompt:
      function pm_showPermissionPrompt(detail) {
      // Note plain text since this may include text from
      // untrusted app manifests, for example.
      var text = this.getStrings(detail);
      this.message.textContent = text.message;
      if (text.moreInfoText &&
          text.moreInfoText &&
          text.moreInfoText.length > 0) {
        // Show the "More info… " link.
        this.moreInfo.classList.remove('hidden');
        this.infoLink.classList.add('canfocused');
        this.moreInfoBox.classList.remove('canfocused');
        this.moreInfoBox.textContent = text.moreInfoText;
      }
      else{
        this.infoLink.classList.remove('canfocused');
      }

      this.currentRequestId = detail.id;

      // Not show the list if there's only 1 option
      if (this.isVideo && this.currentPermissions['video-capture'].length > 1) {
        this.listDeviceOptions();
      }

      // Set event listeners for the yes and no buttons
      var isSharedPermission = this.isVideo || this.isAudio ||
           this.permissionType === 'geolocation';

      // customize camera selector dialog
      if (this.isCamSelector) {
        this.message.classList.add('hidden');
        this.rememberSection.style.display = 'none';
        this.rememberSection.classList.remove('canfocused');
        this.buttons.dataset.items = 1;
      }
      this.front();

      SoftkeyHelper.register(this, "permission_manager");
      window.addEventListener("keydown", this);
      window.addEventListener("keyup", this);
      // Make the screen visible
      this.overlay.classList.add('visible');
    },

    front : function (){
      var self = this;
      if (this.isCamSelector) {
        SoftkeyHelper.init(this.params_singal, function () {
          self.responseStatus = undefined;
          self.hidePermissionPrompt();
          self.showNextPendingRequest();
        });
      }
      else{
        SoftkeyHelper.init(this.params,function(){
          self.responseStatus = undefined
          self.hidePermissionPrompt();
          self.showNextPendingRequest();
        });
      }
      this.frontState = true;
    },

    back : function (){
      this.frontState = false;
    },
    /**
     * Cancels a request with a specfied id. Request can either be
     * currently showing, or pending. If there are further pending requests,
     * the next is shown.
     * @memberof PermissionManager.prototype
     */
    cancelRequest: function pm_cancelRequest(id) {
      if (this.currentRequestId === id) {
        // Request is currently being displayed. Hide the permission prompt,
        // and show the next request, if we have any.
        this.hidePermissionPrompt();
        this.showNextPendingRequest();
      } else {
        // The request is currently not being displayed. Search through the
        // list of pending requests, and remove it from the list if present.
        for (var i = 0; i < this.pending.length; i++) {
          if (this.pending[i].id === id) {
            this.pending.splice(i, 1);
            break;
          }
        }
      }
    },

    /**
     * Clean current request queue and
     * send refuse permission request message to gecko
     * @memberof PermissionManager.prototype
     */
    discardPermissionRequest: function pm_discardPermissionRequest() {
      if (this.currentRequestId === undefined ||
          this.currentRequestId === null) {
        return;
      }

      if (this.currentRequestId == 'fullscreen') {
        this.isFullscreenRequest = false;
        document.mozCancelFullScreen();
      } else {
        this.dispatchResponse(this.currentRequestId, 'permission-deny', false);
      }

      this.hidePermissionPrompt();
      this.pending = [];
    }
  };

  exports.PermissionManager = PermissionManager;

})(window);
