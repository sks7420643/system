'use strict';

/* global CallLog, CallLogDBManager, Contacts,
          KeypadManager, LazyLoader, Notification,
          NotificationHelper, SettingsListener, SimSettingsHelper,
          TonePlayer, gTonesFrequencies, NavigationMap,
          TelephonyHelper, Utils, Voicemail, MozActivity */

var NavbarManager = {
  init: function nm_init() {
    // binding now so that we can remove the listener in unit tests
    this.update = this.update.bind(this);
    this.update();
    window.addEventListener('hashchange', this.update);
  },
  resourcesLoaded: false,
  dialerOpenCalllog: false,
  dialerStartedWithNumber: false,
  /*
   * Ensures resources are loaded
   */
  ensureResources: function(cb) {
    if (this.resourcesLoaded) {
      if (cb && typeof cb === 'function') {
        cb();
      }
      return;
    }
    var self = this;
    LazyLoader.load(['/shared/js/async_storage.js',
                     '/shared/js/notification_helper.js',
                     '/shared/js/simple_phone_matcher.js',
                     '/shared/js/contact_photo_helper.js',
                     '/shared/js/dialer/contacts.js',
                     '/dialer/js/voicemail.js',
                     '/dialer/js/call_log.js'], function rs_loaded() {
                    self.resourcesLoaded = true;
                    if (cb && typeof cb === 'function') {
                      cb();
                    }
                  });
    document.body.classList.toggle('large-text', navigator.largeTextEnabled);
  },

  update: function nm_update() {
    var destination = window.location.hash;
    switch (destination) {
      case '#call-log-view':
        this.ensureResources(function() {
          CallLog.init();
        });
        break;
    }
  },

  hide: function() {
    var views = document.getElementById('views');
    views.classList.add('hide-toolbar');
  },

  show: function() {
    var views = document.getElementById('views');
    views.classList.remove('hide-toolbar');
    SecondScreen.clearMessages();
  }
};

var CallHandler = (function callHandler() {
  var COMMS_APP_ORIGIN = document.location.protocol + '//' +
    document.location.host;

  /* === Settings === */
  var screenState = null;
  var engineeringModeKey = null;

  // Added by yingsen.zhang@t2mobile.com 20180516 begin
  // Added for bug 4452, do not trace specified numbers
  // according to customization value
  var TAB_NUMBERS_SHOULD_NOT_TRACED = ['016'];
  var mNumbersTracedCustomizationValue = true;
  var mNumbersShouldNotTraced = null;
  // Added by yingsen.zhang@t2mobile.com 20180516 end

  /* === WebActivity === */
  function handleActivity(activity) {
    // Workaround here until the bug 787415 is fixed
    // Gecko is sending an activity event in every multiple entry point
    // instead only the one that the href match.
    if (activity.source.data && activity.source.data.type == 'dialer_open_calllog') {
      NavbarManager.dialerOpenCalllog = true;
    } else {
      NavbarManager.dialerOpenCalllog = false;
    }

    if (activity.source.name === 'call-log') {
      if (window.location.hash != '#call-log-view') {
        window.location.hash = '#call-log-view';
      }
    }
  }

  /* === Notifications support === */

  /**
   * Retrieves the parameters from an URL and forms an object with them.
   *
   * @param {String} input A string holding the parameters attached to an URL.
   * @return {Object} An object built using the parameters.
   */
  function deserializeParameters(input) {
    var rparams = /([^?=&]+)(?:=([^&]*))?/g;
    var parsed = {};

    input.replace(rparams, function($0, $1, $2) {
      parsed[$1] = decodeURIComponent($2);
    });

    return parsed;
  }

  function handleNotification(evt) {
    if (!evt.clicked) {
      return;
    }

    navigator.mozApps.getSelf().onsuccess = function gotSelf(selfEvt) {
      var app = selfEvt.target.result;
      app.launch('call_log');
      var location = document.createElement('a');
      location.href = evt.imageURL;
      window.location.hash = '#call-log-view';
    };
  }

  /* === Telephony Call Ended Support === */
  function sendNotification(number, serviceId) {
    LazyLoader.load('/dialer/js/utils.js', function() {
      Contacts.findByNumber(number, function lookup(contact, matchingTel) {
        const body = 'missedCall';
        let icon = 'call-missed';
        if (navigator.mozMobileConnections.length > 1 &&
          navigator.mozMobileConnections[0].iccId &&
          navigator.mozMobileConnections[1].iccId) {
          icon += `-sim${serviceId + 1}`;
        } else {
          //show missed call on second screen
         var message = {
            reason: "MissedCall",
            photo: '',
            name: '',
            number: number
          }
          if (contact && contact.name)
              message.name = contact.name[0];
          if(contact && contact.photo)
              message.photo = 'url(' + URL.createObjectURL(contact.photo[0]) + ')';
          SecondScreen.showMessage(message);
        }

        var title;
        if (!number) {
          title = 'from-withheld-number';
        } else if (contact) {
          var primaryInfo = Utils.getPhoneNumberPrimaryInfo(matchingTel,
            contact);
          if (primaryInfo) {
            if (primaryInfo !== matchingTel.value) {
              // primaryInfo is an object here
              title = { raw: primaryInfo.toString() };
            } else {
              title = { raw: primaryInfo };
            }
          } else {
            title = 'from-withheld-number';
          }
        } else {
          title = { raw: number };
        }

        navigator.mozApps.getSelf().onsuccess = function getSelfCB(evt) {
          var app = evt.target.result;

          var clickCB = function() {
            app.launch('call_log');
            window.location.hash = '#call-log-view';
          };

          NotificationHelper.send(title, {
            bodyL10n: body,
            icon: icon
          }).then(function(notification) {
            notification.addEventListener('click', clickCB);
          });
        };
      });
    });
  }

  function callEnded(data) {
    var highPriorityWakeLock = navigator.requestWakeLock('high-priority');
    var number = data.number;
    var incoming = data.direction === 'incoming';

    NavbarManager.ensureResources(function() {
      if (incoming && !data.duration) {
        sendNotification(number, data.serviceId);
      }
      if (!isValidNumber(number)) {
        return;
      }

      // Added by yingsen.zhang@t2mobile.com 20180516 begin
      // Added for bug 4452, do not trace specified numbers
      // according to customization value
      console.log('Dialer callEnded, number: ' + number);
      if (mNumbersShouldNotTraced && (mNumbersShouldNotTraced.indexOf(number) !== -1)) {
        console.log('Dialer callEnded, number ' + number + ' should not be traced');
        return;
      }
      // Added by yingsen.zhang@t2mobile.com 20180516 end

      var isVoicemailNumber = Voicemail.check(number);
      // Store and display the call that ended
      if (CallLog.callLogReady) {
        _addCallLog(data, isVoicemailNumber, highPriorityWakeLock);
      } else {
        window.addEventListener('call-log-ready', function addCallLog() {
          window.removeEventListener('call-log-ready', addCallLog);
          _addCallLog(data, isVoicemailNumber, highPriorityWakeLock);
        });
      }
    });
  }

  function _addCallLog(data, isVoicemailNumber, wakeLock) {
    var incoming = data.direction === 'incoming';
    var isVideoCall = data.isVt;
    var entry = {
      date: Date.now() - parseInt(data.duration),
      duration: data.duration,
      type: incoming ? (isVideoCall ? 'videoCallIncoming' : 'incoming') :
                       (isVideoCall ? 'videoCallDialing' : 'dialing'),
      number: data.number,
      serviceId: data.serviceId,
      emergency: data.emergency || false,
      voicemail: isVoicemailNumber,
      status: (incoming && data.duration > 0) ? 'connected' : null
    };
    CallLogDBManager.add(entry, function (logEntry) {
      const latestClassName = NavigationMap.latestClassName;
      const latestNavId = NavigationMap.latestNavId;
      CallLog.appendGroupAndSaveCache(logEntry);
      CallLog.selectTabByClassName(latestClassName, latestNavId);

      // A CDMA call can contain two calls. If it only has one call,
      // we have nothing left to do and release the lock.
      if (!data.secondNumber) {
        var curTab = CallLog.getCurrentTabName();
        var editMode = document.body.classList.contains('recents-edit');
        if (!editMode && !OptionHelper.CallInfoVisible && curTab === 'all') {
          NavigationMap.reset();
        }
        wakeLock.unlock();
        return;
      }

      _addSecondCdmaCall(data, isVoicemailNumber, wakeLock);
    });
  }

  function _addSecondCdmaCall(data, isVoicemailNumber, wakeLock) {
    var entryCdmaSecond = {
      date: Date.now() - parseInt(data.duration),
      duration: data.duration,
      type: 'incoming',
      number: data.secondNumber,
      serviceId: data.serviceId,
      emergency: false,
      voicemail: isVoicemailNumber,
      status: 'connected'
    };

    CallLogDBManager.add(entryCdmaSecond, function(logGroupCdmaSecondCall) {
      CallLog.appendGroupAndSaveCache(logGroupCdmaSecondCall);
      if (NavigationMap.latestClassName === 'missed-call') {
        CallLog.selectTab('missed');
      }
      var curTab = CallLog.getCurrentTabName();
      var editMode = document.body.classList.contains('recents-edit');
      if (!editMode && !OptionHelper.CallInfoVisible && curTab === 'all') {
        NavigationMap.reset();
      }
      wakeLock.unlock();
    });
  }

  function isValidNumber(number) {
    // Including 0-9#+*, and null
    var validExp = /^(?!,)([0-9#+*,]){0,50}$/;
    var sanitizedNumber = number.replace(/(\s|-|\.|\(|\))/g, '');
    return validExp.test(sanitizedNumber);
  }

  /* === postMessage support === */
  function handleMessage(evt) {
    if (evt.origin !== COMMS_APP_ORIGIN) {
      return;
    }

    var data = evt.data;

    if (!data.type) {
      return;
    }

    if (data.type === 'hide-navbar') {
      NavbarManager.hide();
    } else if (data.type === 'show-navbar') {
      NavbarManager.show();
    }
  }
  window.addEventListener('message', handleMessage);


  /* === Bluetooth Support === */
  function btCommandHandler(message) {
    var command = message.command;
    var isAtd = command.startsWith('ATD');

    // Not a dialing request
    if (command !== 'BLDN' && !isAtd) {
      return;
    }

    navigator.mozApps.getSelf().onsuccess = (selfEvt) => {
      var app = selfEvt.target.result;
      app.launch('call_log');

      // Dialing a specific number
      if (isAtd && command[3] !== '>') {
        var phoneNumber = command.substring(3);
        CallMenu.dial(OptionHelper.defaultServiceId, phoneNumber, true);
        return;
      }

      // Dialing from the call log
      // ATD>3 means we have to call the 3rd recent number.
      var position = isAtd ? parseInt(command.substring(4), 10) : 1;
      CallLogDBManager.getGroupAtPosition(
        position, 'lastEntryDate', true, 'dialing', function(result) {
        if (result && (typeof result === 'object') && result.number) {
          CallMenu.dial(OptionHelper.defaultServiceId, result.number, true);
        } else {
          console.log('Could not get the group at: ' + position +
                      '. Error: ' + result);
          LazyLoader.load(['/shared/js/custom_dialog.js'], function() {
            var accept = {
              title: 'ok',
              recommend: true,
              callback: function() {
                CustomDialog.hide();
                NavigationMap.reset();
              }
            };
            CustomDialog.setVersion(2);
            CustomDialog.show('tcl-confirmation', 'no-recent-numbers', null,
              null, null, accept);
          });
        }
      });
    }
  }
  var timeoutHandler = null;
  var inCall = false;
  /* === Calls === */
  function call(number, cardIndex, isVideo) {
    var activity = null;
    this.inCall = true;
    if (timeoutHandler) {
      clearTimeout(timeoutHandler);
    }
    timeoutHandler = setTimeout(() => { this.inCall = false; }, 2000);

    var connected, disconnected;
    connected = disconnected = function clearPhoneView() {
    };

    var error = function() {
    };

    var oncall = function() {
    };

    LazyLoader.load(['/shared/js/dialer/telephony_helper.js'], function() {
      TelephonyHelper.call(
        number, cardIndex, isVideo, oncall, connected, disconnected, error);
    });
  }

  function init() {
    LazyLoader.load([
                     '/shared/style/progress_activity.css',
                     '/shared/elements/gaia-progress/dist/gaia-progress.js'
　　　　　　　　　　　　　　　　　　　　], function() {

      if (window.navigator.mozSetMessageHandler) {
        window.navigator.mozSetMessageHandler('telephony-call-ended',
                                              callEnded);
        window.navigator.mozSetMessageHandler('activity', handleActivity);
        window.navigator.mozSetMessageHandler('notification',
                                              handleNotification);
        window.navigator.mozSetMessageHandler('bluetooth-dialer-command',
                                               btCommandHandler);
      }
    });
    LazyLoader.load('/shared/js/settings_listener.js', function() {
      SettingsListener.observe('lockscreen.locked', null, function(value) {
        if (value) {
          screenState = 'locked';
        } else {
          screenState = 'unlocked';
        }
      });
      SettingsListener.observe('engineering-mode.key', null, function(value) {
        engineeringModeKey = value || null;
      });
    });

    // Added by yingsen.zhang@t2mobile.com 20180516 begin
    // Added for bug 4452, do not trace specified numbers
    // according to customization value
    console.log('Dialer init, read numbers traced customization value');
    navigator.customization.getValue('stz.numbers.should_not_traced').then((result) => {
      result = (result === undefined) ? [] : result;
      console.log('Dialer init, read value: ' + JSON.stringify(result));
      mNumbersShouldNotTraced = result;
    }, (reason) => {
      console.log('Dialer init, read value failed, reset to default');
      mNumbersShouldNotTraced = [];
    });
    // Added by yingsen.zhang@t2mobile.com 20180516 end
  }

  return {
    init: init,
    inCall: inCall,
    call: call
  };
})();

// Waiting for issue 787444 being fixed
window.onresize = function(e) {
  if (window.innerHeight < 440) {
    NavbarManager.hide();
  } else {
    NavbarManager.show();
  }
};
