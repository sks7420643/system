
(function syncStore(exports){'use strict';function onChange(evt){var self=this;doSync(this).then(function(){if(typeof self.onChangeFN==='function'){self.onChangeFN(self,evt);}});}
function ensureDS(SDS){if(SDS.store!==null){return Promise.resolve(SDS.store);}
return new Promise(function(resolve,reject){navigator.getDataStores(SDS.STORE_NAME).then(function(stores){if(!Array.isArray(stores)||stores.length<1){reject('Could not find store '+SDS.STORE_NAME);return;}
SDS.store=stores[0];SDS.store.addEventListener('change',onChange.bind(SDS));resolve(SDS);});});}
function doSync(SDS){if(SDS.syncInProgress){return Promise.reject('Sync in progress');}
SDS.syncInProgress=true;if(SDS.store===null){return Promise.reject('Store not initialised');}
return new Promise(function(resolve,reject){var cursor=SDS.store.sync(SDS.lastRevision);function resolveCursor(task){SDS.lastRevision=task.revisionId;switch(task.operation){case'update':case'add':var data=task.data;if(SDS.keyField in data&&!SDS.filterFN(data)){var key=data[SDS.keyField];SDS.persistStore.add(key,data,SDS.store.revisionId);}
break;case'clear':SDS.persistStore.clear(SDS.store.revisionId);break;case'remove':var id=task.id;SDS.persistStore.remove(id,SDS.store.revisionId);break;case'done':SDS.syncInProgress=false;resolve();return;}
cursor.next().then(resolveCursor);}
cursor.next().then(resolveCursor);});}
function SyncDataStore(name,persist,key){this.STORE_NAME=name;this.store=null;this.synced=false;this.lastRevision=0;this.keyField=key||'id';this.onChangeFN=null;this.syncInProgress=false;this.persistStore=persist;this.filterFN=function(){return false;};}
SyncDataStore.prototype={sync:function(revisionId){this.lastRevision=revisionId||this.lastRevision;return ensureDS(this).then(doSync);},remove:function(id,revisionId){return ensureDS(this).then((dataStore)=>{if(revisionId){return dataStore.remove(id,revisionId);}else{return dataStore.remove(id);}});},set onChange(onChange){this.onChangeFN=onChange.bind(this);},set filter(fn){this.filterFN=fn;}};function InMemoryStore(){this.results={};}
InMemoryStore.prototype={add:function(id,data){this.results[id]=data;},update:function(id,data){this.results[id]=data;},clear:function(){this.results={};},remove:function(id){delete this.results[id];}};exports.SyncDataStore=SyncDataStore;exports.InMemoryStore=InMemoryStore;})(window);