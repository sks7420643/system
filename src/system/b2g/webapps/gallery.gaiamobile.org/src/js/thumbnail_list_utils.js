'use strict';

/*
 * This file provide some functions to help update the thumbnail list
 *
 */

function lockSelectedPhotos () {
  var IMGS = thumbnails.querySelectorAll('.selected.thumbnail-list-img');
  if (IMGS.length < 1)
    return;
  var lockItemsLength = 0;
  var n = 0;
  Array.forEach(IMGS,
    function (IMG) {
      // if this file is already locked , skip
      if(!IMG.fileData.metadata.locked)
        updateMetadata(Gallery.getFileIndexByName(IMG.dataset.filename), {locked: true},
          function () {
            n++;
            lockItemsLength ++;
            IMG.lockStatusNode.reset(true);
            IMG.fileData.metadata.locked = true;
            if(n === IMGS.length) {
              SoftkeyManager.setLock(true);
              Message.show('tcl-multiple-images-locked', lockItemsLength);
              isSelectAll = true;
              setTimeout(function(){setView(LAYOUT_MODE.list);},500);
            }
          }
          );
      else
        n ++;
    }
  );
}

function unLockSelectedPhotos () {
  var IMGS = thumbnails.querySelectorAll('.selected.thumbnail-list-img');
  if (IMGS.length < 1)
    return;
  var unlockItemsLength = 0;
  var n = 0;
  Array.forEach(IMGS,
    function (IMG) {
      // if this file is already unlocked , skip
      if(IMG.fileData.metadata.locked)
        updateMetadata(Gallery.getFileIndexByName(IMG.dataset.filename), {locked: false},
          function () {
            n ++;
            unlockItemsLength ++;
            IMG.lockStatusNode.reset(false);
            IMG.fileData.metadata.locked = false;
            if(n === IMGS.length) {
              SoftkeyManager.setLock(false);
              Message.show('tcl-multiple-images-unlocked', unlockItemsLength);
              isSelectAll = true;
              setTimeout(function(){setView(LAYOUT_MODE.list);},500);
            }
          }
        );
      else
        n ++;
    }
  );
}

function shareItems() {
  if(currentView === LAYOUT_MODE.select) { // share selected items
    var blobs = selectedFileNames.map(function(name) {
      return selectedFileNamesToBlobs[name];
    });
    share(blobs);
  } else { // share current items
    var fileinfo = files[currentFileIndex];
    photodb.getFile(fileinfo.name, function(blob) {
      share([blob]);
    });
  }
}

function resetMultiSelect () {
  Array.forEach(thumbnails.querySelectorAll('.thumbnail-list-img'),
    function (IMG) {
      IMG.checkBoxContainerNode.classList.remove('hidden');
      IMG.checkBoxContainerNode.reset(false);
    });
}

// multiple select -- select all
function selectAllPhotos()
{
  var selectedNum = 0;
  var IMGS =thumbnails.querySelectorAll('.thumbnail-list-img');
  selectedFileNames = [];
  selectedFileNamesToBlobs = {};
  selectedLock = 0;

  if (isSelectAll) {
    Array.forEach(IMGS,
      function (IMG) {
        IMG.classList.add('selected');
        IMG.checkBoxContainerNode.reset(true);
        selectedFileNames.push(IMG.fileData.name);
        photodb.getFile(IMG.fileData.name, function (file) {
          selectedFileNamesToBlobs[ IMG.fileData.name ] = file;
        });
        if(IMG.fileData.metadata.locked)
          selectedLock ++;
      });

    selectedNum = IMGS.length;
    SoftkeyManager.setSkMenu();
    SoftkeyManager.updateSkText('tcl-deselect-all');
  }
  else {
    Array.forEach(IMGS,
      function (IMG) {
        IMG.classList.remove('selected');
        IMG.checkBoxContainerNode.reset(false);
      });
    selectedLock = 0;
    selectedNum = 0;
    SoftkeyManager.setSkMenu();
    SoftkeyManager.updateSkText('tcl-select-all');
  }
  updateMultipleCount(selectedNum);
  isSelectAll = !isSelectAll;
  Gallery.setSelectSubView(!isSelectAll?"all":"");
}

function resetSelection() {
  if (!isPhone) {
    // Clear preview screen on large device.
    clearFrames();
  }
  selectedFileNames = [];
  selectedFileNamesToBlobs = {};
  updateMultipleCount(0);
  resetMultiSelect();
}

function updateMultipleCount(count) {
  navigator.mozL10n.setAttributes(
    $('multiple-select-count'),
    'tcl-selected-items-count',
    { n: count }
  );
}

// multiple select -- delete selected photos
function deleteSelectedPhotos() {
  var IMGS = thumbnails.querySelectorAll('.selected.thumbnail-list-img');
  if (IMGS.length < 1)
    return;

  // check if contain locked photo(s)
  var containLocked = false;
  Array.forEach(IMGS,
    function (IMG) {
      if (IMG.fileData.metadata.locked) {
        containLocked = true;
      }
    }
  );

  // if selected files contain locked files , show error dialog
  if (containLocked ) {
    var dialogConfig = {
      title: {id: 'tcl-error', args: {}},
      body: {id: IMGS.length > 1 ? 'tcl-error-delete-locked-images' : 'tcl-error-delete-locked-image', args: {}},
      accept: {
        l10nId:'ok',
        callback: function(){
        },
      },
    };
    confirmDialog(dialogConfig);
    return;
  }

  //Show delete confirm Dialog
  var dialogConfig = {
    title: {id: 'tcl-confirmation', args: {}},
    body: {id: IMGS.length>1?'tcl-delete-selected-images':'tcl-delete-selected-image', args: {}},
    cancel: {
      l10nId:'cancel',
      callback: function(){
      },
    },
    confirm: {
      l10nId:'delete',
      callback: function(){
          deletedCountDone = 0;
          deletedCount = IMGS.length;
          setView(LAYOUT_MODE.list);
          NavigationMap.inProgress = true;
          listProgressBar.value = 0;
          listProgressBar.classList.remove('hidden');
          SoftkeyManager.softkey.hide();
          window.addEventListener('filedeleted', deleteDone);
          for (var i = 0; i < deletedCount; i++)
            deleteFile(Gallery.getFileIndexByName(IMGS[ i ].dataset.filename));
      },
    },
  };
  confirmDialog(dialogConfig);
}

function deleteDone() {
  deletedCountDone ++;
  listProgressBar.value = parseInt(100*deletedCountDone/deletedCount);
  if(deletedCountDone === deletedCount) {
    listProgressBar.classList.add('hidden');
    NavigationMap.inProgress = false;
    SoftkeyManager.softkey.show();
    window.removeEventListener('filedeleted', deleteDone);
    needNav();
    Message.show('tcl-deleted-n-photos', deletedCount);
    deletedCount = 0;
    deletedCountDone = 0;
  }
}

// check if the current display/selected image is locked
function chkIsCurrentLocked() {
  var fileinfo = files[currentFileIndex];
  if(fileinfo && fileinfo.metadata) {
    return typeof fileinfo.metadata.locked === 'undefined'?false : fileinfo.metadata.locked;
  }
  return false;
}

function chkIsCurrentLarge() {
  var fileinfo = files[currentFileIndex];
  if (fileinfo && fileinfo.metadata &&
      fileinfo.metadata.largeSize) {
    return true;
  }
  return false;
}

function updateMetadata(n, metadata,callback) {
  var fileinfo = files[n];
  if(fileinfo)
    photodb.updateMetadata(fileinfo.name,metadata,callback);
}

function updateLockStatusOfCurrentFile() {
  var fileInfo = files[ currentFileIndex ];
  var fileName = fileInfo.name;
  var thumbnailContainer = fileInfo.thumbnailContainer;
  var lock = !chkIsCurrentLocked();
  photodb.updateMetadata(fileName, {locked: lock}, function () {
      if (currentView === LAYOUT_MODE.fullscreen)
        currentFrame.container.lockStatusNode.reset(lock);
      else
        thumbnailContainer.lockStatusNode.reset(lock);
      fileInfo.metadata.locked = lock;
      SoftkeyManager.setLock(lock);
      Message.show(lock?'tcl-single-image-locked':'tcl-single-image-unlocked');
    }
  );
}

function deleteSingleItem() {
  var fileinfo = files[currentFileIndex];
  var msg = 'delete-photo?';

  // show error infomation if current file is locked and return
  if (fileinfo.metadata.locked) {
    var dialogConfig = {
      title: {id: 'tcl-error', args: {}},
      body: {id: 'tcl-error-delete-locked-image', args: {}},
      accept: {
        l10nId:'ok',
        callback: function(){
        },
      },
    };
    confirmDialog(dialogConfig);
    return;
  }

  //Show delete confirmation Dialog
  var dialogConfig = {
    title: {id: 'tcl-confirmation', args: {}},
    body: {id: currentView === LAYOUT_MODE.fullscreen ? 'tcl-delete-this-image' :'tcl-delete-selected-image', args: {}},
    backcallback: function(){
    },
    cancel: {
      l10nId:'cancel',
      callback: function(){
      },
    },
    confirm: {
      l10nId:'delete',
      callback: function(){
        window.addEventListener('filedeleted', singleDeleteDone);
        deleteFile(currentFileIndex);
        Message.show('tcl-single-image-deleted');
      },
    },
  };
  confirmDialog(dialogConfig);

  function singleDeleteDone() {
    window.removeEventListener('filedeleted', singleDeleteDone);
    needNav();
  }
}

// update info dialog
function displayFileInfo(data, filename) {
  var name_label = 'name-label';
  var size_label =  'size-label';
  var image_type_label = 'image-type-label'
  var date_taken_label = 'date-taken-label';
  var resolution_label = 'resolution-label';

  var list_cont = {};
  list_cont[name_label] = data['info-name'];
  list_cont[size_label] = data['info-size'];
  list_cont[image_type_label] = data['info-type'];
  list_cont[date_taken_label] = data['info-date'];
  list_cont[resolution_label] = data['info-resolution'];

  var infoContainer = infoDialog.querySelector('#infoView');
  infoContainer.innerHTML = '';
  Object.keys(list_cont).forEach(function (key) {
    var nameItem = document.createElement('dt');
    nameItem.setAttribute('data-l10n-id',key);
    nameItem.textContent = lget(key);
    var valueItem = document.createElement('dd');
    valueItem.classList.add('value');
    valueItem.textContent = list_cont[key];
    infoContainer.appendChild(nameItem);
    infoContainer.appendChild(valueItem);
  });
}

/**
* create a div and inert into thumbnail to display lock icon
**/
function createLockDiv(imgNode, locked) {
  var lockStatusNode = document.createElement('div');
  lockStatusNode.classList.add('thumbnail-lock-status');
  lockStatusNode.classList.add('gaia-icon');
  lockStatusNode.classList.add('icon-lock');
  lockStatusNode.style.display = locked? 'block':'none';
  lockStatusNode.reset = function(show) {
    this.style.display = show? 'block':'none';
  }
  imgNode.lockStatusNode = lockStatusNode;
  imgNode.appendChild(lockStatusNode);
}

/**
* create a input element and insert into thumbnail
**/
function createMultipleSelectInput(imgNode) {
  var checkBoxContainerNode = document.createElement('div');
  checkBoxContainerNode.classList.add('checkBoxContainer');
  checkBoxContainerNode.classList.add('hidden');

  checkBoxContainerNode.reset = function(checked){
    if(checked)
      checkBoxContainerNode.classList.add('checked');
    else
      checkBoxContainerNode.classList.remove('checked');
  }
  imgNode.checkBoxContainerNode = checkBoxContainerNode;
  imgNode.appendChild(checkBoxContainerNode);
}